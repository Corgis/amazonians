<?php
include 'functions.php';
include 'top.php';
$time = time();
$type = mysql_real_escape_string($_GET['type']);
$comment_table = $type."_comments";
$review_table = $type."_reviews";
$rating_table = $type."_ratings";
$content_id = $type."_id";
$content_name = $type."_name";
$time = time();

$num = 0;

if ($type == "movie")
{
	$event_type = 1;
	$type2 = "movies";
	$color_scheme = "movies";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Directed by </b>" . $content_row['director'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "../movies/images/" . $content_row['poster'];
	$thumb = "../movies/images/thumbs/";
	$nice_word = "Movies";
	$color = "blue";
}
else if($type == "tv")
{
	$event_type = 2;
	$type2 = "television";
	$color_scheme = "television";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['creator'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['network'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "../television/images/" . $content_row['poster'];
	$thumb = "../television/images/thumbs/";
	$nice_word = "T.V. Shows";
	$color = "green";
}
else if($type == "book")
{
	$event_type = 3;
	$type2 = "books";
	$color_scheme = "book";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Written by </b>" . $content_row['author'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "../books/images/" . $content_row['poster'];
	$thumb = "../books/images/thumbs/";
	$nice_word = "Books";
	$color = "red";
}
else if($type == "vg")
{
	$event_type = 4;
	$type2 = "videogames";
	$color_scheme = "games";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['producer'] . "</br>".
				  "<b>Made for </b>" . $content_row['platforms'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['genre'] . "</br>";
	$poster = "../videogames/images/" . $content_row['poster'];
	$thumb = "../videogames/images/thumbs/";
	$nice_word = "Video Games";
	$color = "yellow";
}
else if($type == "music")
{
	$event_type = 5;
	$type2 = "music";
	$color_scheme = "music";
	$description = stripslashes(str_replace("\n", ", " , $content_row['songs']));
	$extra_info = "<b>Produced by </b>" . $content_row['artist'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "../music/images/" . $content_row['poster'];
	$thumb = "../music/images/thumbs/";
	$nice_word = "Music Albums";
	$color = "purple";
}

if (isset ($_POST["comments"]))
{ 
	if(loggedin())
	{
		$message = mysql_real_escape_string(htmlentities($_POST["comments"]));
		
		$offset = 0;
		while(1)
		{
			$start = strpos($message, "@", $offset);
			if($start === false)
				break;
			
			$start++;
			
			for ($end = 13; $end >= 0; $end--) 
			{
				$name = substr($message, $start, $start+$end);
				$at = mysql_query("SELECT * FROM users WHERE username = '$name'") or (die(mysql_error()));
				if(mysql_num_rows($at))
				{
					$at_query = mysql_fetch_array($at);
					$at_id = $at_query['id'];
					$message = str_replace($name,"<a href=\'users.php?id=$at_id\'>$name</a>",$message);
					$offset = $start+$end;
					break;
				}
			}
			$offset += 13;
		}

		$sendcomment = mysql_query("INSERT INTO discussion SET message='$message', date=$time, user_id='".$_SESSION['id']."'");
		
		/*if ($sendcomment)
		{
			$sendcomment = mysql_query("INSERT INTO events SET content_id ='$id', object=1, date=$time, user_id='".$_SESSION['id']."', type = $event_type");
		}*/
		
		update_user_score((int)$_SESSION['id'], 1);
	}
}
?>
<body class="blue">
<div id="container">
  <?php include 'banner.php'; ?>
	<div id="main" role="main" class="container clearfix">
    <section id="single-content" class="left clearfix static" style="padding-top: 30px;">
      <article class="left" style="padding-top: 0px;">
        <section id="discussion" class="clearfix left">
          <h1><span class="left">Alltime Top 100</span></h1>
          <?php
				$i = 1;
		        $ratingsarray = array();
          		$query = mysql_query("SELECT * FROM $type WHERE num_ratings > 2 ORDER BY score DESC LIMIT 100");
				$thumb = "../$type2/images/thumbs/";
      			while($rating = mysql_fetch_array($query))
      			{
      				echo "<article>";
      				echo "<h2><a href='content.php?type=$type&id=".$rating[$content_id]."'>" . $rating[$content_name] . "</a></h2>";
					
      				echo "<a href ='content.php?type=$type&id=" .$rating[$content_id]. "'><img src='" . $thumb . $rating['poster'] . "' alt='" . $rating[$content_name] . "' /></a><span class='content-rating'><b>#". $i ."</b></span><span class='content-rating'><b>". ((int)($rating['score']*10))/10 ."</b></span>";
					$i++;
      			}
          ?>
          
        </section>
      </article>
    </section>
	</div>
	<footer id="global-footer" class="clearfix">
    <div class="container">
      <div class="clearfix">
      </div>
      
    <script type="Text/JavaScript">
	function textCounter(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) // if too long...trim it!
		field.value = field.value.substring(0, maxlimit);
		// otherwise, update 'characters left' counter
		else 
		countfield.value = maxlimit - field.value.length;
		}
      </script>
      
      <!-- Modal Reveal for comments and reviews-->
      <link rel="stylesheet" href="reveal.css">
      <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>-->
	  <script type="text/javascript" src="jquery.reveal.js"></script>
	  <style type="text/css">
			body { font-family: "HelveticaNeue","Helvetica-Neue", "Helvetica", "Arial", sans-serif; }
			.big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
		</style>
		
		  <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		  </nav>
		  <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		</div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>
<script>
/*
  $('#flow').masonry({
    itemSelector: 'article',
    columnWidth: 300,
    isFitWidth: true,
    gutterWidth: 20
  });
*/
</script>
<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
