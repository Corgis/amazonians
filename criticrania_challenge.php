<?php 
	include 'functions.php'; 
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style='text-align:center'>
    <?php 

    if(!loggedin()) {
    	echo "<h1>You must be logged in to play this game.</h1>";
    }
    else 
    {
	    $user_id = $_SESSION['id'];


		//see in which genres they've rated something
		$movies = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id");
		$num_movies_rated = mysql_num_rows($movies);
		$tvs = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_id");
		$num_tvs_rated = mysql_num_rows($tvs);
		$books = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_id");
		$num_books_rated = mysql_num_rows($books);
		$vgs = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_id");
		$num_vgs_rated = mysql_num_rows($vgs);
		$musics = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_id");
		$num_musics_rated = mysql_num_rows($musics);

		$total = $num_movies_rated + $num_tvs_rated + $num_books_rated + $num_vgs_rated + $num_musics_rated;
		if($total < 25) {
			"<h1>You need to rate " . 25 - $total . " thing(s) to play the Criticrania Challenge </h1></br>";
		}

		else 
		{
			$rand = rand(1,$total);
			if ($rand < $num_movies_rated){
				$content_id = "movie_id";
		   		$type = "movie";
		   	}
		   	else if ($rand < ($num_movies_rated + $num_tvs_rated)) {
		   		$content_id = "tv_id";
		   		$type = "tv";
		   	}
		   	else if ($rand < ($num_movies_rated + $num_tvs_rated + $num_books_rated)) {
		   		$content_id = "book_id";
		   		$type = "book";
		   	}
		   	else if ($rand < ($num_movies_rated + $num_tvs_rated + $num_books_rated + $num_vgs_rated)) {
		   		$content_id = "vg_id";
		   		$type = "vg";
		   	}
		   	else {
		   		$content_id = "music_id";
		   		$type = "music";
		   	}

		    $query = mysql_query("SELECT * 
		    FROM " . $type . "_ratings
		    WHERE user_id = $user_id
		    ORDER BY rand()
		    LIMIT 1");

		    $content_rating = mysql_fetch_array($query);
		    $id = $content_rating[$content_id];

		    $query = mysql_query("SELECT * FROM $type WHERE " . $type . "_id = $id");
		    $to_be_guessed = mysql_fetch_array($query);
		    
		    echo "<h1 style='color:#DB9E36'><i> Criticrania Challenge </i></h1> <div style='color:#DB9E36'>
		            Think you're a good critic? See how consistent you are by guessing what you've rated things.
		            Simply tell us what you think you rated the item that shows up and we'll tell you how
		            close you were! Think you've got what it takes?</br>
		            *You get 1 point if you're close. 2 points if you're perfect.</div>";
		    
		    echo "<h2 style='color:#3278AF'>" . $to_be_guessed[$type . '_name'] . " (" . $to_be_guessed['year'] . ")</h2></br>";
		    if($type == "movie")
		    	$type2 = "movies";
		    if($type == "tv")
		    	$type2 = "television";
		    if($type == "book")
		    	$type2 = "books";
		    if($type == "vg")
		    	$type2 = "videogames";
		    if($type == "music")
		    	$type2 = "music";
		    echo "<img src = $type2/images/" . $to_be_guessed['poster'] . "></br></br></br>";
		    ?>
		    <?php ratebar($type, 0);?><div id="ratebar" style="text-align : center; margin:0 auto;"></div>
					<form style="display: inline-block; width: 150px;" method="post" action="challenge_guess.php" name="guess">
						<input type="text" id="amount" name="amount" style="background: #222; border:0; color:#ddd; font-weight:bold; width:25px" readonly="true"/>
						<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
						<input type="hidden" name="type" value="<?php echo $type; ?>">
						<input type="hidden" name="content_id" value="<?php echo $id; ?>">
						<?php
							if (loggedin())
								echo "<button type='button' name='rate' onclick='this.form.submit()'>This is What I Rated it!</button>";
							else
								echo "<button type='button' name='rate' onClick=\"alert('You must be logged in to rate content.')\">Rate</button>";
						?>
					</form>
					</br></br>
			</div>
		<?php 
		}
	}
	?>

		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
