<?php include 'functions.php';  
	
	
	if (isset($_POST['group_name']) || isset($_POST['password']) || isset($_POST['creator']))
	{
		$group_name = $_POST['group_name'];
		$privacy = $_POST['privacy'];
		$password = $_POST['password'];
		$creator_id = (int)$_SESSION['id'];
		$redo = false;
		
		if($privacy == "Private")
			$privacy = 1;
		else
			$privacy = 0;
		
		if ($group_name == "")
		{
			$name_error = "empty";
			$redo = true;
		}
		else if (strlen($group_name) > 14)
		{
			$name_error = "long";
			$redo = true;
		}	
		else
		{
			$check = mysql_query("SELECT * FROM groups WHERE group_name = '". $group_name."'")or die();
			$exists_check = mysql_num_rows($check);
			if ($exists_check >= 1)
			{
				$name_error = "taken";
				$redo = true;
			}
		}
		
		if($password == "" && $privacy == 1)
		{
			$password_error = "empty";
			$redo = true;
		}
		elseif(strlen($password) > 14)
		{
			$password_error = "long";
			$redo = true;
		}
		if($redo == false)
		{
			mysql_query("INSERT INTO groups (private, password, group_name, creator) VALUES(
			'". mysql_escape_string($privacy)."','" . 
			mysql_escape_string($password)."','" .
			mysql_escape_string($group_name)."','" .
			mysql_escape_string($creator_id). "')"); 
			
			
			$query = mysql_query("SELECT * FROM groups WHERE group_name = '" . $group_name . "'");
			$new_group = mysql_fetch_array($query);
			$new_group_id = $new_group['group_id'];
			
			mysql_query("INSERT INTO group_relations (group_id, user_id, is_creator) VALUES(
			'". mysql_escape_string($new_group_id)."','" . 
			mysql_escape_string($creator_id)."','" .
			mysql_escape_string(1). "')");
			header("Location: groups.php?id=$new_group_id");
		}
	}
	
include 'top.php';		
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style="color:#CCC; text-align:center;">
	<h1>Create a Group</h1>
	</br></br>
	<form method='post' autocomplete="off" action='http://www.criticrania.com/create_group.php' name='Add_Group'>
        <label for="group_name">Group Name</label></br>
        <input id='group_name' type='text' name='group_name' onkeyup='timeoutUsernameCheck()'></br>
        <div id='groupname_exists' style='width: 120px; text-align: left; position: absolute; top: 0; left: 305px; display: inline-block;'></div>
				<?php if($name_error == "long"){echo "<br /> <span style='font-size: 10px; font-style: italic;'>Please enter a Group Name that is at most 14 characters long</span>";} 
				if($name_error == "empty"){echo "<br /> <span style='font-size: 10px; font-style: italic;'>Please enter a valid Group Name</span>";}
				if($name_error == "taken"){ echo"<br /> <span style='font-size: 10px; font-style: italic;'>This Group Name is taken</span>";}?>
        <br /></br></br>
        Public <input type="radio" name="privacy" value="Public">
		Private <input type="radio" name="privacy" value="Private"></br></br>
         <label for="password">Password (Only Relevant if Private Group)</label></br>
        <input id='password' type='text' name='password' onkeyup='timeoutUsernameCheck()'></br>
        <div id='passwrod_exists' style='width: 120px; text-align: left; position: absolute; top: 0; left: 305px; display: inline-block;'></div>
				<?php if($password_error == "long"){echo "<br /> <span style='font-size: 10px; font-style: italic;'>Please enter a password that is at most 14 characters long</span>";} 
				if($password_error == "empty"){echo "<br /> <span style='font-size: 10px; font-style: italic;'>Please enter a valid password</span>";}
				?>
        <br />
		<?php
		if (loggedin())
		{
			$query = mysql_query("SELECT * FROM users WHERE id = " . $_SESSION['id']);
			$user = mysql_fetch_array($query);
			if($user['score'] < 100) // not level 3
				echo "<button type='button' value='Create Group' onClick=\"alert('You must be a Classifier (Score > 100) to join a group!')\">Create Group</button>";
			else
				echo "<button type='button' value='Create Group' onclick='this.form.submit()'>Create Group</button>";
		}
		else
			echo "<button type='button' value='Create Group' onClick=\"alert('You must be logged in to create a group!.')\">Create Group</button>";
			
		?>
	</form></br></br>
	<!--<form method="post" action="http://www.criticrania.com/launch/add_group.php">
		Group Name <input type="text" name="group_name"></br></br>
		
		Password (Only Relevant if Private Group) <input type ="text" name="password"></br></br>
		
	</form>-->
	</br></br>
	
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
