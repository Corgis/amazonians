<?php include 'functions.php';  
	include 'top.php';
	
	if (isset($_POST['fname']) || isset($_POST['lname']) || isset($_POST['birthdate']) || isset($_POST['gender']))
	{
		$fname = mysql_real_escape_string($_POST['fname']);
		$lname = mysql_real_escape_string($_POST['lname']);
		$birth = mysql_real_escape_string($_POST['birth']);
		if ($_POST['gender'] == 'M')
			$gender = "Male";
		else
			$gender = "Female";
			
		mysql_query("UPDATE users SET first_name = '".$fname."', last_name = '".$lname."', birthdate = '".$birth."', gender ='".$gender."' WHERE username = '".$_SESSION['username']."' ") or die (mysql_error());
		
		header('Location: dashboard.php');
	}
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div style="color: #ccc;" id="main" role="main" class="container clearfix">
		<h1 style="color:#3278AF;">Thanks for signing up!</h1><br />
		While you wait for your confirmation e-mail, why not fill in some more details about yourself?<br /><br /><br />
		<div style="margin: 0 0 0 30px; float: left; width: 50%;">
			<form method='post' action="create.php" name='Profile'>
				<div style='width: 300px; text-align: right; padding: 0 0 10px 0;'><span style='font-weight: bold;'>First name: </span><input name='fname'  type='text'></div>
				<div style='width: 300px; text-align: right; padding: 0 0 10px 0;'><span style='font-weight: bold;'>Last Name: </span><input name='lname' type='text'></div>
				<div style='width: 300px; text-align: right; padding: 0 0 10px 0;'><span style='font-weight: bold;'>Birthdate: </span><input name='birth' type='text'><br />
				<span style="font-size: 10px; font-style: italic;">yyyy-mm-dd</span></div>
				<div style='width: 300px; padding: 0 0 10px 90px;'><span style='font-weight: bold;'>Gender: </span><input type="radio" name="gender" value="M" >M <input type="radio" name="gender" value="F">F
				<br /><br />
				<button type='submit' name='submit' onclick='this.form.submit()'>Submit</button><br /><br /></div>
			</form>
		</div>
		<div style="float: left; width: 40%;">
		<span style="font-size: 20px;">Why are we asking for this information?</span><br /><br />
		Because we want to give you the best experience out of this
		site, we gather information about you and compare it with all
		other users. This way, we can give you suggestions based
		on your age and gender and what other people your age and
		gender tend to like.<br /><br />
		Your personal information will <span style="font-weight: bold; font-style: italic;">never</span> be shared with third party companies or applications.
		</div>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
