<?php
mysql_connect("localhost","criticrania","Rpi2013") or die(mysql_error());
mysql_select_db("criticrania") or die(mysql_error());

session_start();

Header("Cache-Control: private");
$offset = 60 * 60 * 24 * 3;
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
Header($ExpStr);

$content_names = array(NULL, "movie",
                "tv",
                "book",
                "vg",
                "music" );

/*The admin function corresponds to a column in the user table that differentiates users
between being admins and non-admins which gives them special privileges in certain situations

It is used on the following pages:
comment_reply.php
content.php
deletecomment.php
deletereview.php
homepage.php
*/

function admin()
{
	if (!loggedin())
	return false;
	$id = $_SESSION['id'];
	$admincheck = mysql_query("SELECT * FROM users WHERE id = $id")or die();
	$admin = mysql_fetch_array($admincheck);

	if ($admin['admin'] == 1)
		return true;
	else
		return false;
}

/*This function creates the beginning of the banner for the top of every page that has a banner on criticrania

It is used on the following pages:
404.php
addcontent.php
advancedsearch.php
books.php
common.php
contact.php
content.php
create.php
fbcreate.php
feed2.php
feedback.php
homepage.php
index.php
list.php
login.php
messages.php
movies.php
music.php
newfrontpage.php
preferences.php
privacypolicy.php
profile.php
register.php
register2.php
review.php
search.php
send.php
sent.php
settings.php
stats.php
stats2.php
success.php
television.php
useragreement.php
users.php
users2.php
users3.php
users_backup.php
verify.php
videogames.php
*/

function banner($page_id)
{
	if ($page_id == 0)
	echo "<div id='banner'><a href='http://www.criticrania.com' id='home'></a>";
	else if ($page_id == 1)
	echo "<div id='banner_blue'><a href='http://www.criticrania.com' id='home'></a>";
	else if ($page_id == 2)
	echo "<div id='banner_green'><a href='http://www.criticrania.com' id='home'></a>";
	else if ($page_id == 3)
	echo "<div id='banner_red'><a href='http://www.criticrania.com' id='home'></a>";
	else if ($page_id == 4)
	echo "<div id='banner_orange'><a href='http://www.criticrania.com' id='home'></a>";
	else if ($page_id == 5)
	echo "<div id='banner_purple'><a href='http://www.criticrania.com' id='home'></a>";
}

/*
The function cc is used to compute the criticompatibility between two people which represents a correlation of how closely their tastes in a specific category compare

It is used on the following pages:
users.php
*/

function cc($user_1, $user_2, &$num, &$average, $type_name, $is_user2_group)
{
	if($is_user2_group == false)
	    ratings_compare($user_1, $user_2, $num, $average, false, $type_name);
	else
	    ratings_compare($user_1, $user_2, $num, $average, true, $type_name);
	$square = pow($num,2);
	if ($num > 0)
	{
		$high = 6+((4*$square)/($square+(5*$num)));
		$low = 4+((-4*$square)/($square+(5*$num)));
	}
	$c = (($high-$low)-$average)+$low;

	if(number_format($c,2) < 0)
		return 0;
	else
		return number_format($c,2);
}

/*This functions is used whenever a smaller picture of a piece of content is required for the feed, stat or user pages

It is used on the following pages:
thumbs.php

Within functions it is used the following functions:
feed
library
*/

function createThumb($pathToImages, $fname, $pathToThumbs, $thumbWidth, $thumb_type = 1)
{
	if ($thumb_type == 2)
		$thumbWidth = 60;
	//check to see if image already exists
	if (!file_exists($pathToThumbs . $fname))
	{
		// load image and get image size
		$img = imagecreatefromjpeg( "{$pathToImages}{$fname}" );
		$width = imagesx( $img );
		$height = imagesy( $img );

		// calculate thumbnail size
		$new_width = $thumbWidth;
		$new_height = floor( $height * ( $thumbWidth / $width ) );

		// create a new temporary image
		$tmp_img = imagecreatetruecolor( $new_width, $new_height );

		// copy and resize old image into new image
		imagecopyresampled( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

		// save thumbnail into a file
		imagejpeg( $tmp_img, "{$pathToThumbs}{$fname}" );
	}
}
/*This function breaks down and displays all of the information about a users ratin

/*The drop_menu function displays the menu at the top of each one of the content pages that have the options random content, list content, add content

ARGUMENTS: A string that represents the type

It is used on the following pages:
404.php
addcontent.php
advancedsearch.php
books.php
contact.php
content.php
create.php
fbcreate.php
functions.php
homepage.php
index.php
list.php
messages.php
movies.php
music.php
preferences.php
profile.php
register.php
search.php
television.php
videogames.php
*/

function drop_menu($type)
{
	if($type == "default")
	{
		echo "<div id='drop_menu'></div>";
		return;
	}

	$display = false;
	if ($type == "movie" || $_SERVER['PHP_SELF'] == "/movies.php") //basically a general switch function
	{
		$filler = mysql_query("SELECT * FROM movie");
		$max = mysql_num_rows($filler);
		$random = rand(1,$max);
		$type = "movie";
		$random_type = "movie";
		$add = " a movie";
		$list = "Movie";
		$num_pic = "1";
	}

	if ($type == "tv" || $_SERVER['PHP_SELF'] == "/television.php")
	{
		$filler = mysql_query("SELECT * FROM tv");
		$max = mysql_num_rows($filler);
		$random = rand(1,$max);
		$type = "tv";
		$random_type = "TV Show";
		$add = "a TV show";
		$list = "Television";
		$num_pic = "2";
	}

	if ($type == "book" || $_SERVER['PHP_SELF'] == "/books.php")
	{
		$filler = mysql_query("SELECT * FROM book");
		$max = mysql_num_rows($filler);
		$random = rand(1,$max);
		$type = "book";
		$random_type = "book";
		$add = " a book";
		$list = "Book";
		$num_pic = "3";
	}

	if ($type == "vg" || $_SERVER['PHP_SELF'] == "/videogames.php")
	{
		$filler = mysql_query("SELECT * FROM vg");
		$max = mysql_num_rows($filler);
		$random = rand(1,$max);
		$type = "vg";
		$random_type = "game";
		$add = " a game";
		$list = "Video Game";
		$num_pic = "4";
	}

	if ($type == "music" || $_SERVER['PHP_SELF'] == "/music.php")
	{
		$filler = mysql_query("SELECT * FROM music");
		$max = mysql_num_rows($filler);
		$random = rand(1,$max);
		$type = "music";
		$random_type = "album";
		$add = " music/podcasts";
		$list = "Album";
		$num_pic = "5";
	}

		echo "
		<div id='drop_menu' style='background-image: url(images/drop_menu$num_pic.jpg); font-size: 16px; height: 37px; padding-top: 4px;'>
			<div style='border-right: 1px solid white; text-align: center; display: inline-block; width: 200px;'>
				<a href='http://www.criticrania.com/content.php?type=$type&id=$random'>Random $random_type</a>
			</div>
			<div style='border-right: 1px solid white; text-align: center; display: inline-block; width: 200px;'>";
					echo "<a onclick='if(!checklogged()){return false;}' href='http://www.criticrania.com/addcontent.php?open=$type'>Add $add</a>";
			echo "
			</div>
			<div style='text-align: center; display: inline-block; width: 200px;'>
				<a href='http://www.criticrania.com/list.php?type=$type'>$list list</a>
			</div>
		</div>";
	}

//followed rating

function followed_rating($content_id, $type, &$num_ratings, $ratingornot, $all)
{
	$total = 0;
	$num_ratings = 0;
	$x = 0;
	$index = 0;

	if ($ratingornot){$object = "ratings";} else {$object = "reviews";}
	if ($type == 1 ){$content = "movie"; $folder = "movies"; $link = "001385";}
	if ($type == 2 ){$content = "tv"; $folder = "television"; $link = "0f6d00";}
	if ($type == 3 ){$content = "book"; $folder = "books"; $link = "c30000";}
	if ($type == 4 ){$content = "vg"; $folder = "videogames"; $link = "ed7500";}
	if ($type == 5 ){$content = "music"; $folder = "music"; $link = "87007f";}


	$feed_info = mysql_query("SELECT * FROM relationships WHERE user_id1 = '". (int)$_SESSION['id'] ."'")or die();

		while($array = mysql_fetch_array($feed_info))
		{
			$index++;
			$followed_array[] = $array['user_id2'];
		}

	$ratings = mysql_query("SELECT * FROM ".$content."_$object WHERE ".$content."_id = " . (int)$content_id . " ORDER BY date DESC")or die();

	while($rating = mysql_fetch_array($ratings))
	{
		while($rating['user_id'] != $followed_array[$x] && $x < $index)
		{
			$x++;
		}

		if($x == $index && $all == 'false')
		{
			$x = 0;
			continue;
		}

		else
		{
			if($ratingornot)
			{
				$total+=$rating['rating'];
				$num_ratings++;
				$x = 0;
			}

			else
			{
				$usercheck = mysql_query("SELECT * FROM users WHERE id = '". (int)$rating['user_id'] ."'")or die();
				$username = mysql_fetch_array($usercheck);
				$line = $rating['review'];
				$date = $rating['date'];
				$rating_query = mysql_query("SELECT * FROM ".$content."_reviews WHERE date = '$date' AND user_id='". (int)$rating['user_id'] ."'")or die();
				$score2 = mysql_fetch_array($rating_query);
				$score = $score2['score'];
				$line = stripslashes($line);
				if ($score < 0)
					$color = "red";
				else
				{
					$score = "+".$score;
					$color = "green";
				}
				echo "<a href= 'http://www.criticrania.com/users.php?id=" . $rating['user_id'] . "' >" . $username['username'] . "</a>: <font color='$color'>". $score ."</font> | ";
				if (admin())
				{
					echo "$line - <font style='cursor: pointer; color: #0000FF; text-decoration: underline;' line='".mysql_real_escape_string($line)."' thedate='$date' id='$content_id' class='scoreup'> Up</font> | <font style='cursor: pointer; color: #0000FF; text-decoration: underline;' line='".mysql_real_escape_string($line)."' thedate='$date' id='$content_id' class='scoredown'>Down</font> -
							<font style='cursor: pointer; color: #0000FF; text-decoration: underline' onclick=\"if(confirm('Are you sure you want to delete this review?')) window.location.href='http://www.criticrania.com/deletereview.php?type=$content&date=$date';\">remove</font>";
				}

				else if (loggedin() && $username['id'] == $_SESSION['id'])
				{
					echo "$line - <font style='cursor: pointer; color: #0000FF; text-decoration: underline' onclick=\"if(confirm('Are you sure you want to delete this review?')) window.location.href='http://www.criticrania.com/deletereview.php?type=$content&date=$date';\">remove</font>";
				}
				else if (loggedin())
					echo "$line - <font style='cursor: pointer; color: #0000FF; text-decoration: none;' line='".mysql_real_escape_string($line)."' thedate='$date' id='$content_id' class='scoreup'> Up</font> | <font style='cursor: pointer; color: #0000FF; text-decoration: none;' line='".mysql_real_escape_string($line)."' thedate='$date' id='$content_id' class='scoredown'>Down</font>";
				else
					echo "$line - <font style='cursor: pointer; color: #0000FF; text-decoration: none;' onclick='alert(\"You must be logged in to do that\")' > Up</font> | <font onclick='alert(\"You must be logged in to do that\")' style='cursor: pointer; color: #0000FF; text-decoration: none;'>Down</font>";
				echo "<br /><br />";
				$x = 0;
			}
		}
	}
	if($num_ratings > 0)
	{
		$avg = $total/$num_ratings;
		return ((int)($avg*10))/10;
	}
	return 0;
}

function forgotpass($email)
{
	$check = mysql_query("SELECT * FROM users WHERE email = '$email'") or die(mysql_error());
	if (mysql_num_rows($check) == 0)
		echo "We're sorry, there is no user who has registered with that e-mail address.";
	else
	{
		$info = mysql_fetch_array($check);
		$username = $info['username'];
		$random = rand(10000,99999);
		$newpass = "cc".$random;
		$db_pass = md5($newpass);
		$pass_change = mysql_query("UPDATE users SET password = '$db_pass' WHERE username = '$username' AND email = '$email'") or die(mysql_error());
		if ($pass_change)
		{
			require_once('class.phpmailer.php');

			$mail = new PHPMailer(); // defaults to using php "mail()"

			$body = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'><head>
			<meta http-equiv='content-type' content='text/html; charset=utf-8' />
			<title>Criticrania.com - Reviews Everyday from Everyday People</title>
			<style type='text/CSS'>
			body
			{
				background-color: #7c7c7c;
				background-image:url(http://www.criticrania.com/images/background_gradient.jpg);
				background-repeat:repeat-x;
				color: #000000;
				font-family: Trebuchet MS, Helvetica, Arial, sans-serif;
				font-size: 14px;
				margin: 30px 0px 0px 0px;
				padding: 0;
			}

			a
			{
				border: none;
				color: #000;
				font-weight: bold;
				text-decoration: none;
			}

			a:hover
			{
				text-decoration: underline;
			}

			#drop_menu
			{
				background-image: url(http://www.criticrania.com/images/drop_menu.jpg);
				background-repeat: no-repeat;
				color: #fff;
				font-size: 18px;
				height: 39px;
				margin: 0 auto;
				padding-top: 3px;
				position: relative;
				text-align: center;
				z-index: 10;
			}

			/* Banner */
			/****************************************************************************************************************************************************************************************************/
			/****************************************************************************************************************************************************************************************************/

			a#home
			{
				height: 75px;
				left: 15px;
				position: absolute;
				text-decoration: none;
				top: 20px;
				width: 400px;
			}

			#banner
			{
				background: url(http://www.criticrania.com/images/criticrania_banner.jpg) left top;
				background-repeat: no-repeat;
				height: 113px;
				margin: 0 auto;
				padding: 0 0 0 0;
				position: relative;
				width: 1008px;
				-moz-border-radius-topleft: 20px;
				-moz-border-radius-topright: 20px;
				-webkit-border-top-left-radius: 20px;
				-webkit-border-top-right-radius: 20px;
				border-top-left-radius: 20px;
				border-top-right-radius: 20px;
				-moz-box-shadow:0px 5px 15px #333;
				-webkit-box-shadow:0px 5px 15px #333;
				box-shadow:0px 5px 15px #333;
			}

			/* Page */
			/****************************************************************************************************************************************************************************************************/
			/****************************************************************************************************************************************************************************************************/

			#page
			{
				background: #fff;
				margin: 0 auto;
				min-height: 200px;
				overflow: hidden;
				padding: 0 0 20px 0;
				position: relative;
				width: 1008px;
				-moz-border-radius-bottomleft: 20px;
				-moz-border-radius-bottomright: 20px;
				-webkit-border-bottom-left-radius: 20px;
				-webkit-border-bottom-right-radius: 20px;
				border-bottom-left-radius: 20px;
				border-bottom-right-radius: 20px;
				-moz-box-shadow:0px 3px 15px #333;
				-webkit-box-shadow:0px 3px 15px #333;
				box-shadow:0px 3px 15px #333;
			}

			#nav
			{
				background: url(http://www.criticrania.com/images/nav_bg.jpg) repeat-x;
				height: 40px;
				margin: 0 auto;
				padding: 0 4px 0 4px;
				position: relative;
				width: 1000px;
			}

			.hover, .movie_nav, .tv_nav, .book_nav, .vg_nav, .music_nav
			{
				background-repeat: no-repeat;
				display: inline-block;
				height: 40px;
				width: 200px;
			}

			.movie_nav{background: url(http://www.criticrania.com/images/movie_nav.jpg) top;}
			.tv_nav{background: url(http://www.criticrania.com/images/tv_nav.jpg) top;}
			.book_nav{background: url(http://www.criticrania.com/images/book_nav.jpg) top;}
			.vg_nav{background: url(http://www.criticrania.com/images/vg_nav.jpg) top;}
			.music_nav{background: url(http://www.criticrania.com/images/music_nav.jpg) top;}

			.movie_nav:hover, .tv_nav:hover, .book_nav:hover, .vg_nav:hover, .music_nav:hover{background-position:bottom;}

			#title
			{
				padding-left: 5%;
				text-align: left;
				font-size: 1.8em;
			}

			#description
			{
				padding-left: 8%;
				padding-right: 8%;
				text-align: left;
			}

			/* Content */
			/****************************************************************************************************************************************************************************************************/
			/****************************************************************************************************************************************************************************************************/

			#content
			{
				background: #fff;
				padding: 0 2% 1% 2%;
				width: 96%;
			}

			/* Footer */

			#footer
			{
				clear: both;
				color: #fff;
				height: 29px;
				margin: 0 auto;
				text-align: center;
				width: 1008px;
			}
			</style>
			</head>
			<body>
			<!-- start banner -->
			<div id='banner'><a href='http://www.criticrania.com' id='home'></a></div>
			<!-- end banner -->
			<!-- start page -->
			<div id='page'>
				<div id='nav'>
					<div class='hover' style='display:inline;'><a class='movie_nav' href='http://www.criticrania.com/movies.php'> </a></div><div class='hover' style='display:inline;'><a class='tv_nav' href='http://www.criticrania.com/television.php'> </a></div><div class='hover' style='display:inline;'><a class='book_nav' href='http://www.criticrania.com/books.php'> </a></div><div class='hover' style='display:inline;'><a class='vg_nav' href='http://www.criticrania.com/videogames.php'> </a></div><div class='hover' style='display:inline;'><a class='music_nav' href='http://www.criticrania.com/music.php'> </a></div></div>
				<div id='drop_menu'></div>
				<div id='content'>
					<br />
					<span style='font-size: 20px;'>Criticrania Password Reset</span><br /><br />
					<div style='margin-left: 50px;'>
						Username: $username<br />
						Password: $newpass<br /><br />
						A new password was requested for http://www.criticrania.com using this e-mail address.<br />
						You can login using this new password and change your password by accessing the User Panel and clicking on Profile.<br />
						If you feel that this email was sent to you in error, please reply to feedback@criticrania.com
					</div>
				</div><br /><br />
			</div><br />
			<div id='footer'>
				&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
			</div>
			</body>
			</html>";

			$mail->AddReplyTo("feedback@criticrania.com","Criticrania Feedback");

			$mail->SetFrom("feedback@criticrania.com", "Criticrania Feedback");

			$mail->AddAddress($email);

			$mail->Subject    = "Criticrania Password Reset";

			$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

			$mail->MsgHTML($body);

			$mail->Send();

			echo "A new password has been sent to the e-mail address you provided. Thank you for using Criticrania!";
		}
	}
}

function get_image_url($content_type, $poster_name, $is_thumb)
{	
	if(!$is_thumb){
    $expires = time()+1000000;
    // S3 Signed URL creation
    $string_to_sign = "GET\n\n\n{$expires}\n/criticrania/".$content_type."/".$poster_name;
    $signature = urlencode(base64_encode((hash_hmac("sha1", utf8_encode($string_to_sign), 'fkCzwYF+Kz8kGthSeYk5+yVQ+VgZ3SvxgzNkWhia', TRUE))));
     $authentication_params = "AWSAccessKeyId=AKIAIJWWQCLJD3CVAB5Q";
    $authentication_params.= "&Expires={$expires}";
    $authentication_params.= "&Signature={$signature}";
    $url = "http://s3.amazonAWS.com/criticrania/".$content_type."/".$poster_name."?{$authentication_params}";
		$exists = @file_get_contents($url,null,null,-1,1) ? true : false ;
	}
	else {
		$url = $content_type . "/thumbs/" . $poster_name;
		$exists = file_exists($url);
	}
	

	if($exists){
		return $url;
	}
	else {
		return "images/does_not_exist.jpg";
	}

}

function hit($type, $id)
{

	$table = $type."_hits";
	$type_id = $type."_id";
	$time = time();
	mysql_query("INSERT INTO $table SET $type_id = '" . (int)$id."', visit=$time") or die();
}

function level($user_id)
{
	$curr_user = $user_id;
	$score = 0;

	$ratings = mysql_query("SELECT * FROM movie_ratings WHERE user_id = '$curr_user'");
	$temp = mysql_num_rows($ratings);
	$score+=$temp;
	$ratings = mysql_query("SELECT * FROM book_ratings WHERE user_id = '$curr_user'");
	$temp = mysql_num_rows($ratings);
	$score+=$temp;
	$ratings = mysql_query("SELECT * FROM tv_ratings WHERE user_id = '$curr_user'");
	$temp = mysql_num_rows($ratings);
	$score+=$temp;
	$ratings = mysql_query("SELECT * FROM vg_ratings WHERE user_id = '$curr_user'");
	$temp = mysql_num_rows($ratings);
	$score+=$temp;
	$ratings = mysql_query("SELECT * FROM music_ratings WHERE user_id = '$curr_user'");
	$temp = mysql_num_rows($ratings);
	$score+=$temp;

	$comments = mysql_query("SELECT * FROM movie_comments WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($comments))/2;
	$score+=$temp;
	$comments = mysql_query("SELECT * FROM book_comments WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($comments))/2;
	$score+=$temp;
	$comments = mysql_query("SELECT * FROM tv_comments WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($comments))/2;
	$score+=$temp;
	$comments = mysql_query("SELECT * FROM vg_comments WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($comments))/2;
	$score+=$temp;
	$comments = mysql_query("SELECT * FROM music_comments WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($comments))/2;
	$score+=$temp;

	$reviews = mysql_query("SELECT * FROM movie_reviews WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($reviews))*5;
	$score+=$temp;
	$reviews = mysql_query("SELECT * FROM book_reviews WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($reviews))*5;
	$score+=$temp;
	$reviews = mysql_query("SELECT * FROM tv_reviews WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($reviews))*5;
	$score+=$temp;
	$reviews = mysql_query("SELECT * FROM vg_reviews WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($reviews))*5;
	$score+=$temp;
	$reviews = mysql_query("SELECT * FROM music_reviews WHERE user_id = '$curr_user'");
	$temp = (mysql_num_rows($reviews))*5;
	$score+=$temp;

	$followers = mysql_query("SELECT * FROM relationships WHERE user_id2='$curr_user'");
	$temp = (mysql_num_rows($followers))*3;
	$score+=$temp;

	echo "Score: " . $score . "<br />";

	$level;

	if($score < 100)
		$level = "Beginner";
	else if($score < 200)
		$level = "Deliberater";
	else if($score < 300)
		$level = "Classifier";
	else if($score < 400)
		$level = "Evaluator";
	else if($score < 500)
		$level = "Examiner";
	else if($score < 600)
		$level = "Reviewer";
	else if($score < 700)
		$level = "Analyzer";
	else if($score < 800)
		$level = "Connoisseur";
	else if($score < 900)
		$level = "Critic";
	else
		$level = "Criticranium";

	echo "Level: " . $level . "<br />";
	echo "Points Until Next Level: " . (100 - ($score % 100)) . "<br />";
}

//returns name, threshold based on score of user
function level2($user_id, $score)
{
  $thresholds = array(  100,
                        300,
                        500,
                        750,
                       1000,
                       1300,
                       1750,
                       2250,
                       3000,
                      10000);

  $levels = array(  "Beginner",
                    "Deliberater",
                    "Classifier",
                    "Evaluator",
                    "Examiner",
                    "Reviewer",
                    "Analyzer",
                    "Connoisseur",
                    "Critic",
                    "Criticranium");

  $index = 0;
	while($score >= $thresholds[$index] && $index < (sizeof($thresholds) - 1) )
  {
     $index++;
  }

  return array($levels[$index], $thresholds[$index]);
}

function list_messages($type)
{
	if ($type == "inbox"){$table_info = "recipient"; $opposite = "sender"; $title = "From";}
	if ($type == "sent"){$table_info = "sender"; $opposite = "recipient"; $title = "To";}

		$mail = mysql_query("SELECT * FROM message WHERE $table_info = " . (int)$_SESSION['id'] . " ORDER BY date DESC")or die(mysql_error());

		echo "
		<table cellspacing='0' style='width: 100%;' id='table' class='tinytable'>
			<thead>
				<tr>
					<th style='border: 1px solid gray'width=25%>$title </th>
					<th style='border: 1px solid gray'width=45%>Subject </th>
					<th style='border: 1px solid gray'width=28%>Date </th>
				</tr>
			</thead>
			<tbody style='border-bottom: 1px solid gray;'>";
		$count = 0;
		while ($check = mysql_fetch_array($mail))
		{
			$check2 = mysql_query("SELECT * FROM users WHERE id = " . (int)$check[$opposite]."") or die(mysql_error());
			$from_check = mysql_fetch_array($check2);
			$from = $from_check['username'];
			$date = date("F j, Y, g:i A",$check['date']);
			$subject = stripslashes(stripslashes($check['subject']));
			if ($check['deleted'] != 2 && $type == "inbox")
			{
				echo "
					<tr id='mail". $check['mail_id'] ."' style='cursor: pointer'>
						<td style='border-left: 1px solid gray; border-right: 1px solid gray;'width=25%>$from</td>
						<td style='border-right: 1px solid gray; border-left: 1px solid gray'width=45%>$subject</td>
						<td style='border-right: 1px solid gray; border-left: 1px solid gray'width=28%>$date</td>
					</tr>
				";
				$count++;
			}
			else if ($check['deleted'] != 1 && $type == "sent")
			{
				echo "
					<tr id='mail". $check['mail_id'] ."' style='cursor: pointer'>
						<td style='border-left: 1px solid gray; border-right: 1px solid gray;'width=25%>$from</td>
						<td style='border-right: 1px solid gray; border-left: 1px solid gray'width=45%>".$check['subject']."</td>
						<td style='border-right: 1px solid gray; border-left: 1px solid gray'width=28%>$date</td>
					</tr>
				";
				$count++;
			}
		}
		if ($count == 0)
		{
			echo "
				<tr>
					<td style='border-left: 1px solid gray;'>
					</td>
					<td align='center'>
						<div style='font-style: italic;'>You have no messages to display.</div>
					</td>
					<td style='border-right: 1px solid gray;'>
					</td>
				</tr>
			";
		}
		echo "
			</tbody>
		</table>
		<table cellspacing='0' width='100%'>
			<tr>
				<td style='border-bottom: 1px solid gray; border-left: 1px solid gray;' width=25%> </td>
				<td style='border-bottom: 1px solid gray;' width=45%> </td>
				<td style='border-bottom: 1px solid gray; border-right: 1px solid gray' width=28%> </td>
			</tr>
		</table>";
}

function loggedin()
{
	if (isset($_SESSION['username'])|| isset($_COOKIE['username']))
	{
		if(!isset($_SESSION['username']))
		{
			$_SESSION['username'] = $_COOKIE['username'];
			$_SESSION['id'] = $_COOKIE['id'];
		}
		return true;
	}
	else
		return false;
}

function mailbox()
{
	echo "
	<a href='send.php'>Create</a><br />
	<a href='messages.php'>Inbox</a><br />
	<a href='sent.php'>Sent</a><br />
	";
}

function mailer($sender, $recipient, $message, $subject, $drafts)
{
	$time = time();
	$sender = mysql_real_escape_string($sender);
	$recipient = mysql_real_escape_string($recipient);
	$message = mysql_real_escape_string($message);
	$subject = mysql_real_escape_string($subject);
	$drafts = mysql_real_escape_string($drafts);
	$send_mail = mysql_query("INSERT INTO message SET sender = $sender, message = '$message', subject = '$subject', recipient = $recipient, date = $time") or die(mysql_error());
	if ($send_mail)
	return true;
	else
	return false;
}

function num_comments($type = '', $type2 = '', $type3 = '', $type4 = '', $type5 = '')
{
	$total_comments = 0;
	if ($type1 != '')
	{
		$num_comments = mysql_query("SELECT * FROM ".$type1."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
	}

	if ($type2 != '')
	{
		$num_comments = mysql_query("SELECT * FROM ".$type2."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
	}

	if ($type3 != '')
	{
		$num_comments = mysql_query("SELECT * FROM ".$type3."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
	}

	if ($type4 != '')
	{
		$num_comments = mysql_query("SELECT * FROM ".$type4."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
	}

	if ($type5 != '')
	{
		$num_comments = mysql_query("SELECT * FROM ".$type5."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
	}

	return $total_comments;
}

function num_ratings($type = '', $type2 = '', $type3 = '', $type4 = '', $type5 = '')
{
	$total_ratings = 0;
	if ($type1 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type1."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
	}

	if ($type2 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type2."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
	}

	if ($type3 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type3."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
	}

	if ($type4 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type4."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
	}

	if ($type5 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type5."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
	}

	return $total_ratings;
}

function num_reviews($type1 = '', $type2 = '', $type3 = '', $type4 = '', $type5 = '')
{
	$total_reviews = 0;
	if ($type1 != '')
	{
		$num_reviews = mysql_query("SELECT * FROM ".$type1."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type2 != '')
	{
		$num_reviews = mysql_query("SELECT * FROM ".$type2."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type3 != '')
	{
		$num_reviews = mysql_query("SELECT * FROM ".$type3."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type4 != '')
	{
		$num_reviews = mysql_query("SELECT * FROM ".$type4."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type5 != '')
	{
		$num_reviews = mysql_query("SELECT * FROM ".$type5."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}
	return $total_reviews;
}

function ratebar($type, $id)
{
	$table = $type."_ratings";
	$type_id = $type."_id";
	if (loggedin())
	{
		$check = mysql_query("SELECT * FROM $table WHERE user_id = ".$_SESSION['id']." AND $type_id = $id")or die(mysql_error());
		$exists = mysql_num_rows($check);
		$check2 = mysql_fetch_array($check);
		$start = $check2['rating'];

		if ($exists > 0)
		{
			echo "
				<script>
				$(document).ready(function(){
				function hexFromRGB (r, g, b) {
					var hex = [
						r.toString(16),
						g.toString(16),
						b.toString(16)
					];
					$.each(hex, function (nr, val) {
						if (val.length == 1) {
							hex[nr] = '0' + val;
						}
					});
					return hex.join('').toUpperCase();
				}

				function refreshSwatch(event, ui) {
					$('#amount').val(ui.value);

					if($('#ratebar').slider('value') <= 5)
					{
					var red = 255
						,green = ($('#ratebar').slider('value')*51)
						,hex = hexFromRGB(red, green, 0);
					}

					else
					{
						var red = 255 - (($('#ratebar').slider('value')*51)-256)
						,green = 255
						,hex = hexFromRGB(red, green, 0);
					}
					$('#ratebar .ui-slider-range').css('background-color', '#' + hex);
				}

				$('#ratebar').slider({
					range: 'min',
					min: 0,
					step: .1,
					max: 10,
					value: $start,
					slide: refreshSwatch,
					change: refreshSwatch
				});
				$('#amount').val($('#ratebar').slider('value'));
			});
			</script>";
		}
		else
		{
			echo "
				<script>
				$(document).ready(function(){
				function hexFromRGB (r, g, b) {
					var hex = [
						r.toString(16),
						g.toString(16),
						b.toString(16)
					];
					$.each(hex, function (nr, val) {
						if (val.length == 1) {
							hex[nr] = '0' + val;
						}
					});
					return hex.join('').toUpperCase();
				}

				function refreshSwatch(event, ui) {
					$('#amount').val(ui.value);

					if($('#ratebar').slider('value') <= 5)
					{
					var red = 255
						,green = ($('#ratebar').slider('value')*51)
						,hex = hexFromRGB(red, green, 0);
					}

					else
					{
						var red = 255 - (($('#ratebar').slider('value')*51)-256)
						,green = 255
						,hex = hexFromRGB(red, green, 0);
					}
					$('#ratebar .ui-slider-range').css('background-color', '#' + hex);
				}

				$('#ratebar').slider({
					range: 'min',
					min: 0,
					step: .1,
					max: 10,
					value: 5,
					slide: refreshSwatch,
					change: refreshSwatch
				});
				$('#amount').val($('#ratebar').slider('value'));
			});
			</script>";
		}
	}
	else
	{
		echo "
		<script>
		$(document).ready(function(){
		function hexFromRGB (r, g, b) {
			var hex = [
				r.toString(16),
				g.toString(16),
				b.toString(16)
			];
			$.each(hex, function (nr, val) {
				if (val.length == 1) {
					hex[nr] = '0' + val;
				}
			});
			return hex.join('').toUpperCase();
		}

		function refreshSwatch(event, ui) {
			$('#amount').val(ui.value);

			if($('#ratebar').slider('value') <= 5)
			{
			var red = 255
				,green = ($('#ratebar').slider('value')*51)
				,hex = hexFromRGB(red, green, 0);
			}

			else
			{
				var red = 255 - (($('#ratebar').slider('value')*51)-256)
				,green = 255
				,hex = hexFromRGB(red, green, 0);
			}
			$('#ratebar .ui-slider-range').css('background-color', '#' + hex);
		}

		$('#ratebar').slider({
			range: 'min',
			min: 0,
			step: .1,
			max: 10,
			value: 5,
			slide: refreshSwatch,
			change: refreshSwatch
		});
		$('#amount').val($('#ratebar').slider('value'));
	});
	</script>";
	}
}

function ratings_compare($user_1, $user_2, &$num, &$avg, $user2_is_group, $type)
{
	$user_1 = mysql_real_escape_string($user_1);
	$user_2 = mysql_real_escape_string($user_2);

	$type_id = $type."_id";
	$type_name = $type."_name";
	if ($type == "movie")
	{
		$type2 = $type."s";
		$type3 = "Movie";
	}
	if ($type == "book")
	{
		$type2 = $type."s";
		$type3 = "Book";
	}
	if ($type == "tv")
	{
		$type2 = "television";
		$type3 = "TV Show";
	}
	if ($type == "vg")
	{
		$type2 = "videogames";
		$type3 = "Video Game";
	}
	if ($type == "music")
	{
		$type2 = "music";
		$type3 = "Album";
	}

	$user_1_ratings = mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_1 ORDER BY $type_id");
	if($user2_is_group == false)
	    $user_2_ratings = mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_2 ORDER BY $type_id");
	else
	    $user_2_ratings = mysql_query("SELECT " . $type . "_id, AVG( rating ) AS rating, COUNT( " . $type . "_id ) AS num
                                        FROM " . $type . "_ratings AS m
                                        INNER JOIN group_relations AS g ON m.user_id = g.user_id
                                        WHERE g.group_id = $user_2
                                        GROUP BY " . $type . "_id");
	$user_1_list = mysql_fetch_array($user_1_ratings);
	$user_2_list = mysql_fetch_array($user_2_ratings);
	$num = 0;
	$total_diff = 0;

	while(1)
	{
		if(!$user_1_list || !$user_2_list)
			break;
		if($user_1_list[$type_id] == $user_2_list[$type_id])
		{
			if($print)
			{
				$content = mysql_query("SELECT * FROM $type WHERE $type_id ='" . (int)$user_1_list[$type_id] . "'");
				$title = mysql_fetch_array($content);

				?>
				<tr>
				<td width =50%> <a href="http://www.criticrania.com/content.php?type=<?php echo $type; ?>&id=<?php echo"".$title[$type_id].""?>"><?php echo $title[$type_name]; ?></a></td>
				<td width =20% align=center> <?php echo $type3; ?> </td>
				<td width =10% align=center> <?php echo $user_1_list['rating']; ?> </td>
				<td width =10% align=center> <?php echo $user_2_list['rating']; ?> </td>
				<td width =10% align=center> <?php echo abs($user_1_list['rating'] - $user_2_list['rating']); ?> </td>
				</tr>
				<?php
			}

			$total_diff += abs($user_1_list['rating'] - $user_2_list['rating']);
			$user_1_list = mysql_fetch_array($user_1_ratings);
			$user_2_list = mysql_fetch_array($user_2_ratings);

			$num++;
		}
		else
		if($user_1_list[$type_id] < $user_2_list[$type_id])
		{
			$user_1_list = mysql_fetch_array($user_1_ratings);
			if(!$user_1_list)
				break;
		}

		else
		{
			$user_2_list = mysql_fetch_array($user_2_ratings);
			if(!$user_2_list)
				break;
		}

	}
	if ($num > 0)
	$avg = $total_diff/$num;
}

function stats($type1 = '', $type2 = '', $type3 = '', $type4 = '', $type5 = '')
{
	$total_ratings = 0;
	$total_comments = 0;
	$total_reviews = 0;
	if ($type1 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type1."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
		$num_comments = mysql_query("SELECT * FROM ".$type1."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
		$num_reviews = mysql_query("SELECT * FROM ".$type1."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type2 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type2."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
		$num_comments = mysql_query("SELECT * FROM ".$type2."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
		$num_reviews = mysql_query("SELECT * FROM ".$type2."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type3 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type3."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
		$num_comments = mysql_query("SELECT * FROM ".$type3."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
		$num_reviews = mysql_query("SELECT * FROM ".$type3."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type4 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type4."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
		$num_comments = mysql_query("SELECT * FROM ".$type4."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
		$num_reviews = mysql_query("SELECT * FROM ".$type4."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	if ($type5 != '')
	{
		$num_ratings = mysql_query("SELECT * FROM ".$type5."_ratings WHERE user_id = ".(int)$_SESSION['id']."");
		$total_ratings += mysql_num_rows($num_ratings);
		$num_comments = mysql_query("SELECT * FROM ".$type5."_comments WHERE user_id = ".(int)$_SESSION['id']."");
		$total_comments += mysql_num_rows($num_comments);
		$num_reviews = mysql_query("SELECT * FROM ".$type5."_reviews WHERE user_id = ".(int)$_SESSION['id']."");
		$total_reviews += mysql_num_rows($num_reviews);
	}

	echo "
	Ratings: $total_ratings<br />
	Comments: $total_comments<br />
	Reviews: $total_reviews<br />
	";
}

function time_since_approx($seconds)
{

	if($seconds == 1)
	{
		return "<font style='font-size: 12px; font-style: italic;'>$seconds second ago</font>";
	}
	else if($seconds < 45)
	{
		return "<font style='font-size: 12px; font-style: italic;'>$seconds seconds ago</font>";
	}

	else if($seconds < 90)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about a minute ago</font>";
	}

	else if($seconds < 3000)
	{
		return "<font style='font-size: 12px; font-style: italic;'> ". ceil($seconds / 60) . " minutes ago</font>";
	}

	else if($seconds < 4000)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about an hour ago</font>";
	}

	else if($seconds < 76000)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about " . ceil($seconds / 3600) . " hours ago</font>";
	}

	else if($seconds < 100000)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about a day ago</font>";
	}

	else if($seconds < 518400)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about " . ceil($seconds / 86400) . " days ago</font>";
	}

	else if($seconds < 1209600)
	{
		return "<font style='font-size: 12px; font-style: italic;'>about a week ago</font>";
	}

	else if($seconds < 1814400)
	{
		return "about " .ceil($seconds / 604800) . " weeks ago";
	}

	else if($seconds < 4838400)
	{
		return "about a month ago";
	}

	else if($seconds < 12096000)
	{
		return "about " . ceil($seconds / 2419200) . " months ago";
	}

	else if($seconds < 16934400)
	{
		return "about a half a year ago";
	}

	else if($seconds < 26611200)
	{
		return "about " . ceil($seconds / 2419200) . " months ago";
	}

	else if($seconds < 31449600)
	{
		return "about a year ago";
	}

	else if($seconds < 261273600)
	{
		return "about " . ceil($seconds / 29030400) . " years ago";
	}

	else
		return "just a whole shit tonne of time ago";
}

function top_ten($type)
{
	$height = "389px";
	$rating_total = array();
	$rating_number = array();
	$rating_average = array();
	$table = $type."_ratings";
	$type_id = $type."_id";
	$time = time();
	$ratings = mysql_query("SELECT * FROM $table WHERE date > $time - 60*60*24*7")or die();
	$fill = mysql_query("SELECT * FROM $type ORDER BY $type_id") or die();

	if ($type == "movie" || $type == "book")
	{
		$type2 = $type."s";
	}
	if ($type == "tv")
	{
		$type2 = "television";
	}
	if ($type == "vg")
	{
		$type2 = "videogames";
	}
	if ($type == "music")
	{
		$height = "263px";
		$type2 = "music";
	}

	while($rating_array = mysql_fetch_array($ratings))
	{
		if(array_key_exists($rating_array[$type_id], $rating_total))
		{
			$rating_total[$rating_array[$type_id]]+= $rating_array['rating'];
			$rating_number[$rating_array[$type_id]]++;
			$rating_average[$rating_array[$type_id]] = $rating_total[$rating_array[$type_id]]/$rating_number[$rating_array[$type_id]];
		}

		else
		{
			$rating_total[$rating_array[$type_id]] = $rating_array['rating'];
			$rating_number[$rating_array[$type_id]] = 1;
		}

		$rating_average[$rating_array[$type_id]] = $rating_total[$rating_array[$type_id]]/$rating_number[$rating_array[$type_id]];
	}



	arsort($rating_average);
	$top_rated = array_keys($rating_average);

	while (sizeof($top_rated) < 10)
	{
		$size = sizeof($top_rated);
		$filler = mysql_fetch_array($fill);
		$filled = $filler[$type_id];
		$top_rated[$size] = $filled;
	}

	$counter = 1;

	while($counter < 11)
	{
		$check = mysql_query("SELECT * FROM $type WHERE $type_id = " . $top_rated[$counter-1] . "")or die();

		$row = mysql_fetch_array($check);
		$content_name = $type."_name";
		$name = $row[$content_name];
		$name = htmlspecialchars($name, ENT_QUOTES);
		$poster = "$type2/images/" . $row['poster'] . "";

		if (file_exists($poster))
		{
			echo '<a href="http://www.criticrania.com/content.php?type='.$type.'&id='.$row[$type_id].'"><img height='.$height.' src="'.$type2.'/images/' . $row['poster'] . '" title="#'. $counter .':'. $name. ' "/></a>';
		}

		else
		{
			echo '<a href="http://www.criticrania.com/content.php?type='.$type.'&id='.$row[$type_id].'"><img height='.$height.' src="images/no_image.png" title="#'. $counter .': ' . $name . ' "/></a>';
		}
		$counter++;
	}
}

function trending($type)
{
	$hits_info = array();
	$table = $type."_hits";
	$type_id = $type."_id";
	$time = time();
	$old_hits = mysql_query("DELETE FROM $table WHERE visit < $time - 3600") or die(mysql_error());
	$hits = mysql_query("SELECT * FROM $table")or die();
	if ($type == "movie")
	{
		$type2 = $type."s";
		$marg = 0;
		$color = "#3278af";
	}
	if ($type == "book")
	{
		$type2 = $type."s";
		$marg = 0;
		$color = "#c83535";
	}
	if ($type == "tv")
	{
		$type2 = "television";
		$marg = 0;
		$color = "#559e38";
	}
	if ($type == "vg")
	{
		$type2 = "videogames";
		$marg = 0;
		$color = "#db9e36";
	}
	if ($type == "music")
	{
		$type2 = "music";
		$marg = 10;
		$color = "#644484";
	}

	while($hit_array = mysql_fetch_array($hits))
	{
		if(array_key_exists($hit_array[$type_id], $hits_info))
		{
			$hits_info[$hit_array[$type_id]]++;
		}

		else
		{
			$hits_info[$hit_array[$type_id]] = 1;
		}
	}

	arsort($hits_info);
	$hits_top = array_keys($hits_info);

	/*$filler = mysql_query("SELECT * FROM $type");
	//$max = 0;
	//while ($filled = mysql_fetch_array($filler))
	//	$max++;

	while (count($hits_top) < 6)
	{
		$random = rand(1,$max);
		for ($i = 0; $i < sizeof($hits_top); $i++)
		{
			if ($random == $hits_top[$i])
			{
				$random = rand(1,$max);
			}
		}
		$hits_top[] = $random;
	}*/
	$real_hits = count($hits_top);
	if($real_hits < 5) {
		$fillers_needed = 5-$real_hits;
		$query = mysql_query("SELECT $type_id FROM $type WHERE checked = 1 ORDER BY rand() LIMIT $fillers_needed");
		while($row = mysql_fetch_array($query)){
			$hits_top[] = $row[$type_id];
		}
	}

	for( $i = 0; $i < 5; $i++)
	{
		$content = mysql_query("SELECT * FROM $type WHERE $type_id = '". (int)$hits_top[$i] ."'") or die();
		$content_info = mysql_fetch_array($content);
		$pic = $type2."/images/thumbs/".$content_info['poster']."";
		if ($type == "music")
			echo "<div style='float: left; margin-top: ".$marg."px; text-align: center; width: 121px;'> <a href='/content.php?type=$type&id=".$content_info[$type_id]."'><img id='trend' style='height: 110px; width: 110px;'";
		else
			echo "<div style='float: left; margin-top: ".$marg."px; text-align: center; width: 121px;'> <a href='/content.php?type=$type&id=".$content_info[$type_id]."'><img id='trend' ";

		if (file_exists($pic)) { ?>
			src="<?php echo $pic; ?>"
		<?php }
		else
			echo "src=images/no_image_thumb.png'";
		echo " /></a><br />";
		echo "<a href='/content.php?type=$type&id=".$content_info[$type_id]."' style = 'color:" . $color . "'>".$content_info[$type.'_name']. "</a><br /></div>";
		$marg += 20;
	}
}

//Add to a user's score a given amount
function update_user_score($user_id, $points_added)
{
	$get_score = mysql_query("SELECT * FROM users WHERE id = '$user_id'");
	$result = mysql_fetch_array($get_score);
	$curr_score = $result['score'];
	$curr_score = $curr_score + $points_added;
	mysql_query("UPDATE users SET score = '$curr_score' WHERE id = '$user_id' ");
}

//returns array of user's wishlist items for a specific genre
function get_wishlist($user_id, $type)
{
    $movies = array();
	$tvs = array();
	$books = array();
	$vgs = array();
	$musics = array();

	$get = mysql_query("SELECT * FROM wish_list WHERE user_id = '$user_id'");
	while($row = mysql_fetch_array($get))
		{
			if ($row['type'] == 'movie')
				$movies[] = ($row['type_id']);
			else if ($row['type'] == 'tv')
				$tvs[] = ($row['type_id']);
			else if ($row['type'] == 'book')
				$books[] = ($row['type_id']);
			else if ($row['type'] == 'vg')
				$vgs[] = ($row['type_id']);
			else if ($row['type'] == 'music')
				$musics[] = ($row['type_id']);
		}
	if($type == "movie")
		return $movies;
	elseif($type == "tv")
		return $tvs;
	elseif($type == "book")
		return $books;
	elseif($type == "vg")
		return $vgs;
	else
		return $musics;
}

function user_suggest_cc($user_id)
{
        //$user_id = (int)$_SESSION['id'];
        if(!can_cc($user_id))
        {
                return -1;
        }
        //find most recent rating over an 8 in each genre, if one exists, add it to the potential suggestions array
        $query = mysql_query("SELECT * FROM events WHERE user_id = ".$_SESSION['id']."");
        if(mysql_num_rows($query) == 0)
        {
                return -1;
        }
        $potential = array();
        $recent = 0;
        $check = mysql_query("SELECT * FROM movie_ratings WHERE user_id = '". $user_id ."' AND rating >= 8.0 ORDER BY date DESC LIMIT 1")or die(mysql_error());
        if(mysql_num_rows($check)>0)
                $potential[] = 1;

        $check = mysql_query("SELECT * FROM tv_ratings WHERE user_id = '". $user_id ."' AND rating >= 8.0 ORDER BY date DESC LIMIT 1")or die(mysql_error());
        if(mysql_num_rows($check)>0)
                $potential[] = 2;

        $check = mysql_query("SELECT * FROM book_ratings WHERE user_id = '". $user_id ."' AND rating >= 8.0 ORDER BY date DESC LIMIT 1")or die(mysql_error());
        if(mysql_num_rows($check)>0)
                $potential[] = 3;

        $check = mysql_query("SELECT * FROM vg_ratings WHERE user_id = '". $user_id ."' AND rating >= 8.0 ORDER BY date DESC LIMIT 1")or die(mysql_error());
        if(mysql_num_rows($check)>0)
                $potential[] = 4;

        $check = mysql_query("SELECT * FROM music_ratings WHERE user_id = '". $user_id ."' AND rating >= 8.0 ORDER BY date DESC LIMIT 1")or die(mysql_error());
        if(mysql_num_rows($check)>0)
                $potential[] = 5;

        //array of things with a recent rating over an 8
        $random = rand(0, count($potential)-1);
        $random_genre_date = $potential[$random];

        //determine which genre has been selected
        if($random_genre_date == 1)
        {
                $type = "movie";
        }
        if ($random_genre_date == 2)
        {
                $type = "tv";
        }
        if ($random_genre_date == 3)
        {
                $type = "book";
        }
        if ($random_genre_date == 4)
        {
                $type = "vg";
        }
        if ($random_genre_date == 5)
        {
                $type = "music";
        }
//compute all cc's existing for most recently rated thing
//$user_id = (int)$_SESSION['id'];
//$cc_movie = cc((int)$_SESSION['id'], (int)$users_id, $num_movie, $avg_movie, "movie");

$user_cc_arr = array();
//$highest_cc_user2 = array();

$get = mysql_query("SELECT * FROM users");
$num_users = mysql_num_rows($get);

for($i = 1; $i < $num_users; $i++)
{
  $check = mysql_query("SELECT * FROM relationships WHERE user_id1 = '$user_id' AND user_id2 = '$i'");
  $already_followed = mysql_num_rows($check);
  if($already_followed > 0 && !admin())
        continue;
  $num = 0;
  //$avg_movie = 0;
        $cc_type = cc($user_id, $i, $num, $avg, $type, false);
        if ($user_id != $i)
                {
                        $user_cc_arr[$i] = $cc_type;
                        //$highest_cc[] = $cc_movie;
                        //$highest_cc_user[] = $i;
                }
}

if($type == "") //nothing has been rated over an 8
        return array(-1, NULL, NULL);
        //echo "Sorry, we can't find a good match for you right now. Rate more things to get better suggestions!</br>";
else if (count($user_cc_arr) == 0) //no cc's over 7.5
        return array(-2, NULL, NULL);
        //echo "Sorry, we can't find a good match for you right now. Rate more things to get better suggestions!</br>";
else //there exists some suggestion(s)
{

        arsort($user_cc_arr); //sort by cc's
        //print_r($user_cc_arr);

        $counter = 0;
        foreach ($user_cc_arr as $key => $value)
                {
                        if($counter < 10 && $value < 6)
                                unset($user_cc_arr[$key]);
                }


        $rand_select = array_rand($user_cc_arr);
        //echo "</br></br>" . $rand_select . " with a cc of " . $user_cc_arr[$rand_select];

        $query = mysql_query("SELECT * FROM users WHERE id = '$rand_select'");
        $suggestion = mysql_fetch_array($query);

        //get three things they've rated similarly above 7
        $user1_get_list = mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_id AND rating >= 7");
        $user2_get_list = mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = ".$suggestion['id']." AND rating >= 7");

        $user1_list = array();
        $user2_list = array();
        while($row = mysql_fetch_array($user1_get_list))
                $user1_list[] = $row[$type."_id"];
        while($row = mysql_fetch_array($user2_get_list))
                $user2_list[] = $row[$type."_id"];

        //get items they both liked
        $super_list = array_intersect($user1_list, $user2_list);
        //print_r($super_list);

        //randomly select 3 times, removing things once they've been selected
        $select = array_rand($super_list);
        $similar1 = $super_list[$select];
        unset($super_list[$select]);
        //var_dump($super_list);
        $select = array_rand($super_list);
        $similar2 = $super_list[$select];
        unset($super_list[$select]);
        //var_dump($super_list);
        $select = array_rand($super_list);
        $similar3 = $super_list[$select];
        unset($super_list[$select]);
        //var_dump($super_list);
        //echo "Similar1 = " . $similar1 . " Similar2 = " . $similar2 . " Similar3 = " . $similar3 . "</br>";

        return array($type, $rand_select, $user_cc_arr[$rand_select], $similar1, $similar2, $similar3);
        //echo "You most recently rated something over an 8 in the genre " . $type . " therefore the following suggestion has been made: </br>";
        //echo "User: " . $suggestion['username'] . "</br> Because their ";
        //echo $type . "  CC with you is: " . $highest_cc[$random] . "     ";*/

}
}

// can_cc
// This function accepts a user as an argument a user id and effectively returns
// a boolean if we deem they can recieve a user suggestion

// It is used by user_suggest_cc()

function can_cc($user)
{
        $total = 0;

        $movie_query = mysql_query("SELECT rating FROM movie_ratings WHERE user_id = $user AND rating >= 8") or die(mysql_error());
        $tv_query = mysql_query("SELECT rating FROM tv_ratings WHERE user_id = $user AND rating >= 8") or die(mysql_error());
        $book_query = mysql_query("SELECT rating FROM book_ratings WHERE user_id = $user AND rating >= 8") or die(mysql_error());
        $music_query = mysql_query("SELECT rating FROM music_ratings WHERE user_id = $user AND rating >= 8") or die(mysql_error());
        $vg_query = mysql_query("SELECT rating FROM vg_ratings WHERE user_id = $user AND rating >= 8") or die(mysql_error());

        $total += mysql_num_rows($movie_query);
        $total += mysql_num_rows($tv_query);
        $total += mysql_num_rows($book_query);
        $total += mysql_num_rows($music_query);
        $total += mysql_num_rows($vg_query);


        if ($total > 0) {
                return true;
        } else {
                return $total;
        }
}

//

function sugg($user_id) {
	$genres = array();
	//see in which genres they've rated something
	$query = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id LIMIT 1");
	if (mysql_num_rows($query) != 0)
		$genres[] = "movie";
	$query = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_id LIMIT 1");
	if (mysql_num_rows($query) != 0)
		$genres[] = "tv";
	$query = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_id LIMIT 1");
	if (mysql_num_rows($query) != 0)
		$genres[] = "book";
	$query = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_id LIMIT 1");
	if (mysql_num_rows($query) != 0)
		$genres[] = "vg";
	$query = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_id LIMIT 1");
	if (mysql_num_rows($query) != 0)
		$genres[] = "music";

	//if no ratings at all
	if(sizeof($genres) == 0) {
		echo "You must rate something for us to start giving you suggestions";
		return;
	}

	$suggs = array();

	//get random type of things user has rated before
	$genre = $genres[array_rand($genres)];
	//get all highly rated items
	$rated_highly = mysql_query("SELECT " . $genre . "_id FROM " . $genre . "_ratings WHERE rating >= 7 ORDER BY rating DESC LIMIT 5");

	$random_content_select = rand(0,4);
	$counter = 0;

	$num_suggestions = 0;

	while(mysql_num_rows($rated_highly) > 0 && $source = mysql_fetch_array($rated_highly)) {
		if($counter == $random_content_select){
			$source_id = $source[$genre . '_id'];
			$other_users = mysql_query("SELECT user_id FROM " . $genre . "_ratings WHERE user_id <> $user_id AND " . $genre . "_id = $source_id");
			while(mysql_num_rows($other_users) > 0 && $similar_user = mysql_fetch_array($other_users)) {
				$similar_users_highly_rated = 	mysql_query("SELECT " . $genre . "_id FROM " . $genre . "_ratings
												WHERE user_id = " . $similar_user['user_id'] . " AND
												rating >= 7 AND " . $genre . "_id NOT IN (
													SELECT " . $genre . "_id FROM " . $genre . "_ratings WHERE user_id = $user_id)");
				while(mysql_num_rows($similar_users_highly_rated) > 0 && $potential_suggestion = mysql_fetch_array($similar_users_highly_rated)) {
						$suggs[] = $potential_suggestion[$genre.'_id'];
						$num_suggestions++;
				}
			}
			$counter++;
		}
		$counter++;
	}
	//nothing similar found
	if ($num_suggestions == 0) {
		//randomly select something that someone has rated highly
		$query = 	mysql_query("SELECT " . $genre . "_id FROM " . $genre . "_ratings WHERE rating >= 7 AND
					 " . $genre . "_id NOT IN (
					 		SELECT " . $genre_id . "_id FROM " . $genre . "_ratings WHERE user_id = $user_id )
							ORDER BY RAND LIMIT 1");
		$totally_random_sugg = mysql_fetch_array($query);
		$suggs[] = $totally_random_sugg[$genre.'_id'];
	}

	return array($suggs[array_rand($suggs)], $genre);

}


?>
