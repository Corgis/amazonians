<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" style="min-height: 540px; color: #ccc;" role="main" class="container clearfix">
		<h1 style="color:#3278AF;">Forgot Password</h1><br /><br /><br />
		<?php
		if (isset($_POST["email"]))
		{
			$email = $_POST["email"];
			forgotpass($email);
		}
		else
		{
			echo "
			Enter the e-mail address you signed up with:<br /><br />
			<form id='fbinput' method='POST' action='forgot.php'>
				<input type='text' name='email'>
				<button type='submit' name='submit' onclick='this.form.submit()'>Yeah, I forgot...</button>
			</form>
			";
		}
		?>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
