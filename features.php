<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style ="color:#CCC;font: 'Comfortaa', sans-serif;">
	<h1 style="color:#559E38;"><i><u> FEATURES </i></u></h1>
			<h2 style= "color:#559E38">Cranium</h2> On every single content page you will see a button that says, "Add to my Cranium." This is a running list you can keep in each media genre of those 
			things you know you want to try in the future, but never remember when you find the free time to actually do it. Anything that you add to yoru cranium can be found, filtered,
			under your Thoughtflow on your Dashboard.<br /><br />
			
			<h2 style= "color:#559E38">Criticompatibility (CC)</h2> Using a carefully thought up algorithm, this is the likeliness that you and another person will rate things similarly. As the number of ratings
			you share with another user increases, and the average difference in rating remains constant or decreases, the CC between both of you will increase.<br /><br />

			<h2 style= "color:#559E38">Followed</h2> You choose the people you follow. They could be your friends, acquaintances, or people you don't even know. To follow someone, simply visit his or her page and
			click the follow button. We imagine that you will follow people with whom you have a high CC as this will	lead to you finding more movies, television shows, etc. that you might enjoy.
			<br /><br />

			<h2 style= "color:#559E38">Thoughtflow</h2> On your dashboard, you will see a stream of recent happenings on Criticrania. This is constantly updated to show the 50 most recent
			ratings, comments, and reviews by any user on Criticrania. Now you can finally see what's happening around you!<br /><br />

			<h2 style= "color:#559E38">Suggestions for You</h2> Below your thoughtflow you will generally see a pair of suggestions created specifically for you. Based on your ratings and the ratings
			of those who have a high Criticompatibility with, we give you a new piece of content you may like as well as a user similar to you!<br /><br />

			<h2 style= "color:#559E38">Score</h2> By being active on the site: rating, commenting and reviewing things as well as gaining followers and replying to other users, a user can build up his 
			or her score. This is a way for other users to see how active you truly are and for you to gain the respect of your fellow users. In the near future, certain features
			will only be available to those users over a certain threshold. When logged in, the top right will always show your current score and progress bar as you try to reach
			the next level.<br /><br />

			<h2 style= "color:#559E38">Profile</h2> Every single user has their own profile page. You can see their highest rated pieces of content in each media genre. You can see your Criticompatibility with them.
			You get some of their statistics about their activity on the site. You can choose to follow them. You get their recent activity and a list of some of their top reviews. This is an
			opportunity to see how similar you and another person are. To see a user page simply click on their name anywhere on the site. To see your own user page, click your name
			in the top right when logged in. <i>You can search for users by their username in the search bar!</i><br /><br />

			<h2 style= "color:#559E38">Review</h2> For any piece of content, will be able to write a review. A review consists of your personal take on a media item. How was the acting, the wirting
			style, the story line, the voices, etc.? Why did you rate it what you did. Would you reccommend it? The reviews is expected to be a bit lengthier, hence the 5000 character
			limit.<br /><br />
			
			<h2 style= "color:#559E38">Comment</h2> This is the place to discuss a piece of content with other users on the site. Raise your point of view quickly. Mention your fvorite part. These
			are expected to be much shorter though, hence the limit of 250 character<br /><br />
			
			<h2 style= "color:#559E38">Rate</h2>This is the basis of Criticrania. Tell us what you think of this movie on a generalized scale. This is overall entertainment value of an item.
			0 Should represent the worst of this item you can imagine while 10 should represent the best of that genre you have ever experienced. Everyone has their own
			personal scale, but do try to incorporate the full range of numbers (not everything is better than a 9!)<br /><br />
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
