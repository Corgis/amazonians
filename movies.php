<?php
	include 'functions.php';
	include 'top.php';
	$type = "movie";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Movies</title>
<?php
	include 'sheetshome.php';
?>
</head>
<body style="background:url(img/body-bg.gif)">
<!-- start banner -->
<?php				
	//banner(1);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style="background:url(images/body-bg.gif)">
	<div id="title" style = "color:#3278af">Movies<br /></div>
		<div id="description" style="color:#3278af">
			Find your favorite movies. Find the movies during which you fell asleep. Find the newest movies to hit theatres.
			From the avid filmgoer to the average couch potato, everyone has his or her own opinion on the products of Hollywood.
		</div><br />
	<div id="content" style="background:url(images/body-bg.gif)">
		<div id="top_left"><br /><br />
			<div class="head">This Week's Top Rated Movies</div><br />
			<div id="slider">
				<?php
					top_ten("movie");
				?>
			</div>
		</div>
		<div id="top_right" style="background: "><br />
			<div class="head">Recent Reviews</div><br />
				<?php									
					$check = mysql_query("SELECT * FROM movie_reviews ORDER BY date DESC")or die();
					
					echo "<table width='100%' cellspacing='8px'>";
					$recent = 0;
					while($recent < 3)
					{
						$row = mysql_fetch_array($check);
						$usercheck = mysql_query("SELECT * FROM users WHERE id = '". $row['user_id'] ."'")or die();
						$username = mysql_fetch_array($usercheck);
						$review1 = addslashes($row['review']);
						$movie_info = mysql_query("SELECT * FROM movie_reviews WHERE review = '$review1'");
						$length = strlen($review1);
						$review1 = stripslashes($review1);
						$review1 = stripslashes($review1);
						$movie_row = mysql_fetch_array($movie_info);
						$movie_id = $movie_row['movie_id'];	
						$line2 = mysql_query("SELECT * FROM movie_reviews WHERE movie_id = '$movie_id' ORDER BY date DESC")or die();
						$movie_info = mysql_query("SELECT * FROM movie WHERE movie_id = '$movie_id'")or die();
						$movie_row = mysql_fetch_array($movie_info);
						echo "<tr valign='top'><td width='80%'>";
						echo "<hr><movietitle><a href='/content.php?type=movie&id=$movie_id' style = 'color:#3278af'>" .$movie_row['movie_name']. "</a></movietitle><hr><br />";
						echo "<a href= '/users.php?id=" . $row['user_id'] . "' color='black' >" . $username['username'] . "</a> - ";
						$poster = $movie_row['poster'];
						$filename = "movies/images/$poster";
						
						if ($length > 180)
						{
							$review2 = substr($review1, 0, 150);
							echo "\"$review2...\" <a href ='content.php?type=movie&id=$movie_id'>Read more</a>";
						}
						else
							echo "\"$review1\"";
						if (file_exists($filename))
						{
							$image = "<br><td><a href='content.php?type=movie&id=$movie_id'><img id='poster' src='movies/images/$poster' /></a>";
						}
						else
						{
							$image = "<br><td><a href='content.php?type=movie&id=$movie_id'><img id='poster' src='images/no_image_thumb.png' /></a>";
						}
						echo $image;
						echo "</td></tr>";
						$recent++;
					}
					echo "</table>";
				?>
				&nbsp;
		</div>
		<div id="bottom_left"><br />
			<div class="head">Trending Now</div>
			<div style="text-align: center; color:#3278af;">Here are the 5 most visited movies in the last hour</div><br />
				<?php
					trending("movie");
				?>
		</div>
		<div id="bottom_right"><br />
			<?php include 'poll.php'; ?>
		</div>
	</div>
	&nbsp;
</div><br />
<!-- end page -->

<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>
