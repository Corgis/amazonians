<?php
include 'functions.php';
$type = mysql_real_escape_string($_GET['type']);
$id = mysql_real_escape_string($_GET['id']);
$page = mysql_real_escape_string($_GET['page']);
$radds = mysql_real_escape_string($_GET['radds']);
$group = mysql_real_escape_string($_GET['group']);
if($group == NULL)
 	$group = "all";
if($radds == NULL)
	$radds = 0;
if($page == NULL)
	$page = 0;
if(!($type == 'movie' || $type == 'tv' || $type == 'book' || $type == 'vg' || $type == music))
    header("Location: /");
if($id == '' || $id < 1)
    header("Location: /");

$user_id = (int)$_SESSION['id'];

$comment_table = $type."_comments";
$review_table = $type."_reviews";
$rating_table = $type."_ratings";
$content_id = $type."_id";
$content_name = $type."_name";
$time = time();

$content_info = mysql_query("SELECT * FROM $type WHERE $content_id = '$id'")or die();
$content_row = mysql_fetch_array($content_info);

if(mysql_num_rows($content_info) < 1)
    {
    header("Location: /");
    echo "DOES NOT EXIST!";
    }

if ($content_row[$content_name] == "" || $content_row['checked'] != 1)
	header("Location: /dashboard.php");
else
	hit("$type",$id);

include 'top.php';

if ($type == "movie")
{
	$event_type = 1;
	$type2 = "movies";
	$color_scheme = "movies";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Directed by </b>" . $content_row['director'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "movies/images/" . $content_row['poster'];
	$thumb = "movies/images/thumbs/";
	$nice_word = "Movies";
	$color = "blue";
}
else if($type == "tv")
{
	$event_type = 2;
	$type2 = "television";
	$color_scheme = "television";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['creator'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['network'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "television/images/" . $content_row['poster'];
	$thumb = "television/images/thumbs/";
	$nice_word = "T.V. Shows";
	$color = "green";
}
else if($type == "book")
{
	$event_type = 3;
	$type2 = "books";
	$color_scheme = "book";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Written by </b>" . $content_row['author'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "books/images/" . $content_row['poster'];
	$thumb = "books/images/thumbs/";
	$nice_word = "Books";
	$color = "red";
}
else if($type == "vg")
{
	$event_type = 4;
	$type2 = "videogames";
	$color_scheme = "games";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['producer'] . "</br>".
				  "<b>Made for </b>" . $content_row['platforms'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['genre'] . "</br>";
	$poster = "videogames/images/" . $content_row['poster'];
	$thumb = "videogames/images/thumbs/";
	$nice_word = "Video Games";
	$color = "yellow";
}
else if($type == "music")
{
	$event_type = 5;
	$type2 = "music";
	$color_scheme = "music";
	$description = stripslashes(str_replace("\n", ", " , $content_row['songs']));
	$extra_info = "<b>Produced by </b>" . $content_row['artist'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "music/images/" . $content_row['poster'];
	$thumb = "music/images/thumbs/";
	$nice_word = "Music Albums";
	$color = "purple";
}
?>
<head>
    <title><?php echo $content_row[$content_name]; ?></title>
    <meta name="description" content= "<?php echo $description; ?>" />
    <meta name="image" content = "<?php echo 'http://www.criticrania.com/movies/images/thumbs/' . $content_row['poster']; ?>"/>
    <meta property="og:site_name" content = "Criticrania - The Multimedia Social Network"/>
    <meta property="og:type" content= "<?php echo $type; ?>" />
</head>
<?php
$num = 0;
if (isset ($_POST["comments"]))
{ 
	if(loggedin())
	{
		$message = mysql_real_escape_string(htmlentities($_POST["comments"]));
		
		$offset = 0;
		while(1)
		{
			$start = strpos($message, "@", $offset);
			if($start === false)
				break;
			
			$start++;
			
			for ($end = 13; $end >= 0; $end--) 
			{
				$name = substr($message, $start, $start+$end);
				$at = mysql_query("SELECT * FROM users WHERE username = '$name'") or (die(mysql_error()));
				if(mysql_num_rows($at))
				{
					$at_query = mysql_fetch_array($at);
					$at_id = $at_query['id'];
					$message = str_replace($name,"<a href=\'users.php?id=$at_id\'>$name</a>",$message);
					$offset = $start+$end;
					break;
				}
			}
			$offset += 13;
		}

		$sendcomment = mysql_query("INSERT INTO $comment_table SET $content_id ='$id', comment='$message', date=$time, user_id='".$_SESSION['id']."'");
		
		if ($sendcomment)
		{
			$sendcomment = mysql_query("INSERT INTO events SET content_id ='$id', object=1, date=$time, user_id='".$_SESSION['id']."', type = $event_type");
		}
		
		update_user_score((int)$_SESSION['id'], 1);
	}
}

if (isset ($_POST["reviews"]))
{ 
	if(loggedin())
	{
		$message = mysql_real_escape_string(htmlentities($_POST["reviews"]));
		
		$sendcomment = mysql_query("INSERT INTO $review_table SET $content_id ='$id', review='$message', date=$time, user_id='".$_SESSION['id']."'");
		
		if ($sendcomment)
		{
			$sendcomment = mysql_query("INSERT INTO events SET content_id ='$id', object=2, date=$time, user_id='".$_SESSION['id']."', type = $event_type");
		}
		
		update_user_score((int)$_SESSION['id'],8);
	}
}

if (isset ($_POST["review_id"]) && isset($_POST["change"]))
{
    $review_id = $_POST["review_id"];
	$change = $_POST["change"];
	$review_query = mysql_query("SELECT * FROM $review_table WHERE review_id = $review_id") or die(mysql_error());
	$review = mysql_fetch_array($review_query);
	$cont_id = $review[$content_id];
	$changer_id = $_SESSION['id'];
	
	if(loggedin() && $review['user_id'] != $changer_id)
	{	
		//$already = mysql_query("SELECT user_id FROM reviewers WHERE review = '$line'") or die(mysql_error());
		//if (mysql_num_rows($already) == 0)
		//{
			if($change == "up")
			{
				mysql_query("UPDATE $review_table SET score = score + 1 WHERE review_id = $review_id") or die(mysql_error());
				update_user_score($review['user_id'],1);
			}
			else
			{
				mysql_query("UPDATE $review_table SET score = score - 1 WHERE review_id = $review_id") or die(mysql_error());
				update_user_score($review['user_id'],-1);
			}
			
			//mysql_query("INSERT INTO reviewers SET user_id = ".$_SESSION['id'].", review = '$line'") or die(mysql_error());
			
			$content_query = mysql_query("SELECT * FROM $type WHERE $content_id = $cont_id");
			$content = mysql_fetch_array($content_query);
			
			$user_query = mysql_query("SELECT * FROM users WHERE id = ".$review['user_id']."") or die(mysql_error());
			$user = mysql_fetch_array($user_query);
			
			$changer_query = mysql_query("SELECT * FROM users WHERE id = $changer_id") or die(mysql_error());
			$changer = mysql_fetch_array($changer_query);
			
			$changer_username = $changer['username'];
			
			$name = $content[$type."_name"];
			
			mysql_query("INSERT INTO message (recipient, sender, date, deleted, subject, message) VALUES(".$user['id'].",0,$time,0,'Score Change!','Your review for <a href=http://www.criticrania.com/content.php?type=$type&id=$cont_id>$name</a> has been updated by <a href=http://www.criticrania.com/users.php?id=$changer_id>$changer_username</a>!')  ") or die(mysql_error());
			mysql_query("UPDATE users SET unread = 1 WHERE id = '".$user['id']."' ") or die(mysql_error());
		//}
		//else
		//{
			//header("Location: ".$_SERVER['HTTP_REFERER']."");
		//}
		
	}
}
//$rating = followed_rating($id, $event_type, $num, true, "true");
//$followed = followed_rating($id, $event_type, $num, true, "false");
?>
<body class="<?php echo $color_scheme; ?>">
<div id="container">
  <?php include 'banner.php'; ?>
	<div id="main" role="main" class="container clearfix">
		<div id="add_comment" class="reveal-modal">
			<h1>Add a Comment</h1>
			<?php if(loggedin()) 
			{
				?>
			<div id="addcom"> 
						<?php echo $_SESSION['username'];?>
					
					<div id="clockbox" >
					<?php echo date("n/j/y g:i A",time() + 10800); ?>
					</div>
					<form method="post" action="<?php echo "".$_SERVER['PATH_INFO'].""; ?>" name="addcomment">
						<textarea id="focused" name="comments" wrap=physical cols=50 rows=4 style="font-size: 12px; resize: none;" onKeyDown="textCounter(this.form.comments,this.form.remLen,250);"
						onKeyUp="textCounter(this.form.comments,this.form.remLen,250);"></textarea>
						<br /><br /><input readonly type=text name=remLen size=3 maxlength=3 value="250" style="width: 25px; border: none;"> characters left </font>
						<input  type="submit" value="Comment!">
						<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>" />
					</form>
			</div>
			<?php
			}
			else
			{
				echo "You must be logged in to enter a comment!";
			}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="add_review" class="reveal-modal">
			<h1>Add a Review</h1>
			<?php 
			$query = mysql_query("SELECT * FROM " .  $review_table . " WHERE user_id = " . (int)$_SESSION['id'] . " AND " . $content_id . " = " . $id . "");
			
			if(loggedin() && mysql_num_rows($query) == 0) 
			{
				?>
			<div id="addrev"> 
						<?php echo $_SESSION['username'];?>
					
					<div id="clockbox" >
					<?php echo date("n/j/y g:i A",time() + 10800); ?>
					</div>
					<form method="post" action="<?php echo "".$_SERVER['PATH_INFO'].""; ?>" name="addreview">
						<textarea id="focused" name="reviews" wrap=physical cols=75 rows=8 style="font-size: 12px; resize: none;" onKeyDown="textCounter(this.form.reviews,this.form.remLen,5000);"
						onKeyUp="textCounter(this.form.reviews,this.form.remLen,5000);"></textarea>
						<br /><br /><input readonly type=text name=remLen size=4 maxlength=4 value="5000" style="width: 35px; border: none;"> characters left </font>
						<input  type="submit" value="Review!">
						<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>" />
					</form>
			</div>
			<?php
			}
			elseif(loggedin() && mysql_num_rows($query) == 1)
				echo "You have already written a review for this item!";
			else
			{
				echo "You must be logged in to enter a review!";
			}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
    <section id="single-content" class="left clearfix static" style="padding-top: 0px;">
    	<img style="padding-top: 30px;" class="left" src="<?php echo get_image_url($type,$content_row['poster'], false); ?>"/>
      <article class="left" style="padding-top: 0px;">
        <h1><?php echo $content_row[$content_name] . " (" . $content_row['year'] . ")"; ?></h1>
		<?php ratebar($type, $id);?><div id="ratebar"></div>
			<form style="float: left; display: inline-block; width: 100px;" method="post" action="/rate.php">
				<input type="text" id="amount" name="amount" style="background: #222; border:0; color:#ddd; float: left; font-weight:bold; width:25px" readonly="true"/>
				<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
				<input type="hidden" name="type" value="<?php echo $type; ?>">
				<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
				<?php
					if (loggedin())
						echo "<button style='float: left' type='button' name='rate' onclick='this.form.submit()'>Rate</button>";
					else
						echo "<button style='float: left' type='button' name='rate' onClick=\"alert('You must be logged in to rate content.')\">Rate</button>";
				?>
			</form>
			<?php //echo can_cc($_SESSION['id']); ?>
        <!--<span class="left rating-bar"><button type="button" name="rate">Rate</button></span>-->
        <!--<span class="content-rating"><i>Followed</i><b><?php if(loggedin()) echo followed_rating($id, $event_type, $num, true, "false"); else echo "N/A";?></b><i><?php echo $num; ?> ratings</i></span> -->
        <?php if($group == "followed") {
            $query = mysql_query("SELECT avg(rating) as rating, count(*) as num FROM $rating_table WHERE $content_id = $id AND
                                  user_id IN (
                                  SELECT user_id2 FROM relationships
                                  WHERE user_id1 = $user_id
                                )");
            $result = mysql_fetch_array($query);
        ?>
        <span class="content-rating" id='show_rating'><i>Followed</i><b><?php if(loggedin()) echo round($result['rating'],1); else echo "N/A";?></b><i><?php echo $result['num']; ?> ratings</i></span>
        <?php }
        else if ($group == "all"){
            $query = mysql_query("SELECT avg(rating) as rating, count(*) as num FROM $rating_table WHERE $content_id = $id");
            $result = mysql_fetch_array($query);
            ?>
        <span class="content-rating" id='show_rating'><i>Criticrania</i><b><?php if(loggedin()) echo round($result['rating'],1); else echo "N/A";?></b><i><?php echo $result['num']; ?> ratings</i></span>  
        <?php } 
        else {
            $query = mysql_query("SELECT avg(rating) as rating, count(*) as num FROM $rating_table WHERE $content_id = $id
                                  AND user_id IN (
                                  SELECT user_id FROM group_relations WHERE group_id = $group)");
            $result = mysql_fetch_array($query);
            $query = mysql_query("SELECT group_name FROM groups WHERE group_id = $group");
            $group_name = mysql_fetch_array($query);
        ?>
        <span class="content-rating" id = 'show_rating'><i><?php echo "Group: " . $group_name['group_name']; ?></i><b><?php if(loggedin()) echo round($result['rating'],1); else echo "N/A";?></b><i><?php echo $result['num']; ?> ratings</i></span>
        <?php } ?>
        
        <?php if(loggedin()){ ?>
       <!--  <link href="css/uniform.agent.css" rel='stylesheet' type='text/css'/>
        <script type="text/javascript" src="jquery.uniform.min.js"></script>
        <div class="selector" id="uniform-"> -->
        <style>
        /*.select-style select
        {
		   background: transparent;
		   width: 80px;
		   padding: 5px;
		   font-size: 16px;
		   border: 1px solid #ccc;
		   height: 34px;
		   border-radius: 4px;
		}
		.select-style
		{
		   width: 90px;
		   height: 34px;
		   overflow: hidden;
		   /*background: url(images/cross.jpg) no-repeat right #ddd;
		   border-radius: 4px;
		}*/
		select
		{
			background: transparent;
			-webkit-appearance: none;
			/*border-radius: 7px;*/
			padding: 5px;
			width: 130px;
			outline: none;
		}
		.select-style
		{
			background: url(img/arrows.png) no-repeat 83px 6px #ddd;
			overflow: hidden;
			width: 100px;
			border-radius: 7px;
		}
		</style>
		    <form id="filter" style="margin: -30px 0 0 0; float: right;">
		    	<span style="color: #ccc; font-style: italic;">Filter:</span><br />
		    	<div class="select-style">
					<select name="filters" onchange="filterBy(this.value, <?php echo "'" . $id . "'"; ?>, <?php echo "'" . $type . "'"; ?>)">
						<option value="1">All</option>
						<option value="2">Followed</option>
						<?php 
						$query = mysql_query("SELECT group_id FROM group_relations WHERE user_id = $user_id");
						while($row = mysql_fetch_array($query))
						{
							$group_id = $row['group_id'];
							$query2 = mysql_query("SELECT group_name FROM groups WHERE group_id = $group_id");
							$result = mysql_fetch_array($query2);
							echo "<option value='" . $group_id . "'>" . $result['group_name'] . "</option>";
						}
						?>
					</select>
				</div>
			</form>
		
		<!-- </div> -->
	<?php } ?>

        <p style="padding-bottom: 20px;"><?php echo $description; ?></p>

        <p style="padding-bottom: 20px;"><?php echo $extra_info; ?></p>

        <p class="content-meta"><?php echo $secondary_info; ?></a>

        <p class="content-action">
        <form method="post" action="/wishlist.php">
		<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
		<input type="hidden" name="type" value="<?php echo $type; ?>">
		<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
		<?php
			$check_cranium = get_wishlist($_SESSION['id'], $type);
			$in == false;
			
			foreach($check_cranium as $item)
			{
				if ($item == $id)
				{
					$in = true;
					break;
				}
			}
			if (loggedin() && $in == false)
				echo "<button type='button' value='Add to my Cranium' onclick='this.form.submit()'>Add to your Cranium</button>";
			else if (loggedin() && $in == true)
				echo "<button type='button' value='Remove from my Cranium' onClick='this.form.submit()'>Remove from my Cranium</button>";
			else
				echo "<button type='button' value='Add to my Cranium' onClick=\"alert('You must be logged in to add something to your cranium.')\">Add to my Cranium</button>";
		?>
		</form>
        </p>
        <!-- <p class="content-action">
        <div id="fb-root"></div>
			<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div style="height: 50px;" class="fb-like" data-href="/content.php?type=<?php echo $type; ?>&amp;id=<?php echo $id; ?>" data-send="true" data-width="450" data-show-faces="false"></div>
        </p> -->
        <?php
        if ($type == "movie" && $content_row['year'] < 2011)
        {
        ?>
        <!--<p class="content-action">
        <form method="post" action="http://www.criticrania.com/upac_vote.php">
		<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
		<input type="hidden" name="type" value="<?php echo $type; ?>">
		<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
		<?php
		/*if (loggedin())
			echo "<button type='button' value='Vote for UPAC to Show this Movie!' onclick='this.form.submit()'>Vote for UPAC to show this movie!</button>";
		else
			echo "<button type='button' value='Vote for UPAC to Show this Movie!' onClick=\"alert('You must be logged in to vote.')\">Vote for UPAC to show this movie!</button>";*/
		?>
		</form>
        </p>-->
        <?php
        }
        ?>
      </article>
    </section>
    <section id="comments" class="right" style="padding-top: 50px;">
      <h1><span class="left">Comments</span><a class="add-content" href="#" data-reveal-id="add_comment" data-animation="fade">Add</a></h1>
      <?php 
      		if($group == 'all'){
      			echo "<script>console.log('in all');</script>";
      			$query = mysql_query("SELECT * FROM " . $comment_table . " WHERE " . $content_id ." = " . $id . " ORDER BY date DESC LIMIT " . $page*4 . ", 4");
      			$get_num_comments = mysql_query("SELECT count(*) as count FROM " . $comment_table . " WHERE " . $content_id ." = " . $id);

      		}
      		else if ($group == 'followed'){
      			echo "<script>console.log('in followed');</script>";
      			$query = mysql_query("SELECT * FROM $comment_table WHERE $content_id = $id AND
      		                          user_id IN (
      		                          SELECT user_id2 FROM relationships
                                      WHERE user_id1 = $user_id
                                      ) ORDER BY date DESC LIMIT " . $page*4 . ", 4");
      			$get_num_comments = mysql_query("SELECT count(*) as count FROM $comment_table WHERE $content_id = $id AND
      		                          user_id IN (
      		                          SELECT user_id2 FROM relationships
                                      WHERE user_id1 = $user_id
                                      )");
      		}
      		else {
      			echo "<script>console.log('in group');</script>";
      			$query = mysql_query("SELECT * FROM $comment_table WHERE $content_id = $id
                                  AND user_id IN (
                                  SELECT user_id FROM group_relations 
                                  WHERE group_id = $group
                                  ) ORDER BY date DESC LIMIT " . $page*4 . ", 4");
      			$get_num_comments = mysql_query("SELECT count(*) as count FROM $comment_table WHERE $content_id = $id
                                  AND user_id IN (
                                  SELECT user_id FROM group_relations 
                                  WHERE group_id = $group
                                  )");

      		}
      		$getter = mysql_fetch_array($get_num_comments);
      		$num_comments = $getter['count'];
      		echo "<div id = 'num_comm_info style='display:hidden';>" . $num_comments . "</div>";
      		echo "<script>num_comments = " . $num_comments . "; console.log('Entered num comments');</script>";
      		for($i = 0; $i < mysql_num_rows($query); $i++)
      		{
      			$comment = mysql_fetch_array($query);
				$date = date("n/j/y g:i A",$comment['date']+10800);
      			echo "<article>";
      			$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $comment['user_id'] . "'");
      			$user = mysql_fetch_array($get_name);
      			echo "<h2><a href='users.php?id=".$user['id']."'>" . $user['username'] . "</a><div class='right'>$date</div></h2>";
      			echo "<p>" . stripslashes($comment['comment']) . "</p>";
      			echo "</article>";
      			if ($i == 3)
      				break;
      		}
      		echo "<br />";
      		if($num_comments == 0)
      			echo "<i>There doesn't seem to be anything here. Be the first to comment!</i>";
      ?>
     </section>
	   <div id = 'comm_buttons' style="width: 220px; padding: 6.2em 0 0; color: #ccc; float: right">
			<?php
				//if ($page > 0)
		  			echo "<div id = 'comm_newer' style='font-size: 14px; float: left; display: inline; cursor: pointer'> &laquo Newer</div>";
		  		if ($num_comments > $page*4) 
					echo "<div id = 'comm_older' style='font-size: 14px; float: right; display: inline; cursor: pointer'>Older &raquo</div>";
			?>
		</div>
	</div>
	<footer id="global-footer" class="clearfix">
    <div class="container">
      <div class="clearfix">
        <section id="reviews" class="clearfix left">
        <?php if($radds == 0) { ?>
          <h1><span class="left">Reviews</span><a class="add-content" href="#" data-reveal-id="add_review" data-animation="fade">Add</a></h1>
          <?php
      	}
          		$reviews_shown = $radds * 3;
          		if($group == 'all'){
          		$query = mysql_query("SELECT * FROM " . $review_table . " WHERE " . $content_id ." = " . $id . " ORDER BY date DESC LIMIT $reviews_shown, 3");
      			$num_results = mysql_num_rows($query);
      			$get_num_reviews = mysql_query("SELECT count(*) as num FROM " . $review_table . " WHERE " . $content_id ." = " . $id);
				}
				else if ($group == 'followed'){
				$query = mysql_query("SELECT * FROM $review_table WHERE $content_id = $id AND
      		                          user_id IN (
      		                          SELECT user_id2 FROM relationships
                                      WHERE user_id1 = $user_id
                                      ) ORDER BY date DESC
								      LIMIT $reviews_shown, 3");
				$num_results = mysql_num_rows($query);
				$get_num_reviews = mysql_query("SELECT count(*) as num FROM $review_table WHERE $content_id = $id AND
      		                          user_id IN (
      		                          SELECT user_id2 FROM relationships
                                      WHERE user_id1 = $user_id
                                      )");
				}
				else {
				$query = mysql_query("SELECT * FROM $review_table WHERE $content_id = $id
                                  AND user_id IN (
                                  SELECT user_id FROM group_relations WHERE group_id = $group)
								  ORDER BY date DESC
								  LIMIT $reviews_shown, 3");
      			$num_results = mysql_num_rows($query);
      			$get_num_reviews = mysql_query("SELECT count(*) as num FROM $review_table WHERE $content_id = $id
                                  AND user_id IN (
                                  SELECT user_id FROM group_relations WHERE group_id = $group
                                  )");
				}
      			$fetch = mysql_fetch_array($get_num_reviews);
      			$num_reviews = $fetch['num'];
      			echo "<script>num_reviews = " . $num_reviews . ";</script>";
      			/*if($num_reviews > 3)
      				$num_reviews = 3;*/
      			echo "<div id = 'review_content'>";
      			for($i = 0; $i < $num_results; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				echo "<article>";
      				echo "<h2><a href='users.php?id=".$user['id']."'>" . $user['username'] . "</a><span class='" . $score_color . "'>" . $sign . $review['score'] . "</span></h2>";
      				echo "<p>" . stripslashes($review['review']) . "</br>" ?>
								<form method="post" action="/review_score_change.php" name="vote-up" class="vote-up">
									<input type="image" src="img/icon-upvote.png" />
									<input type="hidden" name="scorer" value="<?php echo $user_id; ?>" />
									<input type="hidden" name="content_type" value="<?php echo $type; ?>" />
									<input type="hidden" name="review_id" value="<?php echo $review['review_id']; ?>" />
									<input type="hidden" name="change" value="up">
								</form>
								<form method="post" action="/review_score_change.php" name="vote-up" class="vote-up">
									<input type="image" src="img/icon-downvote.png" />
									<input type="hidden" name="scorer" value="<?php echo $user_id; ?>" />
									<input type="hidden" name="content_type" value="<?php echo $type; ?>" />
									<input type="hidden" name="review_id" value="<?php echo $review['review_id']; ?>" />
									<input type="hidden" name="change" value="down">
								</form></p>					<?php
      				echo "</article>";
      				if ($i > 4)
      					break;
      			}
      			echo "</div>";
      			if ($num_reviews > 4)
      				echo "<div id = 'more_reviews'> See more... </div>";
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here. Be the first to write a review!</i>";
          ?>
          
        </section>
        <section id="more-like" class="content-blocks right">
          <?php echo "<h1>" . $nice_word . " like " . $content_row[$content_name] . "</h1>"; ?>
          
          <?php
			
			$query = mysql_query("SELECT * FROM " . $rating_table . " WHERE " . $content_id . " = '$id'");
			$num_ratings = mysql_num_rows($query);
			$liked_it = array();
			while($row = mysql_fetch_array($query))
			{
				if($row['rating'] >= 8)
				{
					$liked_it[] = $row['user_id'];
				}
			}
			$suggestions = array();
			for($i = 0; $i < count($liked_it);  $i++)
			{
				$user_check = $liked_it[$i];
				$query = mysql_query("SELECT * FROM " . $rating_table . " WHERE rating >= 8 AND user_id = '$user_check' AND $content_id != $id");
				while($row = mysql_fetch_array($query))
				{
					if(array_search($row[$content_id], $suggestions) != false) //already been suggested
						$suggestions[$row[$content_id]] = $suggestions[$row[$content_id]]+1;
					else
						$suggestions[$row[$content_id]] = 1;
				}
			}
			
			reset($suggestions);
			if(count($suggestions) > 4)
				$num_suggestions = 4;
			else
				$num_suggestions = count($suggestions);
			
				arsort($suggestions); 
				//print_r($suggestions);
				for($j = 0; $j < $num_suggestions; $j++)
				{	
					$finder = mysql_query("SELECT * FROM " . $type . " WHERE " . $content_id . " = '".(key($suggestions))."'") or die(mysql_error());
					$suggest_item = mysql_fetch_array($finder);
					echo "<article class ='" . $color . "'>";
					echo "<div class='rated-img'>";
					echo "<a href ='content.php?type=" . $type . "&id=" .(key($suggestions)). "'><img src='" . get_image_url($type,$suggest_item['poster'],true) . "'/></a>";
					echo "</div>";
					echo "<div class = 'rated-info'>";
					//echo "<h2><a href='content.php?type=" . $type . "&id=" . key($suggestions) . "'>".$suggest_item[$type . '_name']. "</a></h2>";
					echo "<h2><a href ='content.php?type=" . $type . "&id=" .(key($suggestions)). "'>".$suggest_item[$type . '_name']. "</a></h2>";
					$num = 0;
					$average_rating = followed_rating(key($suggestions), $event_type, $num, true, "true");
					echo "<p class='content-rating'><b>" . $average_rating . " out of 10</b></p>";
					echo "<p class='user'>" . $num . " ratings</p>";
					echo "</div>";
					echo "</article>";
					next($suggestions);
				}
			if($num_suggestions == 0)
				echo "More people need to rate this to give suggestions!";
			
		?>
        </section>
      </div>
      
    <script type="Text/JavaScript">
	function textCounter(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) // if too long...trim it!
		field.value = field.value.substring(0, maxlimit);
		// otherwise, update 'characters left' counter
		else 
		countfield.value = maxlimit - field.value.length;
		}
      </script>
      
      <!-- Modal Reveal for comments and reviews-->
      <link rel="stylesheet" href="reveal.css">
      <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>-->
	  <script type="text/javascript" src="jquery.reveal.js"></script>
	  <style type="text/css">
			body { font-family: "HelveticaNeue","Helvetica-Neue", "Helvetica", "Arial", sans-serif; }
			.big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
		</style>
		
		  <nav class="right">
			<ul>
			  <li><a href="privacypolicy.php">Privacy Policy</a></li>
			  <li><a href="useragreement.php">Terms of Use</a></li>
			  <li class="last"><a href="mailto:team@criticrania.com">Contact Us</a></li>
			</ul>
		  </nav>
		  <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		</div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>

<!-- AJAX call to filtering version of content to change rating, comments, reviews -->
<script type="text/javascript" src="jquery.js"></script>

<script>
	var original_address = document.URL;
	filter_address = document.URL;

	var adds = 0;
	console.log("Num reviews: " + num_reviews);

	$("#more_reviews").click(function(){
		adds++;
		console.log("Out Reviews shown " + ((adds+1)*3));
		if(((adds+1)*3)>=num_reviews)
		{
			$("#more_reviews").hide();
			console.log("Reviews shown " + ((adds+1)*3));
		}
		var address = filter_address+"&radds="+adds;
		console.log(address);
		$.get(address, function(data) {
			var reviews = $(data).find("#review_content").html();
			$('#review_content').append(reviews);
		});
	});
</script>

<script type="text/javascript">

		var page_counter = 0;
		var pages = 0;
		console.log('Num comments coming in is: ' + num_comments);
		pages = Math.floor((num_comments-1)/4);
		function dir_display(page_counter, pages)
		{
			console.log("Display Update");
			console.log("DU PC: " + page_counter);
			console.log("DU P: " + pages);
			if(page_counter <= 0)
				$("#comm_newer").hide();
			else
				$("#comm_newer").show();

			if(page_counter >= pages)
				$("#comm_older").hide();
			else
				$("#comm_older").show();
		}
		dir_display(page_counter, pages);
		$("#comm_older").click(function(){
			pages = num_comments;
			pages = Math.floor((pages-1)/4);
			$("#comments").empty().html('<img style="position: absolute; top: 55%; left: 50%" src="images/loading7.gif" />');
			page_counter++;
			if (page_counter >= pages)
				page_counter = pages;
			dir_display(page_counter, pages);
			console.log("Page Counter = "+page_counter);
			var address = filter_address+"&page="+page_counter;
			$.get(address, function(data) {
				var comments = $(data).find('#comments').html();
				document.getElementById("comments").innerHTML=comments;
			});
			dir_display(page_counter, pages);
		});
		$("#comm_newer").click(function(){
			pages = num_comments;
			pages = Math.floor((pages-1)/4);
			$("#comments").empty().html('<img style="position: absolute; top: 55%; left: 50%" src="images/loading7.gif" />');
			page_counter--;
			if(page_counter < 0)
				page_counter = 0;
			dir_display(page_counter, pages);
			console.log("Page Counter = "+page_counter);
			var address = filter_address+"&page="+page_counter;

			$.get(address, function(data) {
				var comments = $(data).find('#comments').html();
				document.getElementById("comments").innerHTML=comments;
			});
			dir_display(page_counter, pages);
		});

</script>

	 <script type="text/javascript">
		function filterBy(id, content_id, content_type)
		{	
			
			if(id == 1){
				var address = "content.php?type="+content_type+"&id="+content_id+"&group=all";
			}
			else if (id == 2){
				var address = "content.php?type="+content_type+"&id="+content_id+"&group=followed";
			}
			else {
				var address = "content.php?type="+content_type+"&id="+content_id+"&group="+id;
			}
			$.get(address, function(data) {
				  var comm = $(data).find('#comments').html();
				  var rating = $(data).find('#show_rating').html();
				  var revs = $(data).find('#reviews').html();
				  var num_comm_info = $(data).find('#num_comm_info').html();
				  document.getElementById("comments").innerHTML=comm;
				  document.getElementById("show_rating").innerHTML=rating;
				  document.getElementById("reviews").innerHTML=revs;
				  num_comments = document.getElementById("num_comm_info").value;
				  console.log("Num comments in filter = " + num_comments);

			});
			filter_address = address;
			console.log("Num comments after filter = " + num_comments);
			dir_display(page_counter, pages);
		}
		</script>

<script>
/*
  $('#flow').masonry({
    itemSelector: 'article',
    columnWidth: 300,
    isFitWidth: true,
    gutterWidth: 20
  });
*/
</script>
<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
