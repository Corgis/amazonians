/* Author:
   Evan Dudla, Criticrania Team
*/
$(document).ready(function() {
  /**
   * Search label hiding
   */
   var search_input = $("#search").val("");
   var search_label = $("#search-form label").css({display: 'block'});

   /* On focus, hide label ------ */
   $(search_input).focus(function() {
     $(search_label).animate({fontSize: '0'}, 200, 'swing', function() {
       $(search_label).css({display: 'none'});
     });
   });
   /* On blur, if nothing is entered present label ----- */
   $(search_input).blur(function() {
     if (this.value === this.defaultValue) {
       $(search_label).stop().css({fontSize: '13px', display: 'block'});
     }
   });
});

