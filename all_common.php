<?php include 'functions.php';  
	include 'top.php';

	$type = mysql_real_escape_string($_GET['type']);
	$user_id1 = (int)$_SESSION['id'];
	$user_id2=  mysql_real_escape_string($_GET['id']);

	if($type == 'movie')
	{
		$thumb = "movies/images/thumbs/";
		$title = "Movie";
		$color = "blue";
	}
	if($type == 'tv')
	{
		$thumb = "television/images/thumbs/";
		$title = "TV Show";
		$color = "green";
	}
	if($type == 'book')
	{
		$thumb = "books/images/thumbs/";
		$title = "Book";
		$color = "red";
	}
	if($type == 'vg')
	{
		$thumb = "videogames/images/thumbs/";
		$title = "Game";
		$color = "yellow";
	}
	if($type == 'music')
	{
		$thumb = "music/images/thumbs/";
		$title = "Music";
		$color = "purple";
	}

	$similar = mysql_query("SELECT rs.u1_rating as u1_rating, rs.u2_rating as u2_rating, content." . $type . "_id as id, content." . $type . "_name as name, content.poster as poster
						FROM " . $type . " AS content
						JOIN (
						SELECT r1.rating AS u1_rating, r2.rating AS u2_rating, r1." . $type . "_id
						FROM " . $type . "_ratings AS r1
						JOIN " . $type . "_ratings AS r2 ON r1." . $type . "_id = r2." . $type . "_id
						AND r1.user_id =" . $user_id1 . "
						AND r2.user_id =" . $user_id2 . "
						) AS rs ON content." . $type . "_id = rs." . $type . "_id
						ORDER BY content." . $type . "_name ASC ");

?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
		<div class = "<?php echo $color; ?>">
			<h1>Common Ratings</h1>
			<table cellspacing="10" cellpadding="10">
			<th>You</th>
			<th><?php echo $title; ?></th>
			<th>Them</th>
			<?php 
				while($row = mysql_fetch_array($similar))
				{
					echo "<tr>";
					echo "<td>" . $row['u1_rating'] . "</td>";
					echo "<td>" . "<a href='/content.php?type=".$type."&id=".$row['id']."'><img src='".$thumb . $row['poster']."'></a>" . "</td>";
					echo "<td>" . $row['u2_rating'] . "</td>";
					echo "<tr>";
				}
			?>
		</table>
		</div>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
