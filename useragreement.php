<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style="color:#CCC">
	User Agreement<br /><br />

		By signing up as a Criticrania User, you hereby agree to the following:<br /><br />

		Criticrania reserves to right to use your ratings, comments, and reviews, in coordination with your username only, in any way.<br /><br />

		As a User, you agree not to attempt to maliciously attack this site or any of its users through any means. <br /><br />

		Criticrania reserves the right to establish cookies on your internet providing device for proper use of the site.<br /><br />

		As a User, you agree to respect the creators of this site and all its users up to the standards determined by Criticrania.<br /><br />

		 Criticrania reserves the right to suspend or ban your use of this site for any reasons we deem reasonable.<br /><br />

		You will not post commercial messages or comments on the site (spam). Criticrania reserves the right to delete any postings it deems inappropriate.<br /><br />

		You will not in any way store the information collected by Criticrania through its users for any reason unless given consent by Criticrania.<br /><br />

		You will not attempt to steal login information, solicit yours or others' login information, or sign in as someone else.<br /><br />

		You will not take any action that infringes upon someone's legal right or is unlawful.<br /><br />

		You will not use anything copyrighted or owned by Criticrania for any use unless given explicit permission by Criticrania.<br /><br />

		You will not encourage any violation of the agreed to statements above.<br /><br /><br />
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
