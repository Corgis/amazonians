<?php include 'functions.php'; 

$user_view = mysql_real_escape_string($_GET['id']);
$user_id = (int)$_SESSION['id'];
$time = time();

//information
$query = mysql_query("SELECT * FROM users WHERE id = $user_view");
$user_view_info = mysql_fetch_array($query);

//level
$level = level2($user_view, $user_view_info['score']);

//stats
$num_ratings = 0;
$num_comments = 0;
$num_reviews = 0;
$num_followers = 0;

$type = "movie";
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_ratings WHERE user_id = $user_view"));
$movie_num_ratings = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_comments WHERE user_id = $user_view"));
$movie_num_comments = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_reviews WHERE user_id = $user_view"));
$movie_num_reviews = $getter['c'];
$type = "tv";
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_ratings WHERE user_id = $user_view"));
$tv_num_ratings = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_comments WHERE user_id = $user_view"));
$tv_num_comments = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_reviews WHERE user_id = $user_view"));
$tv_num_reviews = $getter['c'];
$type = "book";
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_ratings WHERE user_id = $user_view"));
$book_num_ratings = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_comments WHERE user_id = $user_view"));
$book_num_comments = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_reviews WHERE user_id = $user_view"));
$book_num_reviews = $getter['c'];
$type = "vg";
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_ratings WHERE user_id = $user_view"));
$vg_num_ratings = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_comments WHERE user_id = $user_view"));
$vg_num_comments = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_reviews WHERE user_id = $user_view"));
$vg_num_reviews = $getter['c'];
$type = "music";
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_ratings WHERE user_id = $user_view"));
$music_num_ratings = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_comments WHERE user_id = $user_view"));
$music_num_comments = $getter['c'];
$getter = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM " . $type . "_reviews WHERE user_id = $user_view"));
$music_num_reviews = $getter['c'];

//$num_followers = mysql_num_rows(mysql_query("SELECT * FROM relationships WHERE user_id1 = $user_view"));
$type = "";


?>
<?php include 'top.php'; ?>
<style>
.movie_stuff{
text-decoration:none;
};
</style>
<script type="text/javascript">
$(document).ready(function(){
	var slides = $('.sliding_posters');
	var num = slides.length;
	
	$('#flowinner').css('width', num*490);
	
	var count = 0;
	$("#right").click(function()
	{
		$("#left").show();
		if(count == (num-1))
			return;
		else
			$(".sliding_posters").animate({left: "-=490"});
		count++;
	});
	$("#left").click(function()
	{
		if(count == 0)
			return;
		else
			$(".sliding_posters").animate({left: "+=490"});
		count--;
		if(count == 0)
			$("#left").hide();
	});
	$("#left").hide();
	
});
</script>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
    <section id="user-info">
		<div id="username"><?php echo $user_view_info['first_name'] . "'s Profile"; ?></div>
		<div id="cc">
			<div id="cc-header">Average Rating</div>
			<div id="cc-rating" class = "blue movie_stuff">
				<?php
					$average = mysql_fetch_array(mysql_query("SELECT AVG(rating) as average FROM movie_ratings WHERE user_id = $user_id"));
					echo round($average['average'],2);
				?>
			</div>
			<div id="cc-rating" class = "green tv_stuff">
				<?php
					$average = mysql_fetch_array(mysql_query("SELECT AVG(rating) as average FROM tv_ratings WHERE user_id = $user_id"));
					echo round($average['average'],2);
				?>
			</div>
			<div id="cc-rating" class = "red book_stuff">
				<?php
					$average = mysql_fetch_array(mysql_query("SELECT AVG(rating) as average FROM book_ratings WHERE user_id = $user_id"));
					echo round($average['average'],2);
				?>
			</div>
			<div id="cc-rating" class = "yellow vg_stuff">
				<?php
					$average = mysql_fetch_array(mysql_query("SELECT AVG(rating) as average FROM vg_ratings WHERE user_id = $user_id"));
					echo round($average['average'],2);
				?>
			</div>
			<div id="cc-rating" class = "purple music_stuff">
				<?php
					$average = mysql_fetch_array(mysql_query("SELECT AVG(rating) as average FROM music_ratings WHERE user_id = $user_id"));
					echo round($average['average'],2);
				?>
			</div>
			<span id="level">
				<?php echo $level[0] . "(<span id='progress'>" . $user_view_info['score'] . "/" . $level[1] . "</span>)"; ?>
			</span>
		</div>
		<div id="stats" class = "blue movie_stuff">
			<div class="stat-left"><?php echo $movie_num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $movie_num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $movie_num_reviews; ?></div>
			Reviews<br />
		</div>
		<div id="stats" class = "green tv_stuff">
			<div class="stat-left"><?php echo $tv_num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $tv_num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $tv_num_reviews; ?></div>
			Reviews<br />
		</div>
		<div id="stats" class = "red book_stuff">
			<div class="stat-left"><?php echo $book_num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $book_num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $book_num_reviews; ?></div>
			Reviews<br />
		</div>
		<div id="stats" class = "yellow vg_stuff">
			<div class="stat-left"><?php echo $vg_num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $vg_num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $vg_num_reviews; ?></div>
			Reviews<br />
		</div>
		<div id="stats" class = "purple music_stuff">
			<div class="stat-left"><?php echo $music_num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $music_num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $music_num_reviews; ?></div>
			Reviews<br />
		</div>
		
		<div id="followers">
			<span id="following">People Following Me:</span>
			<?php
				$query = mysql_fetch_array(mysql_query("SELECT count(*) as c FROM relationships WHERE user_id1 = $user_view"));
				echo $query['c'];
			?> 
			<span id="more">
				<?php if($num_followers > 5) 
					echo "and " . ($num_followers-5) . " more."; ?>
			</span>
		</div>
		<div id="followers">
			<span id = "following">User Since</span>
			<?php
				$get = mysql_query("SELECT * FROM users WHERE id = $user_view");
				$user = mysql_fetch_array($get);
				echo date("n/j/y",$user['since']);
			?>
		</div>
		<div class = "blue movie_stuff">
		    <a href="all_ratings.php?type=movie">See All My Movie Ratings</a>
		</div>
		<div class = "green tv_stuff">
		    <a href="all_ratings.php?type=tv">See All My Television Ratings</a>
		</div>
		<div class = "red book_stuff">
		    <a href="all_ratings.php?type=book">See All My Book Ratings</a>
		</div>
		<div class = "yellow vg_stuff">
		    <a href="all_ratings.php?type=vg">See All My Game Ratings</a>
		</div>
		<div class = "purple music_stuff">
		    <a href="all_ratings.php?type=music">See All My Music Ratings</a>
		</div>
		<?php
			if (loggedin())
			{
				if($user_id != $user_view)
				{
					$query = mysql_query("SELECT * FROM relationships WHERE user_id1 =" . $user_id . " AND user_id2 = $user_view");
					$check3 = mysql_num_rows($query); 
					
					if($check3 == 0)
					{
						?>
						<form method="post" action= "follow.php">
						<input type="hidden" name="uid1" value="<?php echo $user_id; ?>">
						<input type="hidden" name="uid2" value="<?php echo $user_view; ?>">
						<button type="button" name="follow" onclick="this.form.submit()">Follow</button>
						</form>
						<?php
					}
					
					else
					{
						?>
						<form method="post" action= "unfollow.php">
						<input type="hidden" name="uid1" value="<?php echo $user_id; ?>">
						<input type="hidden" name="uid2" value="<?php echo $user_view; ?>">
						<button type="button" name="unfollow" onclick="this.form.submit()">Unfollow</button>
						</form>
						<?php
					}
				}
			}
			else
			{
				echo "";
			}
		?>
    </section>
		<section id="user-rated">
		<div id="top">
			<div class="blue movie_stuff" id="title">Movies Rated</div>
			<div class="green tv_stuff" id="title">Televison Shows Rated</div>
			<div class="red book_stuff" id="title">Books Rated</div>
			<div class="yellow vg_stuff" id="title">Video Games Rated</div>
			<div class="purple music_stuff" id="title">Music Albums Rated</div>
			
			<ul style="padding: 5px 0 0 0;" class="content-filter right">
				<li class="blue dots"><a>Movies</a></li>
				<li class="green dots"><a>Television</a></li>
				<li class="red dots"><a>Books</a></li>
				<li class="yellow dots"><a>Games</a></li>
				<li class="purple dots"><a>Music</a></li>
				
			  </ul>
		</div>
		 <div id="rated-posters" class = "movie_stuff" style="text-align:center">
		 <?php
		 	$query = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
		 	while($row = mysql_fetch_array($query))
		 	{
		 		$get = mysql_query("SELECT * FROM movie WHERE movie_id = " . $row['movie_id']);
		 		$movie = mysql_fetch_array($get);
		 		echo "<a href='content.php?type=movie&id=" . $row['movie_id'] . "'><img src='../movies/images/thumbs/" . $movie['poster'] . "' /></a>";
		 	}
		 ?>
		 </div>
		 <div id="rated-posters" class = "tv_stuff" style="text-align:center">
		 <?php
		 	$query = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
		 	while($row = mysql_fetch_array($query))
		 	{
		 		$get = mysql_query("SELECT * FROM tv WHERE tv_id = " . $row['tv_id']);
		 		$tv = mysql_fetch_array($get);
		 		echo "<a href='content.php?type=tv&id=" . $row['tv_id'] . "'><img src='../television/images/thumbs/" . $tv['poster'] . "' /></a>";
		 	}
		 ?>
		 </div>
		 <div id="rated-posters" class = "book_stuff" style="text-align:center">
		 <?php
		 	$query = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
		 	while($row = mysql_fetch_array($query))
		 	{
		 		$get = mysql_query("SELECT * FROM book WHERE book_id = " . $row['book_id']);
		 		$book = mysql_fetch_array($get);
		 		echo "<a href='content.php?type=book&id=" . $row['book_id'] . "'><img src='../books/images/thumbs/" . $book['poster'] . "' /></a>";
		 	}
		 ?>
		 </div>
		 <div id="rated-posters" class = "vg_stuff" style="text-align:center">
		 <?php
		 	$query = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
		 	while($row = mysql_fetch_array($query))
		 	{
		 		$get = mysql_query("SELECT * FROM vg WHERE vg_id = " . $row['vg_id']);
		 		$vg = mysql_fetch_array($get);
		 		echo "<a href='content.php?type=vg&id=" . $row['vg_id'] . "'><img src='../videogames/images/thumbs/" . $vg['poster'] . "' /></a>";
		 	}
		 ?>
		 </div>
		 <div id="rated-posters" class = "music_stuff" style="text-align:center">
		 <?php
		 	$query = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 21");
		 	while($row = mysql_fetch_array($query))
		 	{
		 		$get = mysql_query("SELECT * FROM music WHERE music_id = " . $row['music_id']);
		 		$music = mysql_fetch_array($get);
		 		echo "<a href='content.php?type=music&id=" . $row['music_id'] . "'><img src='../music/images/thumbs/" . $music['poster'] . "' /></a>";
		 	}
		 ?>
		 </div>
    </section>
	<section id="user-activity">
		<div id="top">
			<div class="blue" id="title">My Recent Activity</div>
			<!-- 1 = comment 2 = review 3 = rating -->
		</div>
		<?php
			$query = mysql_query("SELECT * FROM events WHERE user_id = $user_view ORDER BY date DESC LIMIT 4 ");
			while($row = mysql_fetch_array($query))
			{
				if($row['type'] == 1)
					$type = "movie";
				if($row['type'] == 2)
					$type = "tv";
				if($row['type'] == 3)
					$type = "book";
				if($row['type'] == 4)
					$type = "vg";
				if($row['type'] == 5)
					$type = "music";
				$get = mysql_query("SELECT * FROM $type WHERE " . $type . "_id = " . $row['content_id'] . "");
				$item = mysql_fetch_array($get);
				$time = time_since_approx(time() - $row['date']);
				if($row['object'] == 1)//comment
				{	
					echo "<b>Commented on </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";	
				}
				if($row['object'] == 2)//review
				{
					echo "<b>Reviewed </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";
				}
				if($row['object'] == 3)//rate
				{
					echo "<b>Rated </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";	
				}
				echo "<i>" . $time . "</i></br></br>";
			}
		?>
	<a href="/activity_history.php">My History</a>
    </section>
	</div>
	<footer id="global-footer" class="clearfix" style="padding-top: 40px;">
    <div class="container">
      <div class="clearfix">
        <section id="user-reviews" class="clearfix left" style="width: 600px;">
          <h1>My Reviews</h1>
		  <div id="the-reviews" class = "movie_stuff">
			 <?php
          		$query = mysql_query("SELECT * FROM movie_reviews WHERE user_id = $user_view ORDER BY score DESC");
      			$num_reviews = mysql_num_rows($query);
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				
      				$get = mysql_query("SELECT * FROM movie WHERE movie_id = " . $review['movie_id']);
      				$movie = mysql_fetch_array($get);
      				
      				echo "<article>";
      				echo "<a href='content.php?type=movie&id=" . $review['movie_id'] . "'>" . $movie['movie_name'] . "</a></br>";
      				echo "Score: <span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
      				echo "<p>" . stripslashes($review['review']) . "</br></p>";
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "tv_stuff">
			 <?php
          		$query = mysql_query("SELECT * FROM tv_reviews WHERE user_id = $user_view ORDER BY score DESC");
      			$num_reviews = mysql_num_rows($query);
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				
      				$get = mysql_query("SELECT * FROM tv WHERE tv_id = " . $review['tv_id']);
      				$tv= mysql_fetch_array($get);
      				
      				echo "<article>";
      				echo "<a href='content.php?type=tv&id=" . $review['tv_id'] . "'>" . $tv['tv_name'] . "</a></br>";
      				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
      				echo "<p>" . stripslashes($review['review']) . "</br></p>";
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "book_stuff">
			 <?php
          		$query = mysql_query("SELECT * FROM book_reviews WHERE user_id = $user_view ORDER BY score DESC");
      			$num_reviews = mysql_num_rows($query);
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				
      				$get = mysql_query("SELECT * FROM book WHERE book_id = " . $review['book_id']);
      				$book= mysql_fetch_array($get);
      				
      				echo "<article>";
      				echo "<a href='content.php?type=book&id=" . $review['book_id'] . "'>" . $book['book_name'] . "</a></br>";
      				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
      				echo "<p>" . stripslashes($review['review']) . "</br></p>";
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "vg_stuff">
			 <?php
          		$query = mysql_query("SELECT * FROM vg_reviews WHERE user_id = $user_view ORDER BY score DESC");
      			$num_reviews = mysql_num_rows($query);
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				
      				$get = mysql_query("SELECT * FROM vg WHERE vg_id = " . $review['vg_id']);
      				$vg= mysql_fetch_array($get);
      				
      				echo "<article>";
      				echo "<a href='content.php?type=vg&id=" . $review['vg_id'] . "'>" . $vg['vg_name'] . "</a></br>";
      				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
      				echo "<p>" . stripslashes($review['review']) . "</br></p>";
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "music_stuff">
			 <?php
          		$query = mysql_query("SELECT * FROM music_reviews WHERE user_id = $user_view ORDER BY score DESC");
      			$num_reviews = mysql_num_rows($query);
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] >= 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				
      				$get = mysql_query("SELECT * FROM music WHERE music_id = " . $review['music_id']);
      				$music= mysql_fetch_array($get);
      				
      				echo "<article>";
      				echo "<a href='content.php?type=music&id=" . $review['music_id'] . "'>" . $music['music_name'] . "</a></br>";
      				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
      				echo "<p>" . stripslashes($review['review']) . "</br></p>";
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here. Be the first to write a review!</i>";
          ?>
		  </div>
        </section>
        
        <section id="common" class="clearfix right" style="padding: 3em, 0;">
          <h1> <b>
          <?php
            echo "<a href=\"suggestions.php\"><img src=\"/images/green_right_arrow.png\">  Go to My Suggestions</a>";
          ?></b></h1>
          <h1> My Groups </h1>
          <?php
                $get = mysql_query("SELECT g.group_name AS g_name, g.group_id AS g_id
                                    FROM groups AS g
                                    INNER JOIN group_relations AS r ON g.group_id = r.group_id
                                    WHERE r.user_id = $user_id");
                while ($row = mysql_fetch_array($get))
                {
                    echo "<a href=\"groups.php?id=" . $row['g_id'] . "\">" . $row['g_name'] . "</a></br>";
                }
          ?>
          <h1>People I Follow</h1>
          <?php
                $get = mysql_query("SELECT u.username as name, u.id as id
                                    FROM users as u INNER JOIN relationships as r
                                    ON u.id = r.user_id1
                                    WHERE r.user_id2 = $user_id
                                    ORDER BY u.username");
                while($row = mysql_fetch_array($get))
                    echo "<a href=\"users.php?id=" . $row['id'] . "\">" . $row['name'] . "</a></br>";
          ?>
        </section>
        
        <script type="text/javascript">
			$(".tv_stuff").hide();
			$(".vg_stuff").hide();
			$(".book_stuff").hide();
			$(".music_stuff").hide();
			
			$(".dots").click(function()
			{
				var str = $(this).attr("class");
				$('.movie_stuff').hide();
				$(".tv_stuff").hide();
				$(".vg_stuff").hide();
				$(".book_stuff").hide();
				$(".music_stuff").hide();
				if(str == "blue dots")
					$(".movie_stuff").fadeIn(500);
				if(str == "green dots")
					$(".tv_stuff").fadeIn(500);
				if(str == "red dots")
					$(".book_stuff").fadeIn(500);
				if(str == "yellow dots")
					$(".vg_stuff").fadeIn(500);
				if(str == "purple dots")
					$(".music_stuff").fadeIn(500);
			});
		</script>
        <script>
       		 $(document).ready(function() {
  				$("#your-cranium ul.content-filter a").click(function(e) {
    			// Prevent href attribute from firing
    			e.preventDefault();

   				 // Unset the current active item
   				 $("#your-cranium ul.content-filter li.active").removeClass("active");

    			// Get the clicked on class to switch out content
    			var class = $(this).parent("li").attr("class");

    			// Fadeout old content
    			$("#your-cranium div.active").fadeOut("fast", function() {
    			// Unset this as active content
    			$(this).removeClass("active");
    			// Fade in new content based on our class from above
    			$("#your-cranium div." + class).fadeIn("fast", function() {
    	  		// Set this as the active content
    	  		$(this).addClass("active");
    			});
    		});
  		});
		});
		</script>
      </div>
      <nav class="right">
        <ul>
          <li><a href="privacypolicy.php">Privacy Policy</a></li>
          <li><a href="useragreement.php">Terms of Use</a></li>
          <li class="last"><a href="mailto:team@criticrania.com">Contact Us</a></li>
        </ul>
      </nav>
      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
    </div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->

<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
