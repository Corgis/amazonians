<?php
include 'functions.php';
if (isset($_GET['logout']))
{
	$_SESSION = array();
	if ($_COOKIE[session_name()])
	{
		setcookie(session_name(), '', time()-42000, '/');
	}
	
	if(isset($_COOKIE['username']))
	{
		setcookie('username', '', time()-42000);
		setcookie('id', '', time()-42000);
	}
	
	session_destroy();
	header("Location: http://www.criticrania.com");
}
?>