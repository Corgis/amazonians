<?php

require_once 'functions.php';

class FunctionsTest extends PHPUnit_Framework_TestCase
{
  /**
   * @dataProvider thresholds
   */
  public function testLevel2($user_id, $score, $ans)
  {
    $arr = level2($user_id,$score);
    $this->assertEquals($ans[0],$arr[0]);
    $this->assertEquals($ans[1],$arr[1]);
  }
  
  public function thresholds()
  {
    return array(
       array(1,23,array('Beginner',100)),
       array(1,100,array('Deliberater',300)),
       array(1,102,array('Deliberater',300)),
       array(1,300,array('Classifier',500)),
       array(1,400,array('Classifier',500)),
       array(1,500,array('Evaluator',750)),
       array(1,750,array('Examiner',1000)),
       array(1,1000,array('Reviewer',1300)),
       array(1,1300,array('Analyzer',1750)),
       array(1,1750,array('Connoisseur',2250)),
       array(1,2250,array('Critic',3000)),
       array(1,3000,array('Criticranium',10000)),
       array(1,10001,array('Criticranium',10000)),
    );
  }
}

?>