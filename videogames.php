<?php
	include 'functions.php';
	include 'top.php';
	$type ="vg";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Video Games</title>
<?php
	include 'sheetshome.php';
?>
</head>
<body>
<!-- start banner -->
<?php
	//banner(4);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style="background:url(images/body-bg.gif)">
	<!-- Title + informative blurb -->
	<div id="title" style = "color:#db9e36">Video Games<br /></div>
	<div id="description" style = "color:#db9e36">
		Whether you're a hardcore gamer from the new age, or one from the days of Pong, we have the greatest sources of entertainment in
		today's world. Ranging all genres, Call of Duty to Wii sports, decide which games were the most fun, and those you wish
		you hadn't wasted your time playing.
	</div><br />	
	<!-- Specific content for video game homepage -->
	<div id="content"  style="background:url(images/body-bg.gif)">
		<div id="top_left"><br /><br />
			<!-- Include NIVO slider. Top Ten obtains the ten highest average ratings of games in the past week (See functions) -->
			<div class="head">This Week's Top Rated Videogames</div><br />
			<div id="slider">
				<?php          
					top_ten("vg");
				?>
			</div>
		</div>
		<!-- 3 Most Recent game reviews, including picture and author of review -->
		<div id="top_right"><br />
			<div class="head">Recent Reviews</div><br />
				<?php
					//Get all game reviews ordered by date (most recent first)									
					$check = mysql_query("SELECT * FROM vg_reviews ORDER BY date DESC")or die();
					
					echo "<table width='100%' cellspacing='8px'>";
					$recent = 0;
					//Only deal with the 3 most recent reviews
					while($recent < 3)
					{
						//Get Review
						$row = mysql_fetch_array($check);
						//Get username of review
						$usercheck = mysql_query("SELECT * FROM users WHERE id = '". $row['user_id'] ."'")or die();
						$username = mysql_fetch_array($usercheck);
						//Obtain actual review, add slashes to be able to check vg table
						$review1 = addslashes($row['review']);
						//Find game that correlates to written review
						$vg_info = mysql_query("SELECT * FROM vg_reviews WHERE review = '$review1'");
						//Determine length of the review
						$length = strlen($review1);
						//Remove slashes to create readable text
						$review1 = stripslashes($review1);
						$review1 = stripslashes($review1);
						//Obtain game information (it's row of information)
						$vg_row = mysql_fetch_array($vg_info);
						$vg_id = $vg_row['vg_id'];	
						$line2 = mysql_query("SELECT * FROM vg_reviews WHERE vg_id = '$vg_id' ORDER BY date DESC")or die();
						$vg_info = mysql_query("SELECT * FROM vg WHERE vg_id = '$vg_id'")or die();
						$vg_row = mysql_fetch_array($vg_info);
						//Display game title with a link to its specific content page
						echo "<tr valign='top'><td width='80%'>";
						echo "<hr><movietitle><a href='/content.php?type=vg&id=$vg_id' style = 'color:#db9e36'>" .$vg_row['vg_name']. "</a></movietitle><hr><br />";
						//Display Author of review, including a link to their profile page
						echo "<a href= '/users.php?id=" . $row['user_id'] . "' color='black' >" . $username['username'] . "</a> - ";
						//Obtain game's image
						$poster = $vg_row['poster'];
						$filename = "videogames/images/$poster";
						
						/*Check if length of review is too long to display, if so display only 150 characters and offer to allow user to read more, else display review*/
						if ($length > 180)
						{
							$review2 = substr($review1, 0, 150);
							echo "\"$review2...\" <a href ='content.php?type=vg&id=$vg_id'>Read more</a>";
						}
						else
							echo "\"$review1\"";
						//Check if thumbnail exists, if not, create it
						if (file_exists($filename))
						{
							$image = "<br><td><a href='content.php?type=vg&id=$vg_id'><img id='poster' src='videogames/images/$poster' /></a>";
						}
						else
						{
							$image = "<br><td><a href='content.php?type=vg&id=$vg_id'><img id='poster' src='images/no_image_thumb.png' /></a>";
						}
						//Disaply thumbnail of game image
						echo $image;
						echo "</td></tr>";
						//Increment to next most recent review
						$recent++;
					}
					echo "</table>";
				?>
				&nbsp;
		</div>
		<div id="bottom_left"><br />
			<!-- Display tuhumbnails of 5 most visited vg content pages in last hour (see Functions) -->
			<div class="head">Trending Now</div>
			<div style="text-align: center; color:#db9e36">Here are the 5 most visited pages in the last hour</div><br />
				<?php
					trending("vg");
				?>
		</div>
		<div id="bottom_right"><br />
			<!-- Allow users to answer a poll question, display results after they have voted (see poll.php) -->
			<?php include 'poll.php'; ?>
		</div>
	</div>
	&nbsp;
</div><br />
<!-- end page -->

<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>
