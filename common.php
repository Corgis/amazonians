<?php include 'functions.php';  
	  include 'top.php';

      $users_id = mysql_escape_string($_GET['id']);
      $usercheck = mysql_query("SELECT * FROM users WHERE id = $users_id")or die(mysql_error());
      $name = mysql_fetch_array($usercheck);
      $username = $name['username'];
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
    <div id="content" style="padding-left: 20px;">
		<font id="movietitle">Common Ratings between You and <?php echo $username; ?> </font><br /><br />
		Click the different headings to sort them<br /><br />
		<table cellspacing="0" width='600px' id="table" class="tinytable" style="margin-bottom: 20px;">
			<thead>
				<tr>
					<th width =50%><font id="tabletop">Title</font> </th>
					<th width =20%><font id="tabletop">Type</font> </th>
					<th width =10%><font id="tabletop">You</font> </th>
					<th width =10%><font id="tabletop">Them</font> </th>
					<th width =10$><font id="tabletop">Difference</font> </th>
				</tr>
			</thead>
			<tbody>
				<?php 
					ratings_compare((int)$_SESSION['id'], (int)$users_id, $num, $avg, true, "movie");
					ratings_compare((int)$_SESSION['id'], (int)$users_id, $num, $avg, true, "tv");
					ratings_compare((int)$_SESSION['id'], (int)$users_id, $num, $avg, true, "book");
					ratings_compare((int)$_SESSION['id'], (int)$users_id, $num, $avg, true, "vg");
					ratings_compare((int)$_SESSION['id'], (int)$users_id, $num, $avg, true, "music");
				?>
			</tbody>
		</table>

			<script type="text/javascript" src="script.js"></script> 
			<script type="text/javascript"> 
			  var sorter = new TINY.table.sorter('sorter','table',{
				headclass:'head',
				ascclass:'asc',
				descclass:'desc',
				evenclass:'evenrow',
				oddclass:'oddrow',
				evenselclass:'evenselected',
				oddselclass:'oddselected',
				paginate:true,
				size:1000,
				sortcolumn:4,
				sortdir:1,
				init:true
				});
			</script>
	</div>
</div>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
