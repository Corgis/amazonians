<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style="color:#DB9E36; text-align:center;">
    <?php 
    $user_id = mysql_real_escape_string($_POST['user_id']);
    $amount = mysql_real_escape_string($_POST['amount']);
    $type = mysql_real_escape_string($_POST['type']);
    $content_id = mysql_real_escape_string($_POST['content_id']);
    
    $query = mysql_query("SELECT * FROM users WHERE id = $user_id");
    $user = mysql_fetch_array($query);

    $query = mysql_query("SELECT * 
    FROM " . $type . "_ratings
    WHERE user_id = $user_id
    AND
     " . $type . "_id = $content_id");

    $content_rating = mysql_fetch_array($query);
    $id = $content_rating[$type . '_id'];

    $query = mysql_query("SELECT * FROM $type WHERE " . $type . "_id = " . $content_rating[$type . '_id']);
    $result = mysql_fetch_array($query);
    

    echo "<h1 style='color:#3278AF'>" . $result[$type . '_name'] . " (" . $result['year'] . ")</h1></br></br>";

    if($type == "movie")
        $type2 = "movies";
    if($type == "tv")
        $type2 = "television";
    if($type == "book")
        $type2 = "books";
    if($type == "vg")
        $type2 = "videogames";
    if($type == "music")
        $type2 = "music";
    echo "<img src = $type2/images/" . $result['poster'] . "></br></br>";
    echo "<b>You guessed: " . $amount . "</b></br>";
    echo "</br> You actually gave this a(n) " . $content_rating['rating'] . "</br>";
    if($content_rating['rating'] == $amount)
    {
        if($content_rating['game_score'] == 1)
            {
                //check off that this movie has been guessed perfectly in the challenge
                mysql_query("UPDATE " . $type . "_ratings 
                SET game_score = 2
                WHERE " . $type . "_id = $content_id and user_id = $user_id");
                //add 1 to user's score (they already got 1 for this item)
                update_user_score($user_id, 1);
                echo "Added 1!</br>";
            }
        elseif($content_rating['game_score'] == 0)
            {
                //check off that this movie has been guessed perfectly in the challenge
                mysql_query("UPDATE " . $type . "_ratings 
                SET game_score = 2
                WHERE " . $type ."_id = $content_id and user_id = $user_id");
                //add 2 to user's score
                update_user_score($user_id, 2);
                echo "Added 2!</br>";
            }
        echo "Good call!";
    }
    elseif(abs($content_rating['rating'] - $amount) <= .5)
    {
        
        if($content_rating['game_score'] == 0)
            {
                //check off that this movie has been guessed close to
                mysql_query("UPDATE " . $type . "_ratings SET game_score = 1 WHERE " . $type . "_id = $content_id and user_id = $user_id");
                //add 1 to user's score 
                update_user_score($user_id, 1);
                echo "Added 1!</br>";
            }
        echo "Pretty darn close.";
    }
    elseif(abs($content_rating['rating'] - $amount) <= 1)
    {
        echo "You were sorta close.";
    }
    elseif(abs($content_rating['rating'] - $amount) <= 3)
    {
        echo "You're in the very general area.";
    }
    else
    {
        echo "You might want to rethink this a bit...";
    }
    ?>
    </br></br></br>
    Do you want to re-rate this? Go to <a href="content.php?type=<?php echo $type; ?>&id=<?php echo $content_id; ?>" style='color:#3278AF'> <?php echo $type2; ?></a>!</br>
    </br>
    <form  method="post" action="criticrania_challenge.php">
         <button type='button' name='next' onclick='this.form.submit()'>Try a New One</button>
    </form>
    </br></br>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>