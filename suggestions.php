<?php include 'functions.php';  
	include 'top.php';

	$user_id = (int)$_SESSION['id'];
	$type  = mysql_real_escape_string($_GET['type']);
	if($type == null)
		$type = "movie";

	$ratings_table = $type . "_ratings";
	$name = $type . "_name";
	$id = $type . "_id";
	$genre = $type . "_genre";

	$top_genre = mysql_query("  SELECT c." . $name . " as name, AVG( r.rating ), c.genre
								FROM " . $type . " AS c
								JOIN " . $ratings_table . " AS r ON c." . $id ." = r.". $id . "
								WHERE genre
								REGEXP (

								SELECT c1.genre
								FROM " . $ratings_table . " AS r1
								JOIN " . $type ." AS c1 ON r1." . $id . " = c1." . $id . "
								WHERE r1.user_id = " . $user_id . "
								GROUP BY c1.genre
								ORDER BY COUNT( c1.genre ) DESC 
								LIMIT 1
								)
								AND r." . $id . " NOT 
								IN (

								SELECT " . $id . "
								FROM " . $ratings_table . "
								WHERE user_id = " . $user_id . "
								)
								GROUP BY r." . $id . "
								HAVING COUNT( r." . $id . ") >3
								ORDER BY AVG( r.rating ) DESC
								LIMIT 5");

	$follower_recs = mysql_query("  SELECT c." . $name . " as name, AVG( r.rating ) 
									FROM " . $ratings_table . " AS r
									JOIN relationships AS r1 ON r.user_id = r1.user_id1
									JOIN " . $type . " AS c ON r." . $id . " = c." . $id . "
									WHERE r1.user_id2 = " . $user_id . "
									AND c." . $id . " NOT 
									IN (
									SELECT c1." . $id . "
									FROM " . $ratings_table . " AS c1
									WHERE c1.user_id = " . $user_id . "
									)
									GROUP BY r." . $id . "
									HAVING COUNT( r." . $id . " ) >3
									ORDER BY AVG( r.rating ) DESC 
									LIMIT 5");
	if($type == "movie")
	{
		$genre_specific = mysql_query(" SELECT ratings.movie_id, results.movie_name
										FROM movie_ratings AS ratings
										JOIN (

										SELECT mo.movie_id, mo.movie_name
										FROM movie AS mo, (

										SELECT actor, r.movie_id, COUNT( * ) 
										FROM movie_ratings AS r
										JOIN (

										SELECT movie_id, actor1 AS actor
										FROM movie
										UNION ALL 
										SELECT movie_id, actor2 AS actor
										FROM movie
										UNION ALL 
										SELECT movie_id, actor3 AS actor
										FROM movie
										) AS m ON m.movie_id = r.movie_id
										WHERE r.user_id = $user_id
										AND m.actor <>  \"\"
										GROUP BY actor
										ORDER BY COUNT( * ) DESC 
										LIMIT 5
										) AS top_actors
										WHERE top_actors.actor = mo.actor1
										OR top_actors.actor = mo.actor2
										OR top_actors.actor = mo.actor3
										) AS results ON results.movie_id = ratings.movie_id
										WHERE ratings.movie_id NOT 
										IN (

										SELECT movie_id
										FROM movie_ratings
										WHERE user_id = $user_id
										)
										GROUP BY ratings.movie_id
										HAVING COUNT( * ) >3
										ORDER BY AVG( ratings.rating ) DESC
										LIMIT 5");
	}
	else if($type == "tv")
	{
		$genre_specific = mysql_query(" SELECT ratings.tv_id, results.tv_name
										FROM tv_ratings AS ratings
										JOIN (

										SELECT tt.tv_id, tt.tv_name
										FROM tv AS tt, (

										SELECT actor, r.tv_id, COUNT( * ) 
										FROM tv_ratings AS r
										JOIN (

										SELECT tv_id, actor1 AS actor
										FROM tv
										UNION ALL 
										SELECT tv_id, actor2 AS actor
										FROM tv
										UNION ALL 
										SELECT tv_id, actor3 AS actor
										FROM tv
										) AS t ON t.tv_id = r.tv_id
										WHERE r.user_id = $user_id
										AND t.actor <>  \"\"
										GROUP BY actor
										ORDER BY COUNT( * ) DESC 
										LIMIT 5
										) AS top_actors
										WHERE top_actors.actor = tt.actor1
										OR top_actors.actor = tt.actor2
										OR top_actors.actor = tt.actor3
										) AS results ON results.tv_id = ratings.tv_id
										WHERE ratings.tv_id NOT 
										IN (

										SELECT tv_id
										FROM tv_ratings
										WHERE user_id = $user_id
										)
										GROUP BY ratings.tv_id
										HAVING COUNT( * ) >3
										ORDER BY AVG( ratings.rating ) DESC
										LIMIT 5");
	}

?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
		<ul style="padding: 5px 0 0 0;" class="content-filter right">
			<li class="blue dots"><a>Movies</a></li>
			<li class="green dots"><a>Television</a></li>
			<li class="red dots"><a>Books</a></li>
			<li class="yellow dots"><a>Games</a></li>
			<li class="purple dots"><a>Music</a></li>
				
		</ul>

		<div class = "blue" id = "top_genre">
			<?php 
			while($row = mysql_fetch_array($top_genre))
			{
				echo $row['name'] . "</br>";
			}
			?>
		</div>
	</br></br>
	<div class = "blue" id = "genre_specific">
			<?php 
			if(mysql_num_rows($genre_specific) == 0)
				echo "You need to rate more things to get a good suggesiton here!" . "</br>";
			else
			{
				while($row = mysql_fetch_array($genre_specific))
				{
					echo $row['movie_name'] . "</br>";
				}
			}
			?>
		</div>
	</br></br>
		<div class = "blue" id = "follower_recs">
			<?php 
			while($row = mysql_fetch_array($follower_recs))
			{
				echo $row['name'] . "</br>";
			}
			?>
		</div>
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->
<script type="text/javascript" src="js/libs/jquery-1.6.2.min.js"></script>
<script type="text/javascript">
			$(".dots").click(function()
			{
				//Loading gif is as simple as these lines
				//Empty all the divs and insert the loading gif in the div you so desire
				$("#top_genre").empty();
				$("#genre_specific").empty().html('<img style="position: fixed; top: 35%; left: 50%" src="images/loading7.gif" />');
				$("#follower_recs").empty();

				var str = $(this).attr("class");
				var type = null;
				if(str == "blue dots")
				{
					type = "movie";
					document.getElementById("top_genre").className="blue";
					document.getElementById("genre_specific").className="blue";
					document.getElementById("follower_recs").className="blue";
				}
				if(str == "green dots")
				{
					type = "tv";
					document.getElementById("top_genre").className="green";
					document.getElementById("genre_specific").className="green";
					document.getElementById("follower_recs").className="green";
				}
				if(str == "red dots")
				{
					type = "book";
					document.getElementById("top_genre").className="red";
					document.getElementById("genre_specific").className="red";
					document.getElementById("follower_recs").className="red";
				}
				if(str == "yellow dots")
				{
					type = "vg";
					document.getElementById("top_genre").className="yellow";
					document.getElementById("genre_specific").className="yellow";
					document.getElementById("follower_recs").className="yellow";
				}
				if(str == "purple dots")
				{
					type = "music";
					document.getElementById("top_genre").className="purple";
					document.getElementById("genre_specific").className="purple";
					document.getElementById("follower_recs").className="purple";
				}

				var address = "suggestions.php?type="+type;
				$.get(address, function(data) {
					var top_genre = $(data).find('#top_genre').html();
					var g_spec = $(data).find('#genre_specific').html();
					var f_rec = $(data).find('#follower_recs').html();

					document.getElementById("top_genre").innerHTML = top_genre;
					document.getElementById("genre_specific").innerHTML = g_spec;
					document.getElementById("follower_recs").innerHTML = f_rec;
				});
			});
</script>
</body>
</html>
