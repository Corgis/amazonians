<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style ="color:#CCC;font: 'Comfortaa', sans-serif;">
	<h1>About</h1><br />
		<div>
			<b><u>Criticrania.com:</b></u> This site is made great by the people who use it. Higher numbers of ratings, comments, and reviews will result in this site thriving. People can share
			their ideas with everyone, and get ideas from everyone else. Wanna see if a movie is worth the 29 dollars or whatever it is to see a movie nowadays? Check out its rating here first.
			People love being heard and here is a place where they can do just that. Think a movie wasn't that good? Go ahead and rate it a 3.7. Think that video game was the best game you've ever
			played? Write a	raving review about it. Whatever you decide to use this site for, the more you use it, the better it gets.<br />
			Want to know more about how Criticrania works? Check out our list of <b><a href="features.php" style="color:#CCC">features </a></b> or join the community 
			<a href="discussion.php" style="color:#CCC"><b>discussion</b></a>!<br />

		</div>
	
		<h2>Contact Us</h2>
		<div>
			Are we missing something? Did you find a bug? Just wanna have a nice conversation
			with one of us? Send us an e-mail at <b><a href="mailto:team@criticrania.com" style="color:#CCC">team@criticrania.com</a></b> and we'll respond as soon as possible. 
			Unless it's a bug fix. Then I think we're busy that day.
			<br /><br />
		</div>	
		<h2>Info</h2>
		<div>
			Criticrania is being built by a team of four Undergraduates in college. The development team consists of Matt (<b><a href="users.php?id=3" style="color:#CCC">Betten</b></a>),
			Matt (<a href="users.php?id=1" style="color:#CCC"><b>Keller</b></a>), and Matt (<a href="users.php?id=2" style="color:#CCC"><b>Wing</b></a>) with various degress of external help. 
			Ryan (<a href="users.php?id=4" style="color:#CCC"><b>Doherty</b></a>) is in charge
			of the businees end of things as Criticrania grows. Criticrania is currently being operated out of Troy, New York.
		</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="privacypolicy.php">Privacy Policy</a></li>
			  <li><a href="useragreement.php">Terms of Use</a></li>
			  <li class="last"><a href="mailto:team@criticrania.com">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
