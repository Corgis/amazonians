<?php include 'functions.php';  
	include 'top.php';
	
	if (!loggedin())
		header("Location: /");
	
	if (isset ($_POST["changeuser"]))
	{
		if(loggedin())
		{	
			$old_user = mysql_query("SELECT * FROM users") or die();
			$user_error = "none";
			
			while ($old_user_array = mysql_fetch_array($old_user))
			{
				if($_POST['changeuser']==$old_user_array['username'] && $_SESSION['id'] != $old_user_array['id'])
				{
					$user_error = "taken";
				}
			}
			if ($user_error == "none")
			{
				$newuser = mysql_real_escape_string(htmlentities($_POST["changeuser"]));
				mysql_query("UPDATE users SET username = '$newuser' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
			}
		}
	}
	
	if (isset ($_POST["changefirst"]))
	{
		if(loggedin())
		{
			$newfirst = mysql_real_escape_string(htmlentities($_POST["changefirst"]));
			mysql_query("UPDATE users SET first_name = '$newfirst' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
		}
	}
	
	if (isset ($_POST["changelast"]))
	{
		if(loggedin())
		{
			$newlast = mysql_real_escape_string(htmlentities($_POST["changelast"]));
			mysql_query("UPDATE users SET last_name = '$newlast' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
		}
	}
	
	if (isset ($_POST["changebirth"]))
	{
		if(loggedin())
		{
			$newbirth = mysql_real_escape_string(htmlentities($_POST["changebirth"]));
			//if(preg_match("[0-9]{4}.'-'.[0-9]{2].'-'[0-9]{2}", $newbirth) == 0)
			mysql_query("UPDATE users SET birthdate = '$newbirth' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
		}
	}
	
	if (isset ($_POST["changemail"]))
	{
		$old_mail = mysql_query("SELECT * FROM users") or die();
			
		$mail_error = "none";

		$newmail = mysql_real_escape_string(htmlentities($_POST["changemail"]));

		$query = mysql_query("SELECT * FROM users WHERE email = '$newmail'");
		if(mysql_num_rows($query) > 0)
			$mail_error = "taken";
		
		if (!filter_var($newmail, FILTER_VALIDATE_EMAIL))
			$mail_error = "wrong";

		while ($old_mail_array = mysql_fetch_array($old_mail))
		{
			if($_POST['changeuser']==$old_mail_array['username'] && $_SESSION['id'] != $old_mail_array['id'])
			{
				$mail_error = "taken";
			}
		}
		if ($mail_error == "none")
		{
			mysql_query("UPDATE users SET email = '$newmail' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
		}
	}
	
	if (($_POST["oldpass"]) != "")
	{
		if(loggedin())
		{	
			$old_pass = mysql_query("SELECT * FROM users WHERE id = ".$_SESSION['id']."") or die();
			$old_pass_array = mysql_fetch_array($old_pass);
			
			if($old_pass_array['password'] == md5($_POST['oldpass']))
			{
				if (strlen($_POST['newpass']) < 6)
					$password_error = "short";
				
				else if($_POST['newpass']==$_POST['retypepass'])
				{
					$newpass = mysql_real_escape_string((md5(htmlentities($_POST['newpass']))));
					mysql_query("UPDATE users SET password = '$newpass' WHERE id = ". $_SESSION['id']."") or die(mysql_error());
					$password_error = "none";
				}
				
				else
					$password_error = "match";
			}
			
			else
				$password_error = "wrong";
		}
	}
	
	$user = mysql_query("SELECT * FROM users WHERE id = ".$_SESSION['id']."");
	$user_info = mysql_fetch_array($user);
?>
<body>
<script type="text/javascript">
$(document).ready(function(){
	$(".changed").hide();
	$(".change").toggle(
	function()
	{
		$(this).next(".changed").fadeIn(600);
	},
	function()
	{
		$(this).next(".changed").fadeOut(300);
	});
});
</script>
<div id="container">
  <?php include ("banner.php"); ?>
	<div style="color: #ccc;" id="main" role="main" class="container clearfix">
		<h1 style="color:#3278AF;">Settings</h1>
		<div style="min-height: 570px;" id="description"><br />
			<form id="input" method="post" action="settings.php">
				<div class="inputname">Username: </div><input type="text" name="changeuser" value="<?php echo $user_info['username']; ?>"> <?php if($user_error == "taken"){echo "<i>Sorry, that username is taken.</i>";}?>
				<br /><br />
				<div class="inputname">First Name: </div><input type="text" name="changefirst" value="<?php echo $user_info['first_name']; ?>"><br /><br />
				<div class="inputname">Last Name: </div><input type="text" name="changelast" value="<?php echo $user_info['last_name']; ?>"><br /><br />
				<div class="inputname">Birthdate: </div><input type="text" name="changebirth" value="<?php echo $user_info['birthdate']; ?>">
				<br /><br />
				<div class="inputname">Email Address: </div><input type="email" name="changemail" value="<?php echo $user_info['email']; ?>">
				<?php if($mail_error == "wrong"){echo "<br /><i>Please enter a valid e-mail address.</i>";} else if($mail_error == "taken"){echo "<br /><i>Sorry, that e-mail address is taken.</i>";}?><br /><br />
				<div style="padding: 0 0 10px 0;" class="inputname">Password:</div> <span class="change">(Edit)</span>
				<div class="changed">
					<div style="width: 140px; padding: 0 10px 20px 0;" class="inputname">Previous Password: </div><input type="password" name="oldpass"><br />
					<div style="width: 140px; padding: 0 10px 20px 0;" class="inputname">New Password: </div><input type="password" name="newpass"><br />
					<div style="width: 140px; padding: 0 10px 20px 0;" class="inputname">Retype New Password: </div><input type="password" name="retypepass"><br />
				</div>
				<?php if($password_error == "wrong"){echo "<br /><i>Please enter your old password correctly.</i>";} if($password_error == "match"){echo "<br /><i>Your new passwords do not match.</i>";}if($password_error == "short"){echo "<br /><i>Please enter a password that is at least 6 characters.</i>";} if($password_error == "none"){echo "<br /><i>You have successfully changed your password!</i>";}?><br /><br />
				<div class="inputname"></div><button type='submit' name='submit' onclick='this.form.submit()'>Submit</button>
			</form>
		</div><br />
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
