<?php
include 'functions.php';
$type = mysql_real_escape_string($_GET['type']);
$id = mysql_real_escape_string($_GET['id']);
if(!($type == 'movie' || $type == 'tv' || $type == 'book' || $type == 'vg' || $type == music))
    header("Location: /");
if($id == '' || $id < 1)
    header("Location: /");

$comment_table = $type."_comments";
$review_table = $type."_reviews";
$rating_table = $type."_ratings";
$content_id = $type."_id";
$content_name = $type."_name";
$time = time();

$content_info = mysql_query("SELECT * FROM $type WHERE $content_id = '$id'")or die();
$content_row = mysql_fetch_array($content_info);

if(mysql_num_rows($content_info) < 1)
    {
    header("Location: /");
    echo "DOES NOT EXIST!";
    }
include 'top.php';
if ($content_row[$content_name] == "" || $content_row['checked'] != 1)
	header("Location: http://www.criticrania.com/$type2.php");
else
hit("$type",$id);
if ($type == "movie")
{
	$event_type = 1;
	$type2 = "movies";
	$color_scheme = "movies";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Directed by </b>" . $content_row['director'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "../movies/images/" . $content_row['poster'];
	$thumb = "../movies/images/thumbs/";
	$nice_word = "Movies";
	$color = "blue";
}
else if($type == "tv")
{
	$event_type = 2;
	$type2 = "television";
	$color_scheme = "television";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['creator'] . "</br>".
				  "<b>Starring </b>" . $content_row['actors'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['network'] . " / " . $content_row['minutes'] . " minutes / " . $content_row['genre'] . "</br>";
	$poster = "../television/images/" . $content_row['poster'];
	$thumb = "../television/images/thumbs/";
	$nice_word = "T.V. Shows";
	$color = "green";
}
else if($type == "book")
{
	$event_type = 3;
	$type2 = "books";
	$color_scheme = "book";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Written by </b>" . $content_row['author'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "../books/images/" . $content_row['poster'];
	$thumb = "../books/images/thumbs/";
	$nice_word = "Books";
	$color = "red";
}
else if($type == "vg")
{
	$event_type = 4;
	$type2 = "videogames";
	$color_scheme = "games";
	$description = $content_row['synopsis'];
	$extra_info = "<b>Created by </b>" . $content_row['producer'] . "</br>".
				  "<b>Made for </b>" . $content_row['platforms'] . "</br>";
	$secondary_info = $content_row['rating'] . " / " . $content_row['genre'] . "</br>";
	$poster = "../videogames/images/" . $content_row['poster'];
	$thumb = "../videogames/images/thumbs/";
	$nice_word = "Video Games";
	$color = "yellow";
}
else if($type == "music")
{
	$event_type = 5;
	$type2 = "music";
	$color_scheme = "music";
	$description = stripslashes(str_replace("\n", ", " , $content_row['songs']));
	$extra_info = "<b>Produced by </b>" . $content_row['artist'] . "</br>";
	$secondary_info = $content_row['genre'] . "</br>";
	$poster = "../music/images/" . $content_row['poster'];
	$thumb = "../music/images/thumbs/";
	$nice_word = "Music Albums";
	$color = "purple";
}
?>
<head>
    <title><?php echo $content_row[$content_name]; ?></title>
    <meta name="description" content= "<?php echo $description; ?>" />
    <meta name="image" content = "<?php echo 'http://www.criticrania.com/movies/images/thumbs/' . $content_row['poster']; ?>"/>
    <meta property="og:site_name" content = "Criticrania - The Multimedia Social Network"/>
    <meta property="og:type" content= "<?php echo $type; ?>" />
</head>
<?php
$num = 0;
if (isset ($_POST["comments"]))
{ 
	if(loggedin())
	{
		$message = mysql_real_escape_string(htmlentities($_POST["comments"]));
		
		$offset = 0;
		while(1)
		{
			$start = strpos($message, "@", $offset);
			if($start === false)
				break;
			
			$start++;
			
			for ($end = 13; $end >= 0; $end--) 
			{
				$name = substr($message, $start, $start+$end);
				$at = mysql_query("SELECT * FROM users WHERE username = '$name'") or (die(mysql_error()));
				if(mysql_num_rows($at))
				{
					$at_query = mysql_fetch_array($at);
					$at_id = $at_query['id'];
					$message = str_replace($name,"<a href=\'users.php?id=$at_id\'>$name</a>",$message);
					$offset = $start+$end;
					break;
				}
			}
			$offset += 13;
		}

		$sendcomment = mysql_query("INSERT INTO $comment_table SET $content_id ='$id', comment='$message', date=$time, user_id='".$_SESSION['id']."'");
		
		if ($sendcomment)
		{
			$sendcomment = mysql_query("INSERT INTO events SET content_id ='$id', object=1, date=$time, user_id='".$_SESSION['id']."', type = $event_type");
		}
		
		update_user_score((int)$_SESSION['id'], 1);
	}
}

if (isset ($_POST["reviews"]))
{ 
	if(loggedin())
	{
		$message = mysql_real_escape_string(htmlentities($_POST["reviews"]));
		
		$sendcomment = mysql_query("INSERT INTO $review_table SET $content_id ='$id', review='$message', date=$time, user_id='".$_SESSION['id']."'");
		
		if ($sendcomment)
		{
			$sendcomment = mysql_query("INSERT INTO events SET content_id ='$id', object=2, date=$time, user_id='".$_SESSION['id']."', type = $event_type");
		}
		
		update_user_score((int)$_SESSION['id'],8);
	}
}

if (isset ($_POST["review_id"]) && isset($_POST["change"]))
{
    $review_id = $_POST["review_id"];
	$change = $_POST["change"];
	$review_query = mysql_query("SELECT * FROM $review_table WHERE review_id = $review_id") or die(mysql_error());
	$review = mysql_fetch_array($review_query);
	$cont_id = $review[$content_id];
	$changer_id = $_SESSION['id'];
	
	if(loggedin() && $review['user_id'] != $changer_id)
	{	
		//$already = mysql_query("SELECT user_id FROM reviewers WHERE review = '$line'") or die(mysql_error());
		//if (mysql_num_rows($already) == 0)
		//{
			if($change == "up")
			{
				mysql_query("UPDATE $review_table SET score = score + 1 WHERE review_id = $review_id") or die(mysql_error());
				update_user_score($review['user_id'],1);
			}
			else
			{
				mysql_query("UPDATE $review_table SET score = score - 1 WHERE review_id = $review_id") or die(mysql_error());
				update_user_score($review['user_id'],-1);
			}
			
			//mysql_query("INSERT INTO reviewers SET user_id = ".$_SESSION['id'].", review = '$line'") or die(mysql_error());
			
			$content_query = mysql_query("SELECT * FROM $type WHERE $content_id = $cont_id");
			$content = mysql_fetch_array($content_query);
			
			$user_query = mysql_query("SELECT * FROM users WHERE id = ".$review['user_id']."") or die(mysql_error());
			$user = mysql_fetch_array($user_query);
			
			$changer_query = mysql_query("SELECT * FROM users WHERE id = $changer_id") or die(mysql_error());
			$changer = mysql_fetch_array($changer_query);
			
			$changer_username = $changer['username'];
			
			$name = $content[$type."_name"];
			
			mysql_query("INSERT INTO message (recipient, sender, date, deleted, subject, message) VALUES(".$user['id'].",0,$time,0,'Score Change!','Your review for <a href=http://www.criticrania.com/launch/content.php?type=$type&id=$cont_id>$name</a> has been updated by $changer_username!')  ") or die(mysql_error());
			mysql_query("UPDATE users SET unread = 1 WHERE id = '".$user['id']."' ") or die(mysql_error());
		//}
		//else
		//{
			//header("Location: ".$_SERVER['HTTP_REFERER']."");
		//}
		
	}
}
//$rating = followed_rating($id, $event_type, $num, true, "true");
//$followed = followed_rating($id, $event_type, $num, true, "false");
?>
<body class="<?php echo $color_scheme; ?>">
<div id="container">
  <?php include 'banner.php'; ?>
	<div id="main" role="main" class="container clearfix">
		<div id="add_comment" class="reveal-modal">
			<h1>Add a Comment</h1>
			<?php if(loggedin()) 
			{
				?>
			<div id="addcom"> 
						<?php echo $_SESSION['username'];?>
					
					<div id="clockbox" >
					<?php echo date("n/j/y g:i A",time() + 10800); ?>
					</div>
					<form method="post" action="<?php echo "".$_SERVER['PATH_INFO'].""; ?>" name="addcomment">
						<textarea id="focused" name="comments" wrap=physical cols=50 rows=4 style="font-size: 12px; resize: none;" onKeyDown="textCounter(this.form.comments,this.form.remLen,250);"
						onKeyUp="textCounter(this.form.comments,this.form.remLen,250);"></textarea>
						<br /><br /><input readonly type=text name=remLen size=3 maxlength=3 value="250" style="width: 25px; border: none;"> characters left </font>
						<input  type="submit" value="Comment!">
						<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>" />
					</form>
			</div>
			<?php
			}
			else
			{
				echo "You must be logged in to enter a comment!";
			}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
		<div id="add_review" class="reveal-modal">
			<h1>Add a Review</h1>
			<?php 
			$query = mysql_query("SELECT * FROM " .  $review_table . " WHERE user_id = " . (int)$_SESSION['id'] . " AND " . $content_id . " = " . $id . "");
			
			if(loggedin() && mysql_num_rows($query) == 0) 
			{
				?>
			<div id="addrev"> 
						<?php echo $_SESSION['username'];?>
					
					<div id="clockbox" >
					<?php echo date("n/j/y g:i A",time() + 10800); ?>
					</div>
					<form method="post" action="<?php echo "".$_SERVER['PATH_INFO'].""; ?>" name="addreview">
						<textarea id="focused" name="reviews" wrap=physical cols=75 rows=8 style="font-size: 12px; resize: none;" onKeyDown="textCounter(this.form.reviews,this.form.remLen,5000);"
						onKeyUp="textCounter(this.form.reviews,this.form.remLen,5000);"></textarea>
						<br /><br /><input readonly type=text name=remLen size=4 maxlength=4 value="5000" style="width: 35px; border: none;"> characters left </font>
						<input  type="submit" value="Review!">
						<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>" />
					</form>
			</div>
			<?php
			}
			elseif(loggedin() && mysql_num_rows($query) == 1)
				echo "You have already written a review for this item!";
			else
			{
				echo "You must be logged in to enter a review!";
			}
			?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
    <section id="single-content" class="left clearfix static" style="padding-top: 30px;">
      <img style="padding-top: 30px;" class="left" src="<?php echo $poster; ?>" alt="Zombieland (2008)" />
      <article class="left" style="padding-top: 0px;">
        <h1><?php echo $content_row[$content_name] . " (" . $content_row['year'] . ")"; ?></h1>
		<?php ratebar($type, $id);?><div id="ratebar"></div>
			<form style="display: inline-block; width: 100px;" method="post" action="http://www.criticrania.com/rate.php">
				<input type="text" id="amount" name="amount" style="background: #222; border:0; color:#ddd; font-weight:bold; width:25px" readonly="true"/>
				<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
				<input type="hidden" name="type" value="<?php echo $type; ?>">
				<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
				<?php
					if (loggedin())
						echo "<button type='button' name='rate' onclick='this.form.submit()'>Rate</button>";
					else
						echo "<button type='button' name='rate' onClick=\"alert('You must be logged in to rate content.')\">Rate</button>";
				?>
			</form>
        <!--<span class="left rating-bar"><button type="button" name="rate">Rate</button></span>-->
        <span class="content-rating"><i>Followed</i><b><?php if(loggedin()) echo followed_rating($id, $event_type, $num, true, "false"); else echo "N/A";?></b><i><?php echo $num; ?> ratings</i></span>
        <span class="content-rating"><i>Criticrania</i><b><?php echo followed_rating($id, $event_type, $num, true, "true"); ?></b><i><?php echo $num; ?> ratings</i></span>

        <p style="padding-bottom: 20px;"><?php echo $description; ?></p>

        <p style="padding-bottom: 20px;"><?php echo $extra_info; ?></p>

        <p class="content-meta"><?php echo $secondary_info; ?></a>

        <p class="content-action">
        <form method="post" action="http://www.criticrania.com/wishlist.php">
		<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
		<input type="hidden" name="type" value="<?php echo $type; ?>">
		<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
		<?php
			$check_cranium = get_wishlist($_SESSION['id'], $type);
			$in == false;
			
			foreach($check_cranium as $item)
			{
				if ($item == $id)
				{
					$in = true;
					echo "<button type='button' value='Already in your Cranium!' onClick=\"alert('This is already in your Cranium!')\">Already in your Cranium!</button>";
					break;
				}
			}
			if (loggedin() && $in == false)
				echo "<button type='button' value='Add to your Cranium' onclick='this.form.submit()'>Add to your Cranium</button>";
			else if ($in == false)
				echo "<button type='button' value='Add to your Cranium' onClick=\"alert('You must be logged in to add something to your cranium.')\">Add to your Cranium</button>";
		?>
		</form>
        </p>
        <p class="content-action">
        <div id="fb-root"></div>
			<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
			<div class="fb-like" data-href="http://www.criticrania.com/content.php?type=<?php echo $type; ?>&amp;id=<?php echo $id; ?>" data-send="true" data-width="450" data-show-faces="false"></div>
        </p>
        <?php
        if ($type == "movie" && $content_row['year'] < 2011)
        {
        ?>
        <p class="content-action">
        <form method="post" action="http://www.criticrania.com/upac_vote.php">
		<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
		<input type="hidden" name="type" value="<?php echo $type; ?>">
		<input type="hidden" name="<?php echo $content_id; ?>" value="<?php echo $id; ?>">
		<?php
		if (loggedin())
			echo "<button type='button' value='Vote for UPAC to Show this Movie!' onclick='this.form.submit()'>Vote for UPAC to show this movie!</button>";
		else
			echo "<button type='button' value='Vote for UPAC to Show this Movie!' onClick=\"alert('You must be logged in to vote.')\">Vote for UPAC to show this movie!</button>";
		?>
		</form>
        </p>
        <?php
        }
        ?>
      </article>
    </section>
    <section id="comments" class="right" style="padding-top: 50px;">
      <h1><span class="left">Comments</span><a class="add-content" href="#" data-reveal-id="add_comment" data-animation="fade">Add</a></h1>
      <?php 
      		$query = mysql_query("SELECT * FROM " . $comment_table . " WHERE " . $content_id ." = " . $id . " ORDER BY date desc");
      		$num_comments = mysql_num_rows($query);
      		if($num_comments > 4 )
      			$num_comments = 4;
      		for($i = 0; $i < $num_comments; $i++)
      		{
      			$comment = mysql_fetch_array($query);
				$date = date("n/j/y g:i A",$comment['date']+10800);
      			echo "<article>";
      			$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $comment['user_id'] . "'");
      			$user = mysql_fetch_array($get_name);
      			echo "<h2><a href='users.php?id=".$user['id']."'>" . $user['username'] . "</a><div class='right'>$date</div></h2>";
      			echo "<p>" . stripslashes($comment['comment']) . "</p>";
      			echo "</article>";
      		}
      		if($num_comments == 0)
      			echo "<i>There doesn't seem to be anything here. Be the first to comment!</i>";
      ?>
	</div>
	<footer id="global-footer" class="clearfix">
    <div class="container">
      <div class="clearfix">
        <section id="reviews" class="clearfix left">
          <h1><span class="left">Reviews</span><a class="add-content" href="#" data-reveal-id="add_review" data-animation="fade">Add</a></h1>
          <?php
          		$query = mysql_query("SELECT * FROM " . $review_table . " WHERE " . $content_id ." = " . $id . " ORDER BY date DESC");
      			$num_reviews = mysql_num_rows($query);
      			if($num_reviews > 3 )
      				$num_reviews = 3;
      			for($i = 0; $i < $num_reviews; $i++)
      			{
      				$review = mysql_fetch_array($query);
      				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
      				$user = mysql_fetch_array($get_name);
      				if($review['score'] > 0)
      				{
      					$score_color = "positive";
      					$sign = "+";
      				}
      				elseif($review['score'] < 0)
      				{
      					$score_color = "negative";
      					$sign = "-";
      				}
      				echo "<article>";
      				echo "<h2><a href='users.php?id=".$user['id']."'>" . $user['username'] . "</a><span class='" . $score_color . "'>" . $sign . $review['score'] . "</span></h2>";
      				echo "<p>" . stripslashes($review['review']) . "</br>" ?>
								<form method="post" action="<?php echo $_SERVER['PATH_INFO']; ?>" name="vote-up" class="vote-up">
									<input type="image" src="img/icon-upvote.png" />
									<input type="hidden" name="review_id" value="<?php echo $review['review_id']; ?>" />
									<input type="hidden" name="change" value="up">
								</form>
								<form method="post" action="<?php echo $_SERVER['PATH_INFO']; ?>" name="vote-up" class="vote-up">
									<input type="image" src="img/icon-downvote.png" />
									<input type="hidden" name="review_id" value="<?php echo $review['review_id']; ?>" />
									<input type="hidden" name="change" value="down">
								</form></p>					<?php
      				echo "</article>";
      			}
      			if($num_reviews == 0)
      				echo "<i>There doesn't seem to be anything here. Be the first to write a review!</i>";
          ?>
          
        </section>
        <section id="more-like" class="content-blocks right">
          <?php echo "<h1>" . $nice_word . " like " . $content_row[$content_name] . "</h1>"; ?>
          
          <?php
			
			$query = mysql_query("SELECT * FROM " . $rating_table . " WHERE " . $content_id . " = '$id'");
			$num_ratings = mysql_num_rows($query);
			$liked_it = array();
			while($row = mysql_fetch_array($query))
			{
				if($row['rating'] >= 8)
				{
					$liked_it[] = $row['user_id'];
				}
			}
			$suggestions = array();
			for($i = 0; $i < count($liked_it);  $i++)
			{
				$user_check = $liked_it[$i];
				$query = mysql_query("SELECT * FROM " . $rating_table . " WHERE rating >= 8 AND user_id = '$user_check' AND $content_id != $id");
				while($row = mysql_fetch_array($query))
				{
					if(array_search($row[$content_id], $suggestions) != false) //already been suggested
						$suggestions[$row[$content_id]] = $suggestions[$row[$content_id]]+1;
					else
						$suggestions[$row[$content_id]] = 1;
				}
			}
			
			reset($suggestions);
			if(count($suggestions) > 4)
				$num_suggestions = 4;
			else
				$num_suggestions = count($suggestions);
			
				arsort($suggestions); 
				//print_r($suggestions);
				for($j = 0; $j < $num_suggestions; $j++)
				{	
					$finder = mysql_query("SELECT * FROM " . $type . " WHERE " . $content_id . " = '".(key($suggestions))."'") or die(mysql_error());
					$suggest_item = mysql_fetch_array($finder);
					echo "<article class ='" . $color . "'>";
					echo "<div class='rated-img'>";
					echo "<a href ='content.php?type=" . $type . "&id=" .(key($suggestions)). "'><img src='" . $thumb . $suggest_item['poster'] . "' alt='" . $suggest_item[$type . '_name'] . "' /></a>";
					echo "</div>";
					echo "<div class = 'rated-info'>";
					//echo "<h2><a href='content.php?type=" . $type . "&id=" . key($suggestions) . "'>".$suggest_item[$type . '_name']. "</a></h2>";
					echo "<h2><a href ='content.php?type=" . $type . "&id=" .(key($suggestions)). "'>".$suggest_item[$type . '_name']. "</a></h2>";
					$num = 0;
					$average_rating = followed_rating(key($suggestions), $event_type, $num, true, "true");
					echo "<p class='content-rating'><b>" . $average_rating . " out of 10</b></p>";
					echo "<p class='user'>" . $num . " ratings</p>";
					echo "</div>";
					echo "</article>";
					next($suggestions);
				}
			if($num_suggestions == 0)
				echo "More people need to rate this to give suggestions!";
			
		?>
        </section>
      </div>
      
    <script type="Text/JavaScript">
	function textCounter(field, countfield, maxlimit) {
		if (field.value.length > maxlimit) // if too long...trim it!
		field.value = field.value.substring(0, maxlimit);
		// otherwise, update 'characters left' counter
		else 
		countfield.value = maxlimit - field.value.length;
		}
      </script>
      
      <!-- Modal Reveal for comments and reviews-->
      <link rel="stylesheet" href="reveal.css">
      <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>-->
	  <script type="text/javascript" src="jquery.reveal.js"></script>
	  <style type="text/css">
			body { font-family: "HelveticaNeue","Helvetica-Neue", "Helvetica", "Arial", sans-serif; }
			.big-link { display:block; margin-top: 100px; text-align: center; font-size: 70px; color: #06f; }
		</style>
		
		  <nav class="right">
			<ul>
			  <li><a href="privacypolicy.php">Privacy Policy</a></li>
			  <li><a href="useragreement.php">Terms of Use</a></li>
			  <li class="last"><a href="mailto:team@criticrania.com">Contact Us</a></li>
			</ul>
		  </nav>
		  <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		</div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>
<script>
/*
  $('#flow').masonry({
    itemSelector: 'article',
    columnWidth: 300,
    isFitWidth: true,
    gutterWidth: 20
  });
*/
</script>
<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
