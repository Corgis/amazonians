<?php include 'functions.php';  
	if (!loggedin())
		header("Location: /");
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
    <section id="mailbox" class="static">
		<h1>Inbox</h1>
		<div id="">
			<?php list_messages("inbox");?>
		</div>
    </section>
	</div>
	<footer id="global-footer" class="clearfix">
    <div class="container">
      <div class="clearfix">
        <section id="suggestions" class="clearfix left">
          <h1>Suggestions for you</h1>
          <?php
			$query = mysql_query("SELECT * FROM events WHERE user_id = ".$_SESSION['id']."");
			if(mysql_num_rows($query) == 0)
			{
				echo "This is where you'd normally find content suggestions. However, you haven't rated enough things or you've rated EVERYTHING
				on the site (unlikely). We suggest you rate more things and then check back.";
				return;
			}
          	$results = sugg((int)$_SESSION['id']);
          	$get = mysql_query("SELECT * FROM " . $results[1] . " WHERE " . $results[1] . "_id = " . $results[0] . "");
          	if($results[0] > 0)
          		$item = mysql_fetch_array($get);
          	else
          		echo "OH NO! You will generally recieve a random piece of content suggested to you here from someone with a high Criticompatibility with you.";
          	if($results[1] == "movie")
          	{
          		$color = "blue";
          		$type = "movies";
          		$description = $item['synopsis'];
          		$by = $item['director'];
          		$type2 = 1;
          	}
          	if($results[1] == "tv")
          	{
          		$color = "green";
          		$type = "television";
          		$description = $item['synopsis'];
          		$by = $item['creator'];
          		$type2 = 2;
          	}
          	if($results[1] == "book")
          	{
          		$color = "red";
          		$type = "books";
          		$description = $item['synopsis'];
          		$by = $item['author'];
          		$type2 = 3;
          	}
          	if($results[1] == "vg")
          	{
          		$color = "yellow";
          		$type = "videogames";
          		$description = $item['synopsis'];
          		$by = $item['producer'];
          		$type2 = 4;
          	}
          	if($results[1] == "music")
          	{
          		$color = "purple";
          		$type = "music";
          		$by = $item['artist'];
          		$type2 = 5;
          	}
          	
          	$poster_loc = "../".$type . "/images/thumbs/" . $item['poster'];
          	$get_name = $results[1] . "_name";
          	$name = $item[$get_name];
          	$year = $item['year'];
          	$description = "(" . $year . ") " . $description;
          	$rating = followed_rating($results[0], $type2, $num, true, "true");
            if($results[0] > 0)
            {	
          ?>
          <article id="content-suggest">
            <?php echo"<img src='" . $poster_loc . "' class='left' />"; ?>
            <div class="right">
              <?php echo"<h2><a href='content.php?type=" . $type . "&id=" . $results['0'] . "'  class = '" . $color . "' text-decoration='none'>" . $name . "</a></h2>"; ?>
              <?php echo"<p>" . $description . "</p>"; ?>
              <p><b>Created by:</b> <?php echo $by; ?><br /><b>Average Rating: </b><?php echo $rating; ?></p>
            </div>
          </article>
          <?php } ?>
          <?php
          	$results = user_suggest_cc();
			if ($reults == -1)
				return;
          	if($results[0] == "movie")
          	{
          		$color = "blue";
          	}
          	if($results[0] == "tv")
          	{
          		$color = "green";
          	}
          	if($results[0] == "book")
          	{
          		$color = "red";
          	}
          	if($results[0] == "vg")
          	{
          		$color = "yellow";
          	}
          	if($results[0] == "music")
          	{
          		$color = "purple";
          	}
          	$suggested_id = $results[1];
          	$get = mysql_query("SELECT * FROM users WHERE id = '$suggested_id'");
          	$suggested_user = mysql_fetch_array($get);
          	$name = $suggested_user['username'];
          	$cc = $results[2];
          	$since = $suggested_user['since'];
          	$date = date('F jS, Y' , $since);
          	$get = mysql_query("SELECT * FROM relationships WHERE user_id1 = '$suggested_id'");
          	$followers = mysql_num_rows($get);
          	if($results[0] > 0)
          	{
          ?>
          <article id="user-suggest">
            <?php echo "<h2><a href = '/users.php?id=" . $suggested_id . "' class = '" . $color ."'>" . $name . "</a></h2>"; ?>
            <?php echo "<span class='" . $color . " rating'>" .  $cc . "</span>";?> <i>(Criticompatibility)</i>
            <?php
            if($results[3] != "")
            	{
            		$query = mysql_query("SELECT * FROM " . $results[0] . " WHERE " . $results[0] . "_id = " . $results[3] . "");
            		$item = mysql_fetch_array($query);
            		echo "<p>You both have enjoyed <i><a href='content.php?type=" . $results[0] . "&id=" . $results[3] . "'>" . $item[$results[0]."_name"] . "</a></i>";
            	}
            if($results[4] != "")
            	{
            		$query = mysql_query("SELECT * FROM " . $results[0] . " WHERE " . $results[0] . "_id = " . $results[4] . "");
            		$item = mysql_fetch_array($query);
            		echo ", <i><a href='content.php?type=" . $results[0] . "&id=" . $results[4] . "'>" . $item[$results[0]."_name"] . "</a></i>";
            	}
            if($results[5] != "")
            	{
            		$query = mysql_query("SELECT * FROM " . $results[0] . " WHERE " . $results[0] . "_id = " . $results[5] . "");
            		$item = mysql_fetch_array($query);
            		echo ", <i><a href='content.php?type=" . $results[0] . "&id=" . $results[5] . "'>" . $item[$results[0]."_name"] . "</a></i>";
            	}
            echo ".</p>";
            ?>
            <p><b>Member since:</b> <?php echo $date;?><br /><b>Followers:</b> <?php echo $followers; ?></p>
          </article>
          <?php
          }
          else
          	echo "OH NO! We'd usually suggest a user for you to follow here based on your Criticompatibility with the entire
          	user base. However, it seems that you have either not rated enough things lately or you follow everyone (unlikely)!";
          ?>
        </section>
        <section id="your-cranium" class="right">
          <h1>Your Cranium</h1>
          <!-- HEY WING! SO I MADE A NICE FUNCTION FOR YOU THAT GETS AN ARRAY OF THE ELEMENTS OF
          A WISHLIST FOR A GENRE: get_wishlist($user_id, $type). Do this however you so choose! -->
          <ul class="content-filter right">
            <li class="blue dots"><a>Movies</a></li>
            <li class="green dots"><a>Television</a></li>
            <li class="purple dots"><a>Music</a></li>
            <li class="yellow dots"><a>Games</a></li>
            <li class="red dots"><a>Books</a></li>
          </ul>
          <div class="blue lists" id="movie_wish">
			<?php
				echo "<h2>Movies</h2>";
				$movies = get_wishlist((int)$_SESSION['id'],"movie");
				if(count($movies)>5)
					$limit = 5;
				else
					$limit = count($movies);
					
				for($i = 0; $i < $limit;$i++)
				{
					$get = mysql_query("SELECT * FROM movie WHERE movie_id = " . $movies[$i]);
					$movie = mysql_fetch_array($get);
					echo "<a href = '/content.php?type=movie&id=" . $movies[$i] . "' class = 'blue'>" . $movie['movie_name'] . "</a></br>";
				}
			?>
          </div>
          <div class="green lists" id="tv_wish">
			  <?php
				echo "<h2>Television</h2>";
				$tvs = get_wishlist((int)$_SESSION['id'],"tv");
				if(count($tvs)>5)
					$limit = 5;
				else
					$limit = count($tvs);
					
				for($i = 0; $i < $limit;$i++)
					{
						$get = mysql_query("SELECT * FROM tv WHERE tv_id = " . $tvs[$i]);
						$tv = mysql_fetch_array($get);
						echo "<a href = '/content.php?type=tv&id=" . $tvs[$i] . "' class = 'green'>" . $tv['tv_name'] . "</a></br>";
					}
			  ?>
          </div>
		  <div class="red lists" id="book_wish">
			  <?php
				echo "<h2>Books</h2>";
				$books = get_wishlist((int)$_SESSION['id'],"book");
				if(count($tvs)>5)
					$limit = 5;
				else
					$limit = count($books);
					
				for($i = 0; $i < $limit;$i++)
					{
						$get = mysql_query("SELECT * FROM book WHERE book_id = " . $books[$i]);
						$book = mysql_fetch_array($get);
						echo "<a href = '/content.php?type=book&id=" . $books[$i] . "' class = 'red'>" . $book['book_name'] . "</a></br>";
					}
			  ?>
          </div>
		  <div class="yellow lists" id="vg_wish">
			  <?php
				echo "<h2>Video Games</h2>";
				$vgs = get_wishlist((int)$_SESSION['id'],"vg");
				if(count($vgs)>5)
					$limit = 5;
				else
					$limit = count($vgs);
					
				for($i = 0; $i < $limit;$i++)
					{
						$get = mysql_query("SELECT * FROM vg WHERE vg_id = " . $vgs[$i]);
						$vg = mysql_fetch_array($get);
						echo "<a href = '/content.php?type=vg&id=" . $vgs[$i] . "' class = 'yellow'>" . $vg['vg_name'] . "</a></br>";
					}
			  ?>
          </div>
		  <div class="purple lists" id="music_wish">
			  <?php
				echo "<h2>Music</h2>";
				$musics = get_wishlist((int)$_SESSION['id'],"music");
				if(count($musics)>5)
					$limit = 5;
				else
					$limit = count($musics);
					
				for($i = 0; $i < $limit;$i++)
					{
						$get = mysql_query("SELECT * FROM music WHERE music_id = " . $musics[$i]);
						$music = mysql_fetch_array($get);
						echo "<a href = '/content.php?type=music&id=" . $musics[$i] . "' class = 'purple'>" . $music['music_name'] . "</a></br>";
					}
			  ?>
          </div>
		  <script type="text/javascript">
		  
			$("#tv_wish").hide();
			$("#vg_wish").hide();
			$("#book_wish").hide();
			$("#music_wish").hide();
			
			$(".dots").click(function()
			{
				var str = $(this).attr("class");
				$('#movie_wish').hide();
				$("#tv_wish").hide();
				$("#vg_wish").hide();
				$("#book_wish").hide();
				$("#music_wish").hide();
				if(str == "blue dots")
					$("#movie_wish").fadeIn(500);
				if(str == "green dots")
					$("#tv_wish").fadeIn(500);
				if(str == "red dots")
					$("#book_wish").fadeIn(500);
				if(str == "yellow dots")
					$("#vg_wish").fadeIn(500);
				if(str == "purple dots")
					$("#music_wish").fadeIn(500);
			});
			
		  </script>
        </section>
      </div>
      <nav class="right">
        <ul>
          <li><a href="#">Privacy Policy</a></li>
          <li><a href="#">Terms of Use</a></li>
          <li class="last"><a href="#">Contact Us</a></li>
        </ul>
      </nav>
      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
    </div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var slides = $('.sliding');
	var num = slides.length;
	
	$('#flowinner').css('width', num*960);
	
	var count = 0;
	$("#right").click(function()
	{
		$("#left").show();
		if(count == (num-1))
			return;
		else
			$(".sliding").animate({left: "-=960"});
		count++;
	});
	$("#left").click(function()
	{
		if(count == 0)
			return;
		else
			$(".sliding").animate({left: "+=960"});
		count--;
		if(count == 0)
			$("#left").hide();
	});
	$("#left").hide();
	
});
</script>
<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
