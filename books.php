<?php
	include 'functions.php';
	include 'top.php';
	$type = "book";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Books</title>
<?php
	include 'sheetshome.php';
?>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<!-- start banner -->
<?php
	//banner(3);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style="white-space: normal; background:url(img/body-bg.gif);">

	<!-- Title + informative blurb -->
	<div id="title" style = "color:#c83535">Books<br /></div>
	<div id="description" style = "color:#c83535">
		From every author, Leo Tolstoy to Dan Brown, show you're literate and share your thoughts on your reading experiences. Whether
		it's your favorite book to read on the beach, or the book you couldn't stand reading (or not reading) in school, books themselves
		encourage opinions.
	</div><br />
	<!-- Specific content for books homepage -->
	<div id="content" style="background:url(images/body-bg.gif)">
		<div id="top_left"><br /><br />
			<!-- Include NIVO slider. Top Ten obtains the ten highest average ratings of books in the past week (See functions) -->
			<div class="head">This Week's Top Rated Books</div><br />
			<div id="slider">
				<?php          
					top_ten("book");
				?>
			</div>
		</div>
		<!-- 3 Most Recent Book reviews, including picture and author of review -->
		<div id="top_right"><br />
			<div class="head">Recent Reviews</div><br />
				<?php			
					//Get all book reviews ordered by date (most recent first)						
					$check = mysql_query("SELECT * FROM book_reviews ORDER BY date DESC")or die();
					
					echo "<table width='100%' cellspacing='8px'>";
					$recent = 0;
					//Only deal with the 3 most recent reviews
					while($recent < 3)
					{
						//Get Review
						$row = mysql_fetch_array($check);
						//Get username of review
						$usercheck = mysql_query("SELECT * FROM users WHERE id = '". $row['user_id'] ."'")or die();
						$username = mysql_fetch_array($usercheck);
						//Obtain actual review, add slashes to be able to check books table
						$review1 = addslashes($row['review']);
						//Find book that correlates to written review
						$book_info = mysql_query("SELECT * FROM book_reviews WHERE review = '$review1'");
						//Determine length of the review
						$length = strlen($review1);
						//Remove slashes to create readable text
						$review1 = stripslashes($review1);
						//Obtain book information (it's row of information)
						$book_row = mysql_fetch_array($book_info);
						$book_id = $book_row['book_id'];	
						$line2 = mysql_query("SELECT * FROM book_reviews WHERE book_id = '$book_id' ORDER BY date DESC")or die();
						$book_info = mysql_query("SELECT * FROM book WHERE book_id = '$book_id'")or die();
						$book_row = mysql_fetch_array($book_info);
						//Display Book title with a link to its specific content page
						echo "<tr valign='top'><td width='80%'>";
						echo "<hr><movietitle><a href='/content.php?type=book&id=$book_id' style = 'color:#c83535'>" .$book_row['book_name']. "</a></movietitle><hr><br />";
						//Display Author of review, including a link to their profile page
						echo "<a href= '/users.php?id=" . $row['user_id'] . "' color='black' >" . $username['username'] . "</a> - ";
						//Obtain book's image
						$poster = $book_row['poster'];
						$filename = "books/images/$poster";
						
						/*Check if length of review is too long to display, if so display only 150 characters and offer to allow user to read more, else display review*/
						if ($length > 180)
						{
							$review2 = substr($review1, 0, 150);
							echo "\"$review2...\" <a href ='/content.php?type=book&id=$book_id'>Read more</a>";
						}
						else
							echo "\"$review1\"";
						//Check if thumbnail exists, if not, create it
						if (file_exists($filename))
						{
							$image = "<br><td><a href='content.php?type=book&id=$book_id'><img id='poster' src='books/images/$poster' /></a>";
						}
						else
						{
							$image = "<br><td><a href='content.php?type=book&id=$book_id'><img id='poster' src='images/no_image_thumb.png' /></a>";
						}
						//Disaply thumbnail of book image
						echo $image;
						echo "</td></tr>";
						//Increment to next most recent review
						$recent++;
					}
					echo "</table>";
				?>
				&nbsp;
		</div>
		<div id="bottom_left"><br />
			<!-- Display tuhumbnails of 5 most visited movie content pages in last hour (see Functions) -->
			<div class="head">Trending Now</div>
			<div style="text-align: center; color:#c83535">Here are the 5 most visited books in the last hour</div><br />
				<?php
					trending("book");
				?>
		</div>
		<div id="bottom_right"><br />
			<!-- Allow users to answer a poll question, display results after they have voted (see poll.php) -->
			<?php include 'poll.php'; ?>
		</div>
	</div>
	&nbsp;
</div><br />
<!-- end page -->

<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>
