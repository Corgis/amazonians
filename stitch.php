<?php
header('Content-Type: image/jpeg');
error_reporting(E_ALL);
require_once('functions.php');
$kWidthOfThumbnail = 54;
$kHeightOfThumbnail = 79;
$kNumThumbColumns = 8;
$kNumThumbRows = 3;
$kGetLabels = array('a', 'b', 'c', 'd', 'e',
		    'f', 'g', 'h', 'i', 'j',
		    'k', 'l', 'm', 'n', 'o',
		    'p', 'q', 'r', 's', 't',
		    'u', 'v', 'w', 'x', 'y',
		    'z');

$thumbsheet = imagecreatetruecolor($kWidthOfThumbnail*$kNumThumbColumns,
                                   $kHeightOfThumbnail*$kNumThumbRows);

for ($i = 0; $i < $kNumThumbRows; $i++)
{
  for ($j = 0; $j < $kNumThumbColumns; $j++)
  {
    $id = $_GET[$kGetLabels[$i*$kNumThumbColumns+$j]];
    $content_info = mysql_query("SELECT * 
		                 FROM `movie`
				 WHERE `movie_id` = ".$id)or die(mysql_error());
    $content_row = mysql_fetch_array($content_info);
    $spacejam = imagecreatefromjpeg('movie/thumbs/'.$content_row['poster']);     
    
    imagecopyresampled($thumbsheet,
                       $spacejam,
                       $kWidthOfThumbnail*$j,
                       $kHeightOfThumbnail*$i,
                       0,
                       0,
                       $kWidthOfThumbnail,
                       $kHeightOfThumbnail,
                       imagesx($spacejam),
                       imagesy($spacejam));
  }
}

imagejpeg($thumbsheet);
imagedestroy($thumbsheet);
imagedestroy($spacejam);
?>
