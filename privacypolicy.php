<?php include 'functions.php';  
	include 'top.php';
?>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix" style="color:#CCC">
	Privacy Policy<br /><br />

		We collect information from you when you register on our site.<br /><br />

		When registering on our site, as appropriate, you may be asked to enter your: name or e-mail address. You may, however, visit our site
		anonymously.<br /><br />

		Google, as a third party vendor, uses cookies to serve ads on your site. Google's use of the DART cookie enables it to serve ads to your users based
		on their visit to your sites and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content
		network privacy policy.<br /><br />

		Any of the information we collect from you may be used in one of the following ways:

		<ul>
			<li>To personalize your experience<br />
			(your information helps us to better respond to your individual needs)</li>
			<li>To improve our website<br />
			(we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
			<li>To improve customer service<br />
			(your information helps us to more effectively respond to your customer service requests and support needs)</li>
			<li>To send periodic emails</li>
		</ul>

		The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions.<br /><br />

		We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal
		information.<br /><br />

		We use cookies (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you
		allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information) to understand and
		save your preferences for future visits and compile aggregate data about site traffic and site interaction so that we can offer better site
		experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors.
		These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.<br /><br />

		We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties
		who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information
		confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or
		protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for
		marketing, advertising, or other uses.<br /><br />

		Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We
		therefore will not distribute your personal information to outside parties without your consent.<br /><br />

		As part of the California Online Privacy Protection Act, all users of our site may make any changes to their information at anytime by logging into
		their control panel and going to the 'Edit Profile' page.<br /><br />

		We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under
		13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.<br /><br />

		This online privacy policy applies only to information collected through our website and not to information collected offline.<br /><br />

		By using our site, you consent to our web site privacy policy.<br /><br />

		If we decide to change our privacy policy, we will post those changes on this page.<br /><br />
	</div>
		<footer id="global-footer" class="clearfix">
		    <div class="container">
		       
		      <nav class="right">
			<ul>
			  <li><a href="#">Privacy Policy</a></li>
			  <li><a href="#">Terms of Use</a></li>
			  <li class="last"><a href="#">Contact Us</a></li>
			</ul>
		      </nav>
		      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
		    </div>
		</footer>
	</div>
</div> <!--! end of #container -->

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
