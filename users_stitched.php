<?php include 'functions.php'; 

//error_reporting(E_ALL);
$user_view = mysql_real_escape_string($_GET['id']);
$user_id = (int)$_SESSION['id'];
$time = time();

//information
$query = mysql_query("SELECT * FROM users WHERE id = $user_view");
$user_view_info = mysql_fetch_array($query);

//level
$level = level2($user_view, $user_view_info['score']);

//stats
$num_ratings = 0;
$num_comments = 0;
$num_reviews = 0;
$num_followers = 0;

$type = "movie";
$num_ratings += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_view"));
$num_comments += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_comments WHERE user_id = $user_view"));
$num_reviews += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_reviews WHERE user_id = $user_view"));
$type = "tv";
$num_ratings += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_view"));
$num_comments += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_comments WHERE user_id = $user_view"));
$num_reviews += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_reviews WHERE user_id = $user_view"));
$type = "book";
$num_ratings += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_view"));
$num_comments += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_comments WHERE user_id = $user_view"));
$num_reviews += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_reviews WHERE user_id = $user_view"));
$type = "vg";
$num_ratings += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_view"));
$num_comments += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_comments WHERE user_id = $user_view"));
$num_reviews += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_reviews WHERE user_id = $user_view"));
$type = "music";
$num_ratings += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_ratings WHERE user_id = $user_view"));
$num_comments += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_comments WHERE user_id = $user_view"));
$num_reviews += mysql_num_rows(mysql_query("SELECT * FROM " . $type . "_reviews WHERE user_id = $user_view"));

$num_followers = mysql_num_rows(mysql_query("SELECT * FROM relationships WHERE user_id1 = $user_view"));
$type = "";


?>
<?php include 'top.php'; ?>
<style>
.movie_stuff{
text-decoration:none;
};
</style>
<body>
<div id="container">
  <?php include ("banner.php"); ?>
	<div id="main" role="main" class="container clearfix">
    <section id="user-info">
		<div id="username"><?php echo $user_view_info['username']; ?></div>
		<div id="cc">
			<div id="cc-header">Criticompatibility</div>
			<div id="cc-rating" class = "blue movie_stuff">
				<?php
					$num = 0;
					$cc = cc($user_id, $user_view, $num, $avg, "movie", false);
					echo $cc;
				?>
			</div>
			<div id="cc-rating" class = "green tv_stuff">
				<?php
					$num = 0;
					$avg = 0;
					$cc = cc($user_id, $user_view, $num, $avg, "tv" , false);
					echo $cc;
				?>
			</div>
			<div id="cc-rating" class = "red book_stuff">
				<?php
					$num = 0;
					$cc = cc($user_id, $user_view, $num, $avg, "book", false);
					echo $cc;
				?>
			</div>
			<div id="cc-rating" class = "yellow vg_stuff">
				<?php
					$num = 0;
					$cc = cc($user_id, $user_view, $num, $avg, "vg", false);
					echo $cc;
				?>
			</div>
			<div id="cc-rating" class = "purple music_stuff">
				<?php
					$num = 0;
					$cc = cc($user_id, $user_view, $num, $avg, "music", false);
					if($cc < 0)
						$cc = 0.0;
					echo $cc;
				?>
			</div>
			<span id="level">
				<?php echo $level[0] . "(<span id='progress'>" . $user_view_info['score'] . "/" . $level[1] . "</span>)"; ?>
			</span>
		</div>
		<div id="stats">
			<div class="stat-left"><?php echo $num_ratings; ?></div>
			Ratings<br />
			<div class="stat-left"><?php echo $num_comments; ?></div>
			Comments<br />
			<div class="stat-left"><?php echo $num_reviews; ?></div>
			Reviews<br />
			<div class="stat-left"><?php echo $num_followers; ?></div>
			Followers<br />
		</div>
		<div id="followers">
			<span id="following">Following</span>
			<?php
				$query = mysql_query("SELECT * FROM relationships WHERE user_id1 = $user_view ORDER BY rand() LIMIT 5");
				while($row = mysql_fetch_array($query))
				{
					$follow = $row['user_id2'];
					$get = mysql_query("SELECT * FROM users WHERE id = " . $follow);
					$user_check = mysql_fetch_array($get);
					echo "<a href = 'users.php?id=" . $user_check['id'] . "'>" . $user_check['username'] . "</a>, ";
				}
			?> 
			<span id="more">
				<?php if($num_followers > 5) 
					echo "and " . ($num_followers-5) . " more."; ?>
			</span>
		</div>
		<div id="followers">
			<span id = "following">User Since</span>
			<?php
				$get = mysql_query("SELECT * FROM users WHERE id = $user_view");
				$user = mysql_fetch_array($get);
				echo date("n/j/y",$user['since']);
			?>
		</div>
		<?php
			if (loggedin())
			{
				if($user_id != $user_view)
				{
					$query = mysql_query("SELECT * FROM relationships WHERE user_id1 =" . $user_id . " AND user_id2 = $user_view");
					$check3 = mysql_num_rows($query); 
					
					if($check3 == 0)
					{
						?>
						<form method="post" action= "follow.php">
						<input type="hidden" name="uid1" value="<?php echo $user_id; ?>">
						<input type="hidden" name="uid2" value="<?php echo $user_view; ?>">
						<button type="button" name="follow" onclick="this.form.submit()">Follow</button>
						</form>
						<?php
					}
					
					else
					{
						?>
						<form method="post" action= "unfollow.php">
						<input type="hidden" name="uid1" value="<?php echo $user_id; ?>">
						<input type="hidden" name="uid2" value="<?php echo $user_view; ?>">
						<button type="button" name="unfollow" onclick="this.form.submit()">Unfollow</button>
						</form>
						<?php
					}
				}
			}
			else
			{
				echo "";
			}
		?>
    </section>
	<section id="user-rated">
		<div id="top">
			<div class="blue movie_stuff" id="title">Movies Rated</div>
			<div class="green tv_stuff" id="title">Televison Shows Rated</div>
			<div class="red book_stuff" id="title">Books Rated</div>
			<div class="yellow vg_stuff" id="title">Video Games Rated</div>
			<div class="purple music_stuff" id="title">Music Albums Rated</div>
			
			<ul style="padding: 5px 0 0 0;" class="content-filter right">
				<li class="blue dots"><a>Movies</a></li>
				<li class="green dots"><a>Television</a></li>
				<li class="red dots"><a>Books</a></li>
				<li class="yellow dots"><a>Games</a></li>
				<li class="purple dots"><a>Music</a></li>
				
			  </ul>
		</div>
		 <div id="rated-posters" class = "movie_stuff" style="text-align:center">
		 <?php
			$db = new mysqli("localhost","criticrania","Rpi2013",'criticrania');	
			$query = $db->query('SELECT *
					     FROM `movie_ratings` 
					     WHERE `user_id` = \'' . $user_view . '\'
					     ORDER BY `rating` DESC 
					     LIMIT 24')or die(mysql_error());
			$thumb_sheet_url = "stitch.php?";
			$get_name=97;
			while($row = mysqli_fetch_array($query))
			{
				$thumb_sheet_url = $thumb_sheet_url.chr($get_name).'='.$row['movie_id'].'&';
				$get_name++;
			}
			$thumb_sheet_url = $thumb_sheet_url.'.jpg';
			$query = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
			$i = 0;
			$j = 0;
			$thumb_cols = 8;
			$thumb_rows = 3;
			$thumb_width = 54;
			$thumb_height = 79;
			while($row = mysql_fetch_array($query))
			{
				if($i == $thumb_cols) {$i = 0; $j++;}
					?><li style="background:url(<?php echo $thumb_sheet_url; ?>); 
						     background-position: <?php echo (432 - $thumb_width*$i) . 'px -' . $thumb_height*$j;?>px;
						     width:<?php echo $thumb_width;?>px;
						     height:<?php echo $thumb_height;?>px;
						     display: inline-block;
						     margin: 0 5px 5px 0"><?php
				$get = mysql_query("SELECT * FROM movie WHERE movie_id = " . $row['movie_id']);
				$movie = mysql_fetch_array($get);
				
				//if(file_exists("movies/images/thumbs/" . $movie['poster']))
					//echo "<a href='content.php?type=movie&id=" . $row['movie_id'] . "'><img class ='posters' id=".$movie['movie_id']." type='movie' user_id =".$user_view." src='" . $thumb_sheet_url . "' /></a>";
				//else
				//	echo "<a href='content.php?type=movie&id=" . $row['movie_id'] . "'><img src='images/no_image_thumb.png' /></a>";
				$i++;
			} 
		 ?>
		 </div>
		 <div id="rated-posters" class = "tv_stuff" style="text-align:center">
		 <?php
			$query = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
			while($row = mysql_fetch_array($query))
			{
				$get = mysql_query("SELECT * FROM tv WHERE tv_id = " . $row['tv_id']);
				$tv = mysql_fetch_array($get);
				if(file_exists("television/images/thumbs/". $tv['poster']))
					echo "<a href='content.php?type=tv&id=" . $row['tv_id'] . "'><img class ='posters' id=".$tv['tv_id']." type='tv' user_id =".$user_view." src='television/images/thumbs/" . $tv['poster'] . "' /></a>";
				else
					echo "<a href='content.php?type=tv&id=" . $row['tv_id'] . "'><img src='images/no_image_thumb.png' /></a>";
			}
		 ?>
		 </div>
		 <div id="rated-posters" class = "book_stuff" style="text-align:center">
		 <?php
			$query = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
			while($row = mysql_fetch_array($query))
			{
				$get = mysql_query("SELECT * FROM book WHERE book_id = " . $row['book_id']);
				$book = mysql_fetch_array($get);
				if(file_exists("books/images/thumbs/" . $book['poster']))
					echo "<a href='content.php?type=book&id=" . $row['book_id'] . "'><img class ='posters' id=".$book['book_id']." type='book' user_id =".$user_view." src='books/images/thumbs/" . $book['poster'] . "' /></a>";
				else
					echo "<a href='content.php?type=book&id=" . $row['book_id'] . "'><img src='images/no_image_thumb.png' /></a>";
			}
		 ?>
		 </div>
		 <div id="rated-posters" class = "vg_stuff" style="text-align:center">
		 <?php
			$query = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 24");
			while($row = mysql_fetch_array($query))
			{
				$get = mysql_query("SELECT * FROM vg WHERE vg_id = " . $row['vg_id']);
				$vg = mysql_fetch_array($get);
				if(file_exists("videogames/images/thumbs/" . $vg['poster']))
					echo "<a href='content.php?type=vg&id=" . $row['vg_id'] . "'><img class ='posters' id=".$vg['vg_id']." type='vg' user_id =".$user_view." src='videogames/images/thumbs/" . $vg['poster'] . "' /></a>";
				else
					echo "<a href='content.php?type=vg&id=" . $row['vg_id'] . "'><img src='images/no_image_thumb.png' /></a>";
			}
		 ?>
		 </div>
		 <div id="rated-posters" class = "music_stuff" style="text-align:center">
		 <?php
			$query = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_view ORDER BY rating DESC LIMIT 21");
			while($row = mysql_fetch_array($query))
			{
				$get = mysql_query("SELECT * FROM music WHERE music_id = " . $row['music_id']);
				$music = mysql_fetch_array($get);
				if(file_exists("music/images/thumbs/" . $music['poster']))
					echo "<a href='content.php?type=music&id=" . $row['music_id'] . "'><img class ='posters' id=".$music['music_id']." type='music' user_id =".$user_view." src='music/images/thumbs/" . $music['poster'] . "' /></a>";
				else
					echo "<a href='content.php?type=music&id=" . $row['music_id'] . "'><img src='images/no_image_thumb.png' /></a>";
			}
		 ?>
		 </div>
		 <div id='print'>
			</div>
    </section>
	<section id="user-activity">
		<div id="top">
			<div class="blue" id="title">Recent Activity</div>
			<!-- 1 = comment 2 = review 3 = rating -->
		</div>
		<?php
			$query = mysql_query("SELECT * FROM events WHERE user_id = $user_view ORDER BY date DESC LIMIT 4 ");
			while($row = mysql_fetch_array($query))
			{
				if($row['type'] == 1)
					$type = "movie";
				if($row['type'] == 2)
					$type = "tv";
				if($row['type'] == 3)
					$type = "book";
				if($row['type'] == 4)
					$type = "vg";
				if($row['type'] == 5)
					$type = "music";
				$get = mysql_query("SELECT * FROM $type WHERE " . $type . "_id = " . $row['content_id'] . "");
				$item = mysql_fetch_array($get);
				$time = time_since_approx(time() - $row['date']);
				if($row['object'] == 1)//comment
				{	
					echo "<b>Commented on </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";	
				}
				if($row['object'] == 2)//review
				{
					echo "<b>Reviewed </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";
				}
				if($row['object'] == 3)//rate
				{
					echo "<b>Rated </b><i><a href='content.php?type=" . $type . "&id=" . $row['content_id'] . "'>" . $item[$type."_name"] . "</a></i></br>";	
				}
				echo "<i>" . $time . "</i></br></br>";
			}
		?>
    </section>
	</div>
	<footer id="global-footer" class="clearfix" style="padding-top: 40px;">
    <div class="container">
      <div class="clearfix">
        <section id="user-reviews" class="clearfix left" style="width: 600px;">
          <h1>Reviews</h1>
		  <div id="the-reviews" class = "movie_stuff">
			 <?php
			$query = mysql_query("SELECT * FROM movie_reviews WHERE user_id = $user_view ORDER BY score DESC");
			$num_reviews = mysql_num_rows($query);
			if($num_reviews > 3 )
				$num_reviews = 3;
			for($i = 0; $i < $num_reviews; $i++)
			{
				$review = mysql_fetch_array($query);
				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
				$user = mysql_fetch_array($get_name);
				if($review['score'] > 0)
				{
					$score_color = "positive";
					$sign = "+";
				}
				elseif($review['score'] < 0)
				{
					$score_color = "negative";
					$sign = "-";
				}
				
				$get = mysql_query("SELECT * FROM movie WHERE movie_id = " . $review['movie_id']);
				$movie = mysql_fetch_array($get);
				
				echo "<article>";
				echo "<a href='content.php?type=movie&id=" . $review['movie_id'] . "'>" . $movie['movie_name'] . "</a></br>";
				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
				echo "<p>" . stripslashes($review['review']) . "</br></p>";
				echo "</article>";
			}
			if($num_reviews == 0)
				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "tv_stuff">
			 <?php
			$query = mysql_query("SELECT * FROM tv_reviews WHERE user_id = $user_view ORDER BY score DESC");
			$num_reviews = mysql_num_rows($query);
			if($num_reviews > 3 )
				$num_reviews = 3;
			for($i = 0; $i < $num_reviews; $i++)
			{
				$review = mysql_fetch_array($query);
				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
				$user = mysql_fetch_array($get_name);
				if($review['score'] > 0)
				{
					$score_color = "positive";
					$sign = "+";
				}
				elseif($review['score'] < 0)
				{
					$score_color = "negative";
					$sign = "-";
				}
				
				$get = mysql_query("SELECT * FROM tv WHERE tv_id = " . $review['tv_id']);
				$tv= mysql_fetch_array($get);
				
				echo "<article>";
				echo "<a href='content.php?type=tv&id=" . $review['tv_id'] . "'>" . $tv['tv_name'] . "</a></br>";
				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
				echo "<p>" . stripslashes($review['review']) . "</br></p>";
				echo "</article>";
			}
			if($num_reviews == 0)
				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "book_stuff">
			 <?php
			$query = mysql_query("SELECT * FROM book_reviews WHERE user_id = $user_view ORDER BY score DESC");
			$num_reviews = mysql_num_rows($query);
			if($num_reviews > 3 )
				$num_reviews = 3;
			for($i = 0; $i < $num_reviews; $i++)
			{
				$review = mysql_fetch_array($query);
				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
				$user = mysql_fetch_array($get_name);
				if($review['score'] > 0)
				{
					$score_color = "positive";
					$sign = "+";
				}
				elseif($review['score'] < 0)
				{
					$score_color = "negative";
					$sign = "-";
				}
				
				$get = mysql_query("SELECT * FROM book WHERE book_id = " . $review['book_id']);
				$book= mysql_fetch_array($get);
				
				echo "<article>";
				echo "<a href='content.php?type=book&id=" . $review['book_id'] . "'>" . $book['book_name'] . "</a></br>";
				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
				echo "<p>" . stripslashes($review['review']) . "</br></p>";
				echo "</article>";
			}
			if($num_reviews == 0)
				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "vg_stuff">
			 <?php
			$query = mysql_query("SELECT * FROM vg_reviews WHERE user_id = $user_view ORDER BY score DESC");
			$num_reviews = mysql_num_rows($query);
			if($num_reviews > 3 )
				$num_reviews = 3;
			for($i = 0; $i < $num_reviews; $i++)
			{
				$review = mysql_fetch_array($query);
				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
				$user = mysql_fetch_array($get_name);
				if($review['score'] > 0)
				{
					$score_color = "positive";
					$sign = "+";
				}
				elseif($review['score'] < 0)
				{
					$score_color = "negative";
					$sign = "-";
				}
				
				$get = mysql_query("SELECT * FROM vg WHERE vg_id = " . $review['vg_id']);
				$vg= mysql_fetch_array($get);
				
				echo "<article>";
				echo "<a href='content.php?type=vg&id=" . $review['vg_id'] . "'>" . $vg['vg_name'] . "</a></br>";
				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
				echo "<p>" . stripslashes($review['review']) . "</br></p>";
				echo "</article>";
			}
			if($num_reviews == 0)
				echo "<i>There doesn't seem to be anything here.</i>";
          ?>
		  </div>
		  <div id="the-reviews" class = "music_stuff">
			 <?php
			$query = mysql_query("SELECT * FROM music_reviews WHERE user_id = $user_view ORDER BY score DESC");
			$num_reviews = mysql_num_rows($query);
			if($num_reviews > 3 )
				$num_reviews = 3;
			for($i = 0; $i < $num_reviews; $i++)
			{
				$review = mysql_fetch_array($query);
				$get_name = mysql_query("SELECT * FROM users WHERE id = '" . $review['user_id'] . "'");
				$user = mysql_fetch_array($get_name);
				if($review['score'] >= 0)
				{
					$score_color = "positive";
					$sign = "+";
				}
				elseif($review['score'] < 0)
				{
					$score_color = "negative";
					$sign = "-";
				}
				
				$get = mysql_query("SELECT * FROM music WHERE music_id = " . $review['music_id']);
				$music= mysql_fetch_array($get);
				
				echo "<article>";
				echo "<a href='content.php?type=music&id=" . $review['music_id'] . "'>" . $music['music_name'] . "</a></br>";
				echo $user['username'] . "<span class='" . $score_color . "'> " . $sign . $review['score'] . "</span>";
				echo "<p>" . stripslashes($review['review']) . "</br></p>";
				echo "</article>";
			}
			if($num_reviews == 0)
				echo "<i>There doesn't seem to be anything here. Be the first to write a review!</i>";
          ?>
		  </div>
        </section>
        
        <section id="common" class="clearfix right" style="padding: 3em, 0;">
        <?php if($user_id == $user_view){ ?>
	  <h1>Groups</h1>
		  <div id="common-ratings" class="movie_stuff">
			<?php
				$query = mysql_query("SELECT g.group_name as name, g.group_id as group_id FROM groups as g JOIN group_relations as gr ON g.group_id = gr.group_id WHERE gr.user_id = $user_view");
				while (mysql_num_rows($query) > 0 && $row = mysql_fetch_array($query))
				{
					echo "<a href='groups.php?id=". $row['group_id'] ."'>". $row['name'] ."</a></br>";
				}
				if (mysql_num_rows($query) == 0 )
					echo "Not a member of any group!";
			?> 
			<!-- <table border ="0" class= "movie_stuff">
			<tr>
				<td style="width: 150px;">Movie</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['movie_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['movie_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list) <= 10)
					$show = count($super_list);
				else
					$show = 10;
					
				for($i = 0; $i < $show; $i++)
				{
					$select = array_rand($super_list);
					$movie_selected = $super_list[$select];
					unset($super_list[$select]);
					$get = mysql_query("SELECT * FROM movie WHERE movie_id = $movie_selected");
					$movie = mysql_fetch_array($get);
					echo "<tr><td><a href='content.php?type=movie&id=" . $movie['movie_id'] . "'>" . $movie['movie_name'] . "</a></td>";
					$get = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view AND movie_id = $movie_selected");
					$check = mysql_fetch_array($get);
					echo "<td class='smaller'>" . $check['rating'] . "</td>";
					$get = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id AND movie_id = $movie_selected");
					$check2 = mysql_fetch_array($get);
					echo "<td class='smaller'>" . $check2['rating'] . "</td>";
					echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table> 
		  </div><?php
          }
        ?>-->
          <h1><?php echo $user_view_info['username'];?> and me</h1>
		  <div id="common-ratings" class="movie_stuff">
			<table border ="0" class= "movie_stuff">
			<tr><i>
				<td style="width: 150px;">Movie</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</i></tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['movie_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['movie_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list) <= 10)
					$show = count($super_list);
				else
					$show = 10;
					
				for($i = 0; $i < $show; $i++)
				{
				$select = array_rand($super_list);
				$movie_selected = $super_list[$select];
				unset($super_list[$select]);
				$get = mysql_query("SELECT * FROM movie WHERE movie_id = $movie_selected");
				$movie = mysql_fetch_array($get);
				echo "<tr><td><a href='content.php?type=movie&id=" . $movie['movie_id'] . "'>" . $movie['movie_name'] . "</a></td>";
				$get = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_view AND movie_id = $movie_selected");
				$check = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check['rating'] . "</td>";
				$get = mysql_query("SELECT * FROM movie_ratings WHERE user_id = $user_id AND movie_id = $movie_selected");
				$check2 = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check2['rating'] . "</td>";
				echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table>
			<?php echo "</br></br>" . "<a href='all_common.php?type=movie&id=" . $user_view . "'>See All Common Ratings</a>"; ?>
		  </div>
		  <div id="common-ratings" class="tv_stuff">
			<table border ="0" class= "tv_stuff">
			<tr><i>
				<td style="width: 150px;">TV Show</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</i></tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['tv_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['tv_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list) <= 10)
					$show = count($super_list);
				else
					$show = 10;
					
				for($i = 0; $i < $show; $i++)
				{
				$select = array_rand($super_list);
				$tv_selected = $super_list[$select];
				unset($super_list[$select]);
				$get = mysql_query("SELECT * FROM tv WHERE tv_id = $tv_selected");
				$tv = mysql_fetch_array($get);
				echo "<tr><td><a href='content.php?type=tv&id=" . $tv['tv_id'] . "'>" . $tv['tv_name'] . "</a></td>";
				$get = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_view AND tv_id = $tv_selected");
				$check = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check['rating'] . "</td>";
				$get = mysql_query("SELECT * FROM tv_ratings WHERE user_id = $user_id AND tv_id = $tv_selected");
				$check2 = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check2['rating'] . "</td>";
				echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table>
			<?php echo "</br></br>" . "<a href='all_common.php?type=tv&id=" . $user_view . "'>See All Common Ratings</a>"; ?>
		  </div>
			<div id="common-ratings" class = "book_stuff">
            <table border ="0" class= "book_stuff">
			<tr><i>
				<td style="width: 150px;">Book</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</i></tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['book_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['book_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list )<= 10)
					$show = count($super_list);
				else
					$show = 10;
				
				for($i = 0; $i < $show; $i++)
				{
				$select = array_rand($super_list);
				$book_selected = $super_list[$select];
				unset($super_list[$select]);
				$get = mysql_query("SELECT * FROM book WHERE book_id = $book_selected");
				$book = mysql_fetch_array($get);
				echo "<tr><td><a href='content.php?type=book&id=" . $book['book_id'] . "'>" . $book['book_name'] . "</a></td>";
				$get = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_view AND book_id = $book_selected");
				$check = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check['rating'] . "</td>";
				$get = mysql_query("SELECT * FROM book_ratings WHERE user_id = $user_id AND book_id = $book_selected");
				$check2 = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check2['rating'] . "</td>";
				echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table>
			<?php echo "</br></br>" . "<a href='all_common.php?type=book&id=" . $user_view . "'>See All Common Ratings</a>"; ?>
			</div>
			<div id="common-ratings" class = "vg_stuff">
            <table border ="0" class= "vg_stuff">
			<tr><i>
				<td style="width: 150px;">Video Game</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</i></tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['vg_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['vg_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list) <= 10)
					$show = count($super_list);
				else
					$show = 10;
					
				for($i = 0; $i < $show; $i++)
				{
				$select = array_rand($super_list);
				$vg_selected = $super_list[$select];
				unset($super_list[$select]);
				$get = mysql_query("SELECT * FROM vg WHERE vg_id = $vg_selected");
				$vg = mysql_fetch_array($get);
				echo "<tr><td><a href='content.php?type=vg&id=" . $vg['vg_id'] . "'>" . $vg['vg_name'] . "</a></td>";
				$get = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_view AND vg_id = $vg_selected");
				$check = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check['rating'] . "</td>";
				$get = mysql_query("SELECT * FROM vg_ratings WHERE user_id = $user_id AND vg_id = $vg_selected");
				$check2 = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check2['rating'] . "</td>";
				echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table>
			<?php echo "</br></br>" . "<a href='all_common.php?type=vg&id=" . $user_view . "'>See All Common Ratings</a>"; ?>
			</div>
			<div id="common-ratings" class = "music_stuff">
            <table border="0" class= "music_stuff">
			<tr><i>
				<td style="width: 150px;">Music Album</td>
				<td class="smaller">Them</td>
				<td class="smaller">You</td>
				<td class="smaller">Diff</td>
			</i></tr>
			<?php
				$user1_get_list = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_id");
				$user2_get_list = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_view");
				
				$user1_list = array();
				$user2_list = array();
				while($row = mysql_fetch_array($user1_get_list))
					$user1_list[] = $row['music_id'];
				while($row = mysql_fetch_array($user2_get_list))
					$user2_list[] = $row['music_id'];
	
				//get items they both liked
				$super_list = array_intersect($user1_list, $user2_list);
				if(count($super_list) <= 10)
					$show = count($super_list);
				else
					$show = 10;
		
				for($i = 0; $i < $show; $i++)
				{
				$select = array_rand($super_list);
				$music_selected = $super_list[$select];
				unset($super_list[$select]);
				$get = mysql_query("SELECT * FROM music WHERE music_id = $music_selected");
				$music = mysql_fetch_array($get);
				echo "<tr><td><a href='content.php?type=music&id=" . $music['music_id'] . "'>" . $music['music_name'] . "</a></td>";
				$get = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_view AND music_id = $music_selected");
				$check = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check['rating'] . "</td>";
				$get = mysql_query("SELECT * FROM music_ratings WHERE user_id = $user_id AND music_id = $music_selected");
				$check2 = mysql_fetch_array($get);
				echo "<td class='smaller'>" . $check2['rating'] . "</td>";
				echo "<td class='smaller'>" . abs($check['rating']-$check2['rating']) . "</td></tr>";
				}
			?>
			</table>
			<?php echo "</br></br>" . "<a href='all_common.php?type=music&id=" . $user_view . "'>See All Common Ratings</a>"; ?>
			</div>
        </section>
        
        <script type="text/javascript" src="js/libs/jquery-1.6.2.min.js"></script>
		<script>
        $(".posters").mouseover(function() {
		
		//document.getElementById("print").innerHTML="More Stuff!";

		var target = $(this);
		var id = target.attr("id");
		var user_id = target.attr("user_id");
		var type = target.attr("type");
		var address = "pic_to_rating.php?id="+id+"&type="+type+"&user_id="+user_id;

		$.get(address, function(data) {
			  
				  $('#select').html(data);
				  
				 document.getElementById("print").innerHTML=data;
			});

        });
        $(".posters").mouseleave(function() {

		document.getElementById("print").innerHTML="";
        });
		</script>

        </script>
        <script type="text/javascript">
			$(".tv_stuff").hide();
			$(".vg_stuff").hide();
			$(".book_stuff").hide();
			$(".music_stuff").hide();
			
			$(".dots").click(function()
			{
				var str = $(this).attr("class");
				$('.movie_stuff').hide();
				$(".tv_stuff").hide();
				$(".vg_stuff").hide();
				$(".book_stuff").hide();
				$(".music_stuff").hide();
				if(str == "blue dots")
					$(".movie_stuff").fadeIn(500);
				if(str == "green dots")
					$(".tv_stuff").fadeIn(500);
				if(str == "red dots")
					$(".book_stuff").fadeIn(500);
				if(str == "yellow dots")
					$(".vg_stuff").fadeIn(500);
				if(str == "purple dots")
					$(".music_stuff").fadeIn(500);
			});
		</script>
        <script>
		 $(document).ready(function() {
				$("#your-cranium ul.content-filter a").click(function(e) {
			// Prevent href attribute from firing
			e.preventDefault();

				 // Unset the current active item
				 $("#your-cranium ul.content-filter li.active").removeClass("active");

			// Get the clicked on class to switch out content
			var class = $(this).parent("li").attr("class");

			// Fadeout old content
			$("#your-cranium div.active").fadeOut("fast", function() {
			// Unset this as active content
			$(this).removeClass("active");
			// Fade in new content based on our class from above
			$("#your-cranium div." + class).fadeIn("fast", function() {
			// Set this as the active content
			$(this).addClass("active");
			});
		});
		});
		});
		</script>
      </div>
      <nav class="right">
        <ul>
          <li><a href="privacypolicy.php">Privacy Policy</a></li>
          <li><a href="useragreement.php">Terms of Use</a></li>
          <li class="last"><a href="mailto:team@criticrania.com">Contact Us</a></li>
        </ul>
      </nav>
      <p>&copy; Copyright 2010&ndash;2011 Criticrania. All rights reserved.</p>
    </div>
	</footer>
</div> <!--! end of #container -->

<!-- scripts concatenated and minified via ant build script-->
<script src="js/plugins.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
	var slides = $('.sliding');
	var num = slides.length;
	
	$('#flowinner').css('width', num*990);
	
	var count = 0;
	$("#right").click(function()
	{
		$("#left").show();
		if(count == (num-1))
			return;
		else
			$(".sliding").animate({left: "-=990"});
		count++;
	});
	$("#left").click(function()
	{
		if(count == 0)
			return;
		else
			$(".sliding").animate({left: "+=990"});
		count--;
		if(count == 0)
			$("#left").hide();
	});
	$("#left").hide();
</script>
<!-- end scripts-->

<script>
	var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']]; // Change UA-XXXXX-X to be your site's ID
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script>
	<script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
<![endif]-->

</body>
</html>
