<?php
	include 'functions.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Reviews Everyday from Everyday People</title>
<?php
	include 'top.php';
	include 'sheetshome.php';
?>
</head>
<body>
<!-- start banner -->
<?php
	//banner(0);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style = "background:url(images/body-bg.gif)">

	<div id="content" style="display: inline-block; margin-bottom: 50px; width: 60%; background:url(images/body-bg.gif);">
		<?php
		//include 'did_you_mean_function.php';
		$search_string = mysql_real_escape_string($_GET['search']);
		$search_from_type = mysql_real_escape_string($_GET['type']);
		$search_string_norm = stripslashes(stripslashes($search_string));
		$search_string = stripslashes(strtolower($search_string));

		//$searcher = mysql_query("SELECT * FROM movies WHERE movie_name LIKE '".mysql_escape_string($search_string)."'");
		//$num_rows = mysql_num_rows($searcher);

		//if($num_rows == 1)
		//{
			//$search = mysql_fetch_array($searcher);
			//header('Location: http://www.criticrania.com/movies/movie.php?id='. $search['movie_id'] );
		//}

		$word_array = str_word_count($search_string, 1, '0123456789');
		$words_index = 0;
		$suggest = false;
		$suggestion = $word_array;
		$results = array_fill(0,40,-1);
		$results2 = array_fill(0,40,-1);
		$results3 = array_fill(0,40,-1);
		$results4 = array_fill(0,40,-1);
		$results5 = array_fill(0,40,-1);
		$results6 = array_fill(0,40,-1);
		$hits = array(-1 => 0);
		$num_results = 0;
		$stop_words = array('to','at','of','the','and', 'a', 'for');
		$stop = false;

		$type = "all";

		foreach ($suggestion as $word) 
		{
			
			foreach($stop_words as $stopper)
			{
				if ($word == $stopper)
				{
					$stop = true;
					break;
				}
			}
			
			if($stop)
			{
				$stop = false;
				continue;
			}
			
			if ($type == "movie" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT movie_id, checked FROM movie WHERE movie_name LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					if ($searcher_row['checked'] != 0)
					{
						$results_index = 0;
						if(!($hits[$searcher_row['movie_id']]>= 1))
						{	
							$hits[$searcher_row['movie_id']] = 1;
							$num_results++;
						}
						else
						{
							$hits[$searcher_row['movie_id']]++;
						}
						
						while($results_index < 40)
						{
							if(!($hits[$results[$results_index]]>=$hits[$searcher_row['movie_id']]) || $results[$results_index] == $searcher_row['movie_id'])
							{
								if($results[$results_index] == $searcher_row['movie_id'])
								{
									$results_index = 0;
									break;
								}
								
								else
								{
									for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
									{
										if($results[$stop_loc] == $searcher_row['movie_id'])
										{
											break;
										}
									}
									
									$replacer = $results_index;
									$last = $searcher_row['movie_id'];
									while($replacer < $stop_loc + 1 && $replacer < 40)
									{
										$holder = $results[$replacer];
										$results[$replacer] = $last;
										$last = $holder;
										$replacer++;
									}
									$results_index = 0;
									break;
								}
							}
							$results_index++;
						}
					}
				}
				if ($results_index == 1)
				{
					header("Location: movies/movie.php?id=".$results[0]."");
				}
			}
			
			if ($type == "tv" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT * FROM tv WHERE tv_name LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					if ($searcher_row['checked'] != 0)
					{
						$results_index = 0;
						if(!($hits[$searcher_row['tv_id']]>= 1))
						{	
							$hits[$searcher_row['tv_id']] = 1;
							$num_results++;
						}
						else
						{
							$hits[$searcher_row['tv_id']]++;
						}
						
						while($results_index < 40)
						{
							if(!($hits[$results2[$results_index]]>=$hits[$searcher_row['tv_id']]) || $results2[$results_index] == $searcher_row['tv_id'])
							{
								if($results2[$results_index] == $searcher_row['tv_id'])
								{
									$results_index = 0;
									break;
								}
								
								else
								{
									for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
									{
										if($results2[$stop_loc] == $searcher_row['tv_id'])
										{
											break;
										}
									}
									
									$replacer = $results_index;
									$last = $searcher_row['tv_id'];
									while($replacer < $stop_loc + 1 && $replacer < 40)
									{
										$holder = $results2[$replacer];
										$results2[$replacer] = $last;
										$last = $holder;
										$replacer++;
									}
									$results_index = 0;
									break; 
								}
							}
							$results_index++;
						}
					}
				}
			}
			
			if ($type == "book" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT * FROM book WHERE book_name LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					if ($searcher_row['checked'] != 0)
					{
						$results_index = 0;
						if(!($hits[$searcher_row['book_id']]>= 1))
						{	
							$hits[$searcher_row['book_id']] = 1;
							$num_results++;
						}
						else
						{
							$hits[$searcher_row['book_id']]++;
						}
						
						while($results_index < 40)
						{
							if(!($hits[$results3[$results_index]]>=$hits[$searcher_row['book_id']]) || $results3[$results_index] == $searcher_row['book_id'])
							{
								if($results3[$results_index] == $searcher_row['book_id'])
								{
									$results_index = 0;
									break;
								}
								
								else
								{
									for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
									{
										if($results3[$stop_loc] == $searcher_row['book_id'])
										{
											break;
										}
									}
									
									$replacer = $results_index;
									$last = $searcher_row['book_id'];
									while($replacer < $stop_loc + 1 && $replacer < 40)
									{
										$holder = $results3[$replacer];
										$results3[$replacer] = $last;
										$last = $holder;
										$replacer++;
									}
									$results_index = 0;
									break;
								}
							}
							$results_index++;
						}
					}
				}
			}
			
			if ($type == "vg" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT * FROM vg WHERE vg_name LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					
					if ($searcher_row['checked'] != 0)
					{
						$results_index = 0;
						if(!($hits[$searcher_row['vg_id']]>= 1))
						{	
							$hits[$searcher_row['vg_id']] = 1;
							$num_results++;
						}
						else
						{
							$hits[$searcher_row['vg_id']]++;
						}
						
						while($results_index < 40)
						{
							if(!($hits[$results4[$results_index]]>=$hits[$searcher_row['vg_id']]) || $results4[$results_index] == $searcher_row['vg_id'])
							{
								if($results4[$results_index] == $searcher_row['vg_id'])
								{
									$results_index = 0;
									break;
								}
								
								else
								{
									for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
									{
										if($results4[$stop_loc] == $searcher_row['vg_id'])
										{
											break;
										}
									}
									
									$replacer = $results_index;
									$last = $searcher_row['vg_id'];
									while($replacer < $stop_loc + 1 && $replacer < 40)
									{
										$holder = $results4[$replacer];
										$results4[$replacer] = $last;
										$last = $holder;
										$replacer++;
									}
									$results_index = 0;
									break;
								}
							}
							$results_index++;
						}
					}
				}
			}
			
			if ($type == "music" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT * FROM music WHERE music_name LIKE '%" . mysql_escape_string($word) . "%' OR artist LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					if ($searcher_row['checked'] != 0)
					{
						$results_index = 0;
						if(!($hits[$searcher_row['music_id']]>= 1))
						{	
							$hits[$searcher_row['music_id']] = 1;
							$num_results++;
						}
						else
						{
							$hits[$searcher_row['music_id']]++;
						}
						
						while($results_index < 40)
						{
							if(!($hits[$results5[$results_index]]>=$hits[$searcher_row['music_id']]) || $results5[$results_index] == $searcher_row['music_id'])
							{
								if($results5[$results_index] == $searcher_row['music_id'])
								{
									$results_index = 0;
									break;
								}
								
								else
								{
									for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
									{
										if($results5[$stop_loc] == $searcher_row['music_id'])
										{
											break;
										}
									}
									
									$replacer = $results_index;
									$last = $searcher_row['music_id'];
									while($replacer < $stop_loc + 1 && $replacer < 40)
									{
										$holder = $results5[$replacer];
										$results5[$replacer] = $last;
										$last = $holder;
										$replacer++;
									}
									$results_index = 0;
									break;
								}
							}
							$results_index++;
						}
					}
				}
			}
			
			if ($type == "users" || $type == "all")
			{
				$searcher1 = mysql_query("SELECT id FROM users WHERE username LIKE '%" . mysql_escape_string($word) . "%' OR first_name LIKE '%" . mysql_escape_string($word) . "%' OR last_name LIKE '%" . mysql_escape_string($word) . "%'");
				while($searcher_row = mysql_fetch_array($searcher1))
				{
					$results_index = 0;
					if(!($hits[$searcher_row['id']]>= 1))
					{	
						$hits[$searcher_row['id']] = 1;
						$num_results++;
					}
					else
					{
						$hits[$searcher_row['id']]++;
					}
					
					while($results_index < 40)
					{
						if(!($hits[$results6[$results_index]]>=$hits[$searcher_row['id']]) || $results6[$results_index] == $searcher_row['id'])
						{
							if($results6[$results_index] == $searcher_row['id'])
							{
								$results_index = 0;
								break;
							}
							
							else
							{
								for($stop_loc = 0; $stop_loc < $num_results; $stop_loc++)
								{
									if($results6[$stop_loc] == $searcher_row['id'])
									{
										break;
									}
								}
								
								$replacer = $results_index;
								$last = $searcher_row['id'];
								while($replacer < $stop_loc + 1 && $replacer < 40)
								{
									$holder = $results6[$replacer];
									$results6[$replacer] = $last;
									$last = $holder;
									$replacer++;
								}
								$results_index = 0;
								break;
							}
						}
						$results_index++;
					}
				}
			}
			/*
			if(didYouMean($word))
			{
				$suggest = true;
				$suggestion[$words_index] = $word;
			}
			
			$new_index = strlen($word_array[$words_index]) +1;
			$words_index+=$new_index;*/
		}
		/*
		if($suggest == true)
		{
			$didya = "Did you mean <a href='http://www.criticrania.com/keller/movie_search.php?search=";
			$href = "";
			$word_lister = "";
			foreach($suggestion as $mean)
			{
				$word_lister = $word_lister . " " . $mean;
				$href = $href . $mean . "+";
			}
			
			$href = $href . "'>";
			$word_lister = $word_lister;

			echo $didya . $href . $word_lister . "<a />";
			
			echo"?<br />";
		}*/
		//echo "The incoming type is:" . $type . "</br>";
		echo "</br></br>";
		$type = $search_from_type;
		$genres_printed = 0;
		while($genres_printed < 6)
		{
			if (($type == "movie" || $type == "all") && $genres_printed < 6)
			{
				echo "</br><div style='padding-left: 50px'><span id='search_results' style='color: #3278af;'>Movies matching: <i>$search_string_norm</i></span><br /><br />";
				$count = 0;
				foreach ($results as $result)
				{
					if($result != -1)
					{
						$movie_info = mysql_query("SELECT synopsis, movie_name FROM movie WHERE movie_id = '". (int)$result ."'")or die();
						$movie_name = mysql_fetch_array($movie_info);
						$synopsis = $movie_name['synopsis'];
						$length = strlen($synopsis);
						/*if ($search_string == strtolower($movie_name['movie_name']))
						{
							header ("Location: http://www.criticrania.com/movies/movie.php?id=".(int)$result."");
						}*/
						echo "<a href= 'content.php?type=movie&id=" . (int)$result . "' style='color: #3278af'>" . $movie_name['movie_name'] . "</a><br />";
						if ($length > 180)
						{
							$synopsis2 = substr($synopsis, 0, 150);
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis2...</div><br /><br /><br />";
						}
						else
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis</div><br /><br /><br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There are no movies matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "tv";
			}
			
			if (($type == "tv" || $type == "all") && $genres_printed < 6)
			{
				echo "<div style='padding-left: 50px'><span id='search_results' style='color: #559e38;'>Television shows matching: <i>$search_string_norm</i></span><br /><br />";
				$count = 0;
				foreach ($results2 as $result)
				{
					if($result != -1)
					{
						$tv_info = mysql_query("SELECT * FROM tv WHERE tv_id = '". (int)$result ."'")or die();
						$tv_name = mysql_fetch_array($tv_info);
						$synopsis = $tv_name['synopsis'];
						$length = strlen($synopsis);
						/*if ($search_string == strtolower($tv_name['tv_name']))
						{
							header ("Location: http://www.criticrania.com/television/tv.php?id=".(int)$result."");
						}*/
						echo "<a href= 'content.php?type=tv&id=" . (int)$result . "' style='color: #559e38'>" . $tv_name['tv_name'] . "</a><br />";
						if ($length > 180)
						{
							$synopsis2 = substr($synopsis, 0, 150);
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis2...</div><br /><br /><br />";
						}
						else
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis</div><br /><br /><br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There are no television shows matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "book";
			}
			
			if (($type == "book" || $type == "all") && $genres_printed < 6)
			{
				echo "<div style='padding-left: 50px'><span id='search_results' style='color: #c83535;'>Books matching: <i>$search_string_norm</i></span><br /><br />";
				$count = 0;
				foreach ($results3 as $result)
				{
					if($result != -1)
					{
						$book_info = mysql_query("SELECT * FROM book WHERE book_id = '". (int)$result ."'")or die();
						$book_name = mysql_fetch_array($book_info);
						$synopsis = $book_name['synopsis'];
						$length = strlen($synopsis);
						/*if ($search_string == strtolower($book_name['book_name']))
						{
							header ("Location: http://www.criticrania.com/books/book.php?id=".(int)$result."");
						}*/
						echo "<a href= 'content.php?type=book&id=" . (int)$result . "' style='color: #c83535'>" . $book_name['book_name'] . "</a><br />";
						if ($length > 180)
						{
							$synopsis2 = substr($synopsis, 0, 150);
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis2...</div><br /><br /><br />";
						}
						else
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis</div><br /><br /><br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There are no books matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "vg";
			}
			
			if (($type == "vg" || $type == "all") && $genres_printed < 6)
			{
				echo "<div style='padding-left: 50px'><span id='search_results' style='color: #db9e36;'>Video Games matching: <i>$search_string_norm</i></span><br /><br />";
				$count = 0;
				foreach ($results4 as $result)
				{
					if($result != -1)
					{
						$vg_info = mysql_query("SELECT * FROM vg WHERE vg_id = '". (int)$result ."'")or die();
						$vg_name = mysql_fetch_array($vg_info);
						$synopsis = $vg_name['synopsis'];
						$length = strlen($synopsis);
						/*if ($search_string == strtolower($vg_name['vg_name']))
						{
							header("Location: http://www.criticrania.com/videogames/vg.php?id=".(int)$result."");
						}*/
						echo "<a href= 'content.php?type=vg&id=" . (int)$result . "' style='color: #db9e36'>" . $vg_name['vg_name'] . "</a><br />";
						if ($length > 180)
						{
							$synopsis2 = substr($synopsis, 0, 150);
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis2...</div><br /><br /><br />";
						}
						else
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis</div><br /><br /><br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There are no video games matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "music";
			}
			
			if (($type == "music" || $type == "all") && $genres_printed < 6)
			{
				echo "<div style='padding-left: 50px'><span id='search_results' style='color: #644484;'>Music matching: <i>$search_string_norm</i></span><br /><br />";
				$count = 0;
				foreach ($results5 as $result)
				{
					if($result != -1)
					{
						$music_info = mysql_query("SELECT * FROM music WHERE music_id = '". (int)$result ."'")or die();
						$music_name = mysql_fetch_array($music_info);
						$synopsis = $music_name['artist'];
						$length = strlen($synopsis);
						/*if ($search_string == strtolower($music_name['music_name']))
						{
							header ("Location: http://www.criticrania.com/music/music.php?id=".(int)$result."");
						}*/
						echo "<a href= 'content.php?type=music&id=" . (int)$result . "' style='color: #644484'>" . $music_name['music_name'] . "</a><br />";
						if ($length > 180)
						{
							$synopsis2 = substr($synopsis, 0, 150);
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis2...</div><br /><br />";
						}
						else
							echo "<div id='synopsis' style='color: #f8f8f0'>$synopsis</div><br /><br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There is no music matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "users";
			}
			
			if (($type == "users" || $type == "all") && $genres_printed < 6)
			{
				echo "<div style='padding-left: 50px;color: #a9a9a9;'><font id='search_results'>Users matching: <i>$search_string_norm</i></font><br /><br />";
				$count = 0;
				foreach ($results6 as $result)
				{
					if($result != -1)
					{
						$user_info = mysql_query("SELECT * FROM users WHERE id = '". (int)$result ."'")or die();
						$user_name = mysql_fetch_array($user_info);
						/*if ($search_string == strtolower($user_name['username']))
						{
							header ("Location: http://www.criticrania.com/users.php?id=".(int)$result."");
						}*/
						echo "<a href= 'users.php?id=" . (int)$result . "' style='color: #f8f8f0' >" . $user_name['username'] . "</a><br />";
						echo "<br />";
						$count++;
					}
				}
				if ($count == 0)
				{
					echo "<div id='synopsis' style='color: #f8f8f0'>There are no users matching your search.</div><br /><br />";
				}
				echo "</div>";
				$genres_printed++;
				$type = "movie";
			}
		}

		?>
	</div>&nbsp;
	<div style="float: right; width: 25%; text-align: center; font-size: 18px; padding: 200px 50px 0 0; background:url(/images/body-bg.gif); color:#a9a9a9;">
		<b>Don't see something you're looking for?</b></br> Shoot us an <a href="mailto:add@criticrania.com">email</a>
		
	</div>&nbsp;
<!-- end page -->

</div>
<br />
<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>