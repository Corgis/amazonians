<?php
	include 'functions.php';
	include 'top.php';
	$type ="tv";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Television</title>
<?php
	include 'sheetshome.php';
?>
</head>
<body>
<!-- start banner -->
<?php
	//banner(2);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style="white-space: normal; background:url(images/body-bg.gif);">

	<!-- Title + informative blurb -->
	<div id="title" style = "color:#559e38">Television<br /></div>
	<div id="description" style = "color:#559e38">
		Share your feelings about each and every episode of your favorite show, or let the world know just how much you despise what
		your sister forces you to watch. Discuss the newest episodes and reminisce about past classics. It is the Digital Age after all.
	</div><br />
	<!-- Specific content for television homepage -->
	<div id="content" style = "background:url(images/body-bg.gif)">
		<div id="top_left"><br /><br />
			<!-- Include NIVO slider. Top Ten obtains the ten highest average ratings of tv shows in the past week (See functions) -->
			<div class="head">This Week's Top Rated TV Shows</div><br />
			<div id="slider">
				<?php          					
					top_ten("tv");
				?>
			</div>
		</div>
		<!-- 3 Most Recent TV Show reviews, including picture and author of review -->
		<div id="top_right"><br />
			<div class="head">Recent Reviews</div><br />
				<?php	
					//Get all TV Show reviews ordered by date (most recent first)								
					$check = mysql_query("SELECT * FROM tv_reviews ORDER BY date DESC")or die();
					
					echo "<table width='100%' cellspacing='8px'>";
					$recent = 0;
					//Only deal with the 3 most recent reviews
					while($recent < 3)
					{
						//Get Review
						$row = mysql_fetch_array($check);
						//Get username of review
						$usercheck = mysql_query("SELECT * FROM users WHERE id = '". $row['user_id'] ."'")or die();
						$username = mysql_fetch_array($usercheck);
						//Obtain actual review, add slashes to be able to check tv table
						$review1 = addslashes($row['review']);
						//Find TV Show that correlates to written review
						$tv_info = mysql_query("SELECT * FROM tv_reviews WHERE review = '$review1'");
						//Determine length of the review
						$length = strlen($review1);
						//Remove slashes to create readable text
						$review1 = stripslashes($review1);
						$review1 = stripslashes($review1);
						//Obtain TV Show information (it's row of information)
						$tv_row = mysql_fetch_array($tv_info);
						$tv_id = $tv_row['tv_id'];	
						$line2 = mysql_query("SELECT * FROM tv_reviews WHERE tv_id = '$tv_id' ORDER BY date DESC")or die();
						$tv_info = mysql_query("SELECT * FROM tv WHERE tv_id = '$tv_id'")or die();
						$tv_row = mysql_fetch_array($tv_info);
						//Display TV Show title with a link to its specific content page
						echo "<tr valign='top'><td width='80%'>";
						echo "<hr><movietitle><a href='/content.php?type=tv&id=$tv_id' style = 'color:#559e38'>" .$tv_row['tv_name']. "</a></movietitle><hr><br />";
						//Display Author of review, including a link to their profile page
						echo "<a href= '/users.php?id=" . $row['user_id'] . "' color='black'>" . $username['username'] . "</a> - ";
						//Obtain TV Show's image
						$poster = $tv_row['poster'];
						$filename = "television/images/$poster";
						
						/*Check if length of review is too long to display, if so display only 150 characters and offer to allow user to read more, else display review*/
						if ($length > 180)
						{
							$review2 = substr($review1, 0, 150);
							echo "\"$review2...\" <a href ='content.php?type=tv&id=$tv_id'>Read more</a>";
						}
						else
							echo "\"$review1\"";
						//Check if thumbnail exists, if not, create it
						if (file_exists($filename))
						{
							$image = "<br><td><a href='content.php?type=tv&id=$tv_id'><img id='poster' src='television/images/$poster' /></a>";
						}
						else
						{
							$image = "<br><td><a href='content.php?type=tv&id=$tv_id'><img id='poster' src='images/no_image_thumb.png' /></a>";
						}
						//Disaply thumbnail of TV Show image
						echo $image;
						echo "</td></tr>";
						//Increment to next most recent review
						$recent++;
					}
					echo "</table>";
				?>
				&nbsp;
		</div>
		<div id="bottom_left"><br />
			<!-- Display tuhumbnails of 5 most visited tv content pages in last hour (see Functions) -->
			<div class="head">Trending Now</div>
			<div style="text-align: center; color:#559e38">Here are the 5 most visited televison shows in the last hour</div><br />
				<?php
					trending("tv");
				?>
		</div>
		<div id="bottom_right"><br />
		<!-- Allow users to answer a poll question, display results after they have voted (see poll.php) -->
			<?php include 'poll.php'; ?>
		</div>
	</div>
	&nbsp;
</div><br />
<!-- end page -->

<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>
