<?php
	if ($type == "movie")
	{
		$color = "blue";
		$random_type = "Movie";
	}
	else if ($type == "tv")
	{
		$color = "green";
		$random_type = "TV Show";
	}
	else if ($type == "book")
	{
		$color = "red";
		$random_type = "Book";
	}
	else if ($type == "vg")
	{
		$color = "yellow";
		$random_type = "Game";
	}
	else if ($type == "music")
	{
		$color = "purple";
		$random_type = "Album";
	}

	if(!$type)
	{
		$newrand = rand(1, 5);
		if ($newrand == 1)
		{
			$type = "movie";
			$color = "blue";
			$random_type = "Movie";
		}
		if ($newrand == 2)
		{
			$type = "tv";
			$color = "green";
			$random_type = "TV Show";
		}
		if ($newrand == 3)
		{
			$type = "book";
			$color = "red";
			$random_type = "Book";
		}
		if ($newrand == 4)
		{
			$type = "vg";
			$color = "yellow";
			$random_type = "Game";
		}
		if ($newrand == 5)
		{
			$type = "music";
			$color = "purple";
			$random_type = "Album";
		}
	}

	$found = false;
	$ratings_table = $type."_ratings";
	$type_id = $type."_id";
	while($found == false)
	{
		$query = mysql_query("SELECT DISTINCT $type_id FROM $ratings_table ORDER BY rand() LIMIT 5");
		while($row = mysql_fetch_array($query))
		{
			$rating_check = mysql_query("SELECT * FROM $ratings_table WHERE user_id = '".$_SESSION['id']."' AND $type_id = " . $row[$type_id]);
			if (mysql_num_rows($rating_check) == 0 )
			{
				$rand_id = $row[$type_id];
				$found = true;
				break;
			}
		}
	}
	if(!loggedin()){
?>
	<header id="global-header">
		<div class="container clearfix">
		  <a href="/" id="logo"><img src="img/logo.png" alt="Criticrania" /> Criticrania</a>
		  <div id="user-nav" class="right">
			<span class="open log" style="display: inline">Login</span>
			<span class="close log" style="display: inline">Close</span>
			&middot;
			<span><a href="about.php">About Us</a></span>
		  </div>
		</div>
		<div id="global-nav" class="container clearfix">
		  <nav>
			<ul>
			  <li><a class="blue" href="movies.php">Movies</a></li>
			  <li><a class="green" href="television.php">Television</a></li>
			  <li><a class="red" href="books.php">Books</a></li>
			  <li><a class="yellow" href="videogames.php">Games</a></li>
			  <li><a class="purple" href="music.php">Music</a></li>
			  <li><a class="white" href="groups_list.php">Groups</a></li>
			</ul>
		  </nav>
		  <div id="randsearch" class="right">
		  <a class='<?php echo $color; ?>' href='content.php?type=<?php echo $type; ?>&id=<?php echo $rand_id; ?>'>Random <?php echo $random_type; ?></a>
			<form id="search-form" action='search.php' accept-charset="utf-8">
				<input type='hidden' name='type' value='<?php echo $type; ?>'/>
				<div class="input text">
				  <!--<label for="search">Search movies, tv, music...</label>-->
				  <input name="search" type="text" tabindex="1" id="search" placeholder="Search movies, tv, books..."/>
				  <input type="image" style="cursor: pointer; position: absolute; top: 15px; right: 10px;" src="../img/quick-search.png" />
				</div>
			</form>
		  </div>
		  <div style="position: absolute; right: 0; top: -40px; width: 100%;">
			<form id="log" method="post" action="login.php">
				<input id="user" class="input" type="text" name="logininfo" placeholder="Username or Email" />
				<input class="input" type="password" name="password" placeholder="Password" />
				<button type="submit" name="login">Login</button>
			</form>
		  </div>
		</div>
	</header>
<?php }
	else{ ?>
		<header id="global-header">
		<div class="container clearfix">
		  <a href="/" id="logo"><img src="img/logo.png" alt="Criticrania" /> Criticrania</a>
		  <div id="user-nav" class="right">
		  	<a href="/users.php?id=<?php echo $_SESSION['id'];?>">
			<span id="score">
				<div id="progress-outer">
					<div id="progress-inner"></div>
				</div>
				<?php
					$query = mysql_query("SELECT score FROM users WHERE id = '".$_SESSION['id']."'")or die();
					$get_score = mysql_fetch_array($query);
					$score = $get_score["score"];
					echo $score;
					$tilnext = (level2($_SESSION['id'], $score));
					$nextlevel = $tilnext[1];
					$prevlevel = $tilnext[2];
					$percent = ($score - $prevlevel)/($nextlevel - $prevlevel);
					$percent = $percent*100;
				?>
			</a>
			</span>	
			&#124;
			<span class="username" style="display: inline;"><?php echo $_SESSION['username']; ?> <img id="down_arrow" src="img/arrow_down.png" /><img id="up_arrow" src="img/arrow_up.png" /></span>
		  </div>
		</div>
		<div id="global-nav" class="container clearfix">
		  <nav>
			<ul>
			  <li><a class="blue" href="movies.php">Movies</a></li>
			  <li><a class="green" href="television.php">Television</a></li>
			  <li><a class="red" href="books.php">Books</a></li>
			  <li><a class="yellow" href="videogames.php">Games</a></li>
			  <li><a class="purple" href="music.php">Music</a></li>
			  <li><a class="white" href="groups_list.php">Groups</a></li>
			</ul>
		  </nav>
		  <div id="randsearch" class="right">
		    <a class='<?php echo $color; ?>' href='content.php?type=<?php echo $type; ?>&id=<?php echo $rand_id; ?>'>Random <?php echo $random_type; ?></a>
			<form id="search-form" action='search.php' accept-charset="utf-8">
				<input type='hidden' name='type' value='<?php echo $type; ?>'/>
				<div class="input text">
				  <!--<label for="search">Search movies, tv, music...</label>-->
				  <input name="search" type="text" tabindex="1" id="search" placeholder="Search movies, tv, books..." />
				  <input type="image" style="cursor: pointer; position: absolute; top: 15px; right: 10px;" src="../img/quick-search.png" />
				</div>
			</form>
		  </div>
		  <div id="drop" style="position: absolute; right: 0; top: 7px;">
			  <a href="mail.php">Mailbox</a>
			  &middot;
			  <a href="settings.php">Settings</a>
			  &middot;
			  <a href="/logout.php?logout=1">Logout</a>
		  </div>
		</div>
	</header>	
	<?php } ?>
<script type="text/javascript">
	$("#log").hide();
	$("#signup").hide();
	$(".close").hide();
	$("#drop").hide();
	$("#up_arrow").hide();
	
	var clicklog = 0;
	var clicksign = 0;
	var clickuser = 0;
	
	$(".log").click(function()
	{
		if((clicklog%2) == 0)
		{
			$(this).toggle();
			$(this).next(".close").toggle();
			$("#randsearch").animate({bottom: '-=80'}, 400);
		}
		else
		{
			$(this).toggle();
			$(this).prev(".open").toggle();
			$("#randsearch").animate({bottom: '+=80'}, 400);
		}
		$("#log").slideToggle(400);
		
		clicklog++;
	});
	
	$(".signup").click(function()
	{
		if((clicksign%2) == 0)
		{
			$(this).toggle();
			$(this).next(".close").toggle();
		}
		else
		{
			$(this).toggle();
			$(this).prev(".open").toggle();
		}
		$("#signup").slideToggle(400);
		clicksign++;
	});
	
	$(".username").click(function()
	{
		$("#up_arrow").toggle();
		$("#down_arrow").toggle();
	
		if((clickuser%2) == 0)
		{
			$("#randsearch").animate({bottom: '-=80'}, 400);
		}
		else
		{
			$("#randsearch").animate({bottom: '+=80'}, 400);
		}
		$("#drop").slideToggle(400);
		
		clickuser++;
	});
			<?php if (loggedin()) { ?>
		var percent = <?php echo $percent;?>;
		$("#progress-inner").css("width", percent+"%");<?php } ?>

</script>
