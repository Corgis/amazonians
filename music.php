<?php
	include 'functions.php';
	include 'top.php';
	$type = "music";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Criticrania.com - Music</title>
<?php
	include 'sheetshome.php';
?>
</head>
<body>
<!-- start banner -->
<?php
	//banner(5);
	include 'banner.php';
?>
<!-- end banner -->
<!-- start page -->
<div id="page" style="background:url(images/body-bg.gif)">

	<!-- Title + informative blurb -->
	<div id="title" style = "color:#644484">Music<br /></div>
	<div id="description" style = "color:#644484">
		Do you yell, "I love this song!" or change the station when this song comes on the radio? Share your musical tastes and rate the greatest albums of 
		all time as well as the much-anticipated ones that failed your expectations.
	</div><br />
	<!-- Specific content for music homepage -->
	<div id="content" style="background:url(images/body-bg.gif)">
		<div id="top_left"><br /><br />
			<!-- Include NIVO slider. Top Ten obtains the ten highest average ratings of albums in the past week (See functions) -->
			<div class="head">This Week's Top Rated Albums</div><br /><br /><br /><br />
			<div id="slider" style="height: 291px; width: 263px;">
				<?php          
					top_ten("music");
				?>
			</div>
		</div>
		<!-- 3 Most Recent Album reviews, including picture and author of review -->
		<div id="top_right"><br />
			<div class="head">Recent Reviews</div><br />
				<?php
					//Get all album reviews ordered by date (most recent first)									
					$check = mysql_query("SELECT * FROM music_reviews ORDER BY date DESC")or die();
					
					echo "<table width='100%' cellspacing='20px'>";
					$recent = 0;
					//Only deal with the 3 most recent reviews
					while($recent < 3)
					{
						//Get Review
						$row = mysql_fetch_array($check);
						//Get username of review
						$usercheck = mysql_query("SELECT * FROM users WHERE id = '". $row['user_id'] ."'")or die();
						$username = mysql_fetch_array($usercheck);
						//Obtain actual review, add slashes to be able to check music table
						$review1 = addslashes($row['review']);
						//Find album that correlates to written review
						$music_info = mysql_query("SELECT * FROM music_reviews WHERE review = '$review1'");
						//Determine length of the review
						$length = strlen($review1);
						//Remove slashes to create readable text
						$review1 = stripslashes($review1);
						$review1 = stripslashes($review1);
						//Obtain album information (it's row of information)
						$music_row = mysql_fetch_array($music_info);
						$music_id = $music_row['music_id'];	
						$line2 = mysql_query("SELECT * FROM music_reviews WHERE music_id = '$music_id' ORDER BY date DESC")or die();
						$music_info = mysql_query("SELECT * FROM music WHERE music_id = '$music_id'")or die();
						$music_row = mysql_fetch_array($music_info);
						//Display Album title with a link to its specific content page
						echo "<tr valign='top'><td width='80%'>";
						echo "<hr><movietitle><a href='/content.php?type=music&id=$music_id' style = 'color:#644484'>" .$music_row['music_name']. "</a></movietitle><hr><br />";
						//Display Author of review, including a link to their profile page
						echo "<a href= '/users.php?id=" . $row['user_id'] . "' color='black'>" . $username['username'] . "</a> - ";
						//Obtain album's image
						$poster = $music_row['poster'];
						$filename = "music/images/$poster";
						
						/*Check if length of review is too long to display, if so display only 150 characters and offer to allow user to read more, else display review*/
						if ($length > 180)
						{
							$review2 = substr($review1, 0, 150);
							echo "\"$review2...\" <a href ='content.php?type=music&id=$music_id'>Read more</a>";
						}
						else
							echo "\"$review1\"";
						//Check if thumbnail exists, if not, create it
						//**Must adjust for music album squareness by adding padding for correct positioning and size
						if (file_exists($filename))
						{
							$image = "<br><td style='padding-top: 10px;'><a href='content.php?type=music&id=$music_id'><img id='poster' style='height:120px; width:120px;' src='music/images/$poster' /></a>";
						}
						else
						{
							$image = "<br><td style='padding-top: 10px;'><a href='content.php?type=music&id=$music_id'><img id='poster' style='height:120px; width:120px;' src='images/no_image_thumb.png' /></a>";
						}
						//Disaply thumbnail of music image
						echo $image;
						echo "</td></tr>";
						//Increment to next most recent review
						$recent++;
					}
					echo "</table>";
				?>
				&nbsp;
		</div>
		<div id="bottom_left"><br />
			<!-- Display tuhumbnails of 5 most visited music content pages in last hour (see Functions) -->
			<div class="head">Trending Now</div>
			<div style="text-align: center; color:#644484;">Here are the 5 most visited pages in the last hour</div><br />
				<?php
					trending("music");
				?>
		</div>
		<div id="bottom_right"><br />
			<!-- Allow users to answer a poll question, display results after they have voted (see poll.php) -->
			<?php include 'poll.php'; ?>
		</div>
	</div>
	&nbsp;
</div><br />
<!-- end page -->

<div id="footer">
	&copy; Copyright 2010 Criticrania.com, Inc.  All Rights Reserved.
</div>
</body>
</html>
