<?php 

require_once 'functions.php';

class user
{
  public $id;
  public $username;
  public $first;
  public $last;
  public $email;
  public $admin;
    
  function __construct($id)
  {
    $res = mysql_query('SELECT username, 
                               email, 
                               first_name, 
                               last_name 
                        FROM users 
                        WHERE id = ' . $id);
    $arr = mysql_fetch_array($res);
    $this->id = $id;
    $this->username = $arr['username'];
    $this->first = $arr['first_name'];
    $this->last = $arr['last_name'];
    $this->email = $arr['email'];
  }
    
  function cc($ob, $type)
  {
	  $this->ratings_compare($ob, $num, $average, $type);
    $square = pow($num,2);
    $high = 0;
    $low = 0;
    if ($num > 0)
    {
      $high = 6+((4*$square)/($square+(5*$num)));
      $low = 4+((-4*$square)/($square+(5*$num)));
    }
    $c = (($high-$low)-$average)+$low;

	  if(number_format($c,2) < 0)
	    return 0;
	  else
	    return number_format($c,2);
  }
    
  function get_ratings($limit, $order, $type)
  {
    if($limit == 'all')
      $ratings_query = mysql_query('SELECT * 
                                    FROM ' . $type . '_ratings 
                                    WHERE user_id = ' . $this->id . '
                                    ORDER BY ' . $order);
    else
      $ratings_query = mysql_query('SELECT * 
                                    FROM ' . $type . '_ratings 
                                    WHERE user_id = ' . $this->id . '
                                    ORDER BY ' . $order . ' 
                                    LIMIT ' . $limit);
                              
    $res = array(); 
    for ($i = 0; $ratings = mysql_fetch_assoc($ratings_query); $i++)
      $res[$i] = $ratings;
    return $res;
  }
    
  function ratings_compare($ob, &$num, &$avg, $type)
  {
    $type_id = $type.'_id';
    $type_name = $type.'_name';
    if ($type == 'movie')
    {
      $type2 = $type.'s';
      $type3 = 'Movie';
    }
    if ($type == 'book')
    {
      $type2 = $type.'s';
      $type3 = 'Book';
    }
    if ($type == 'tv')
    {
      $type2 = 'television';
      $type3 = 'TV Show';
    }
    if ($type == 'vg')
    {
      $type2 = 'videogames';
      $type3 = 'Video Game';
    }
    if ($type == 'music')
    {
      $type2 = 'music';
      $type3 = 'Album';
    }
    
    $user_1_ratings = $this->get_ratings('all', $type_id, $type);
    if (get_class($ob) == 'user')
      $user_2_ratings = $ob->get_ratings('all', $type_id, $type);
    else if (get_class($ob) == 'group')
      $user_2_ratings = $ob->get_average_ratings('all', $type_id, $type);
    $num = 0;
    $total_diff = 0;

    $user_1_size = sizeof($user_1_ratings);
    $user_2_size = sizeof($user_2_ratings);
  
    for ($i = 0, $j = 0; $i < $user_1_size && $j < $user_2_size; $i++, $j++)
    {
      if($user_1_ratings[$i][$type_id] == $user_2_ratings[$j][$type_id])
      {
        $total_diff += abs($user_1_ratings[$i]['rating'] - $user_2_ratings[$j]['rating']);
        $num++;
      } 
      else if($user_1_ratings[$i][$type_id] < $user_2_ratings[$j][$type_id])
        $j--;
      else
        $i--;
    }
    if ($num > 0)
    $avg = $total_diff/$num;
  }
}

$usr = new user(1);

$usr2 = new user(2);

echo ($usr2->username);

echo ($usr->cc($usr2,'movie')); 

?>
